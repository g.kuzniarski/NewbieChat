package newbieChat.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import newbieChat.entity.GameProfile;

import java.io.Serializable;
import java.util.Objects;

public class GameProfileDTO implements Serializable {

    @JsonProperty("game")
    private String game;

    @JsonProperty("details")
    private String details;

    public GameProfileDTO() {
    }

    public GameProfileDTO(String game, String details) {
        this.game = game;
        this.details = details;
    }

    public GameProfileDTO(GameProfile gameProfile) {
        this.game = gameProfile.getConfigGameProfile().getGame();
        this.details = gameProfile.getDetails();
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameProfileDTO that = (GameProfileDTO) o;
        return Objects.equals(game, that.game) &&
                Objects.equals(details, that.details);
    }

    @Override
    public int hashCode() {

        return Objects.hash(game, details);
    }

    @Override
    public String toString() {
        return "GameProfileDTO{" +
                "game='" + game + '\'' +
                ", details=" + details +
                '}';
    }
}
