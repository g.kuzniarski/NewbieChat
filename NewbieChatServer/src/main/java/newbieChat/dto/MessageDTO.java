package newbieChat.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import newbieChat.entity.Message;
import newbieChat.util.OffsetDateTimeDeserializer;

import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.Objects;

public class MessageDTO implements Serializable {

    @JsonProperty("sendFrom")
    private Long sendFrom;

    @JsonProperty("sendTo")
    private Long sendTo;

    @JsonProperty("content")
    private String content;

    @JsonProperty("sendDate")
    @JsonSerialize(using = ToStringSerializer.class)
    @JsonDeserialize(using = OffsetDateTimeDeserializer.class)
    private OffsetDateTime sendDate;

    @JsonProperty("read")
    private boolean read;

    public MessageDTO() {
    }

    public MessageDTO(Long sendFrom, Long sendTo, String content, OffsetDateTime sendDate, boolean read) {
        this.sendFrom = sendFrom;
        this.sendTo = sendTo;
        this.content = content;
        this.sendDate = sendDate;
        this.read = read;
    }

    public MessageDTO(Message message) {
        this.sendFrom = message.getSendFrom().getId();
        this.sendTo = message.getSendTo().getId();
        this.content = message.getContent();
        this.sendDate = message.getSendDate();
        this.read = message.isRead();
    }

    public Long getSendFrom() {
        return sendFrom;
    }

    public void setSendFrom(Long sendFrom) {
        this.sendFrom = sendFrom;
    }

    public Long getSendTo() {
        return sendTo;
    }

    public void setSendTo(Long sendTo) {
        this.sendTo = sendTo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public OffsetDateTime getSendDate() {
        return sendDate;
    }

    public void setSendDate(OffsetDateTime sendDate) {
        this.sendDate = sendDate;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageDTO that = (MessageDTO) o;
        return read == that.read &&
                Objects.equals(sendFrom, that.sendFrom) &&
                Objects.equals(sendTo, that.sendTo) &&
                Objects.equals(content, that.content) &&
                Objects.equals(sendDate, that.sendDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sendFrom, sendTo, content, sendDate, read);
    }

    @Override
    public String toString() {
        return "MessageDTO{" +
                "sendFrom=" + sendFrom +
                ", sendTo=" + sendTo +
                ", content='" + content + '\'' +
                ", sendDate=" + sendDate +
                ", read=" + read +
                '}';
    }
}
