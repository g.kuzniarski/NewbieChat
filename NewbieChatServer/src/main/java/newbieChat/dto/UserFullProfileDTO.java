package newbieChat.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import newbieChat.entity.User;
import newbieChat.enums.UserStatusType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class UserFullProfileDTO implements Serializable {

    @JsonProperty("id")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long id;

    @JsonProperty("nickname")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String nickname;

    @JsonProperty("name")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String name;

    @JsonProperty("oldPassword")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String oldPassword;

    @JsonProperty("newPassword")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String newPassword;

    @JsonProperty("language")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String language;

    @JsonProperty("age")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer age;

    @JsonProperty("sex")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String sex;

    @JsonProperty("country")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String country;

    @JsonProperty("city")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String city;

    @JsonProperty("avatarPath")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String avatarPath;

    @JsonProperty("status")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private UserStatusType status;

    @JsonProperty("gamingStyle")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String gamingStyle;

    @JsonProperty("timeSpent")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String timeSpent;

    @JsonProperty("timeOfDay")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String timeOfDay;

    @JsonProperty("gameProfiles")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<GameProfileDTO> gameProfileList;

    public UserFullProfileDTO() {
    }

    public UserFullProfileDTO(Long id, String nickname, String name, String oldPassword, String newPassword, String language, Integer age, String sex, String country, String city, String avatarPath, UserStatusType status, String gamingStyle, String timeSpent, String timeOfDay, List<GameProfileDTO> gameProfileList) {
        this.id = id;
        this.nickname = nickname;
        this.name = name;
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
        this.language = language;
        this.age = age;
        this.sex = sex;
        this.country = country;
        this.city = city;
        this.avatarPath = avatarPath;
        this.status = status;
        this.gamingStyle = gamingStyle;
        this.timeSpent = timeSpent;
        this.timeOfDay = timeOfDay;
        this.gameProfileList = gameProfileList;
    }

    public UserFullProfileDTO(User user) {
        if (user != null) {
            this.id = user.getId();
            this.nickname = user.getLogin();
            this.name = user.getName();
            this.avatarPath = user.getAvatarUrl();
            this.language = user.getLanguage().toString();

            if (user.getProfile() != null) {
                this.age = user.getProfile().getAge();
                this.sex = user.getProfile().getSex();
                this.country = user.getProfile().getCountry();
                this.city = user.getProfile().getCity();
                this.gamingStyle = user.getProfile().getGamingStyle();
                this.timeSpent = user.getProfile().getTimeSpent();
                this.timeOfDay = user.getProfile().getTimeOfDay();
            }

            if (user.getGameProfileList() != null) {
                this.gameProfileList = new ArrayList<>();
                user.getGameProfileList().forEach(gameProfile -> {
                    this.gameProfileList.add(new GameProfileDTO(gameProfile));
                });
            }

            if (user.getStatus() != null) {
                this.status = user.getStatus().getStatusType();
            }
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAvatarPath() {
        return avatarPath;
    }

    public void setAvatarPath(String avatarPath) {
        this.avatarPath = avatarPath;
    }

    public UserStatusType getStatus() {
        return status;
    }

    public void setStatus(UserStatusType status) {
        this.status = status;
    }

    public String getGamingStyle() {
        return gamingStyle;
    }

    public void setGamingStyle(String gamingStyle) {
        this.gamingStyle = gamingStyle;
    }

    public String getTimeSpent() {
        return timeSpent;
    }

    public void setTimeSpent(String timeSpent) {
        this.timeSpent = timeSpent;
    }

    public String getTimeOfDay() {
        return timeOfDay;
    }

    public void setTimeOfDay(String timeOfDay) {
        this.timeOfDay = timeOfDay;
    }

    public List<GameProfileDTO> getGameProfileList() {
        return gameProfileList;
    }

    public void setGameProfileList(List<GameProfileDTO> gameProfileList) {
        this.gameProfileList = gameProfileList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserFullProfileDTO that = (UserFullProfileDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(nickname, that.nickname) &&
                Objects.equals(name, that.name) &&
                Objects.equals(language, that.language) &&
                Objects.equals(oldPassword, that.oldPassword) &&
                Objects.equals(newPassword, that.newPassword) &&
                Objects.equals(age, that.age) &&
                Objects.equals(sex, that.sex) &&
                Objects.equals(country, that.country) &&
                Objects.equals(city, that.city) &&
                Objects.equals(avatarPath, that.avatarPath) &&
                status == that.status &&
                Objects.equals(gamingStyle, that.gamingStyle) &&
                Objects.equals(timeSpent, that.timeSpent) &&
                Objects.equals(timeOfDay, that.timeOfDay) &&
                Objects.equals(gameProfileList, that.gameProfileList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nickname, name, language, oldPassword, newPassword, age, sex, country, city, avatarPath, status,
                gamingStyle, timeSpent, timeOfDay, gameProfileList);
    }

    @Override
    public String toString() {
        return "UserFullProfileDTO{" +
                "id=" + id +
                ", nickname='" + nickname + '\'' +
                ", name='" + name + '\'' +
                ", language='" + language + '\'' +
                ", oldPassword='" + oldPassword + '\'' +
                ", newPassword='" + newPassword + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", avatarPath='" + avatarPath + '\'' +
                ", status=" + status +
                ", gamingStyle='" + gamingStyle + '\'' +
                ", timeSpent='" + timeSpent + '\'' +
                ", timeOfDay='" + timeOfDay + '\'' +
                ", gameProfileList=" + gameProfileList +
                '}';
    }
}
