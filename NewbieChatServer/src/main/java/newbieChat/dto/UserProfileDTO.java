package newbieChat.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import newbieChat.entity.User;
import newbieChat.enums.UserStatusType;

import java.io.Serializable;

public class UserProfileDTO implements Serializable {

    @JsonProperty("id")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long id;

    @JsonProperty("nickname")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String nickname;

    @JsonProperty("name")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String name;

    @JsonProperty("age")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer age;

    @JsonProperty("sex")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String sex;

    @JsonProperty("country")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String country;

    @JsonProperty("gamingStyle")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String gamingStyle;

    @JsonProperty("avatarPath")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String avatarPath;

    @JsonProperty("status")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String status;

    public UserProfileDTO() {
    }

    public UserProfileDTO(Long id, String nickname, String name, Integer age, String sex, String country, String gamingStyle, String avatarPath, String status) {
        this.id = id;
        this.nickname = nickname;
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.country = country;
        this.gamingStyle = gamingStyle;
        this.avatarPath = avatarPath;
        this.status = status;
    }

    public UserProfileDTO(User user) {
        if (user != null) {
            this.id = user.getId();
            this.nickname = user.getLogin();
            this.name = user.getName();
            this.avatarPath = user.getAvatarUrl();

            if (user.getProfile() != null) {
                this.age = user.getProfile().getAge();
                this.sex = user.getProfile().getSex();
                this.country = user.getProfile().getCountry();
                this.gamingStyle = user.getProfile().getGamingStyle();
            }

            if (user.getStatus() != null) {
                if (user.getStatus().isOnline()) {
                    this.status = String.valueOf(user.getStatus().getStatusType());
                } else {
                    this.status = UserStatusType.OFFLINE.toString();
                }
            }
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getGamingStyle() {
        return gamingStyle;
    }

    public void setGamingStyle(String gamingStyle) {
        this.gamingStyle = gamingStyle;
    }

    public String getAvatarPath() {
        return avatarPath;
    }

    public void setAvatarPath(String avatarPath) {
        this.avatarPath = avatarPath;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "UserProfileDTO{" +
                "id=" + id +
                ", nickname='" + nickname + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                ", country='" + country + '\'' +
                ", gamingStyle='" + gamingStyle + '\'' +
                ", avatarPath='" + avatarPath + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
