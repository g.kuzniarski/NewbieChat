package newbieChat.dto;

import newbieChat.entity.User;

import java.io.Serializable;
import java.util.Objects;

public class InviteDTO implements Serializable {
    private Long userId;
    private String nickname;
    private String name;
    private String avatar;
    private String action;

    public InviteDTO() {
    }

    public InviteDTO(Long userId, String nickname, String name, String avatar, String action) {
        this.userId = userId;
        this.nickname = nickname;
        this.name = name;
        this.avatar = avatar;
        this.action = action;
    }

    public InviteDTO(User user) {
        this.userId = user.getId();
        this.nickname = user.getLogin();
        this.name = user.getName();
        this.avatar = user.getAvatarUrl();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InviteDTO inviteDTO = (InviteDTO) o;
        return Objects.equals(userId, inviteDTO.userId) &&
                Objects.equals(nickname, inviteDTO.nickname) &&
                Objects.equals(name, inviteDTO.name) &&
                Objects.equals(avatar, inviteDTO.avatar) &&
                Objects.equals(action, inviteDTO.action);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, nickname, name, avatar, action);
    }

    @Override
    public String toString() {
        return "InviteDTO{" +
                "userId='" + userId + '\'' +
                ", nickname='" + nickname + '\'' +
                ", name='" + name + '\'' +
                ", avatar='" + avatar + '\'' +
                ", action='" + action + '\'' +
                '}';
    }
}
