package newbieChat.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import newbieChat.entity.Contact;
import newbieChat.entity.User;
import newbieChat.enums.UserStatusType;

import java.io.Serializable;
import java.util.Objects;

public class UserDetailsDTO implements Serializable {

    @JsonProperty("id")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long id;

    @JsonProperty("nickname")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String nickname;

    @JsonProperty("name")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String name;

    @JsonProperty("status")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private UserStatusType status;

    @JsonProperty("statusDetails")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String statusDetails;

    @JsonProperty("avatarPath")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String avatarPath;

    public UserDetailsDTO() {
    }

    public UserDetailsDTO(Long id, String nickname, String name, UserStatusType status, String statusDetails, String avatarPath) {
        this.id = id;
        this.nickname = nickname;
        this.name = name;
        this.status = status;
        this.statusDetails = statusDetails;
        this.avatarPath = avatarPath;
    }

    public UserDetailsDTO(User user) {
        this.id = user.getId();
        this.nickname = user.getLogin();
        this.name = user.getName();
        this.avatarPath = user.getAvatarUrl();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserStatusType getStatus() {
        return status;
    }

    public void setStatus(UserStatusType status) {
        this.status = status;
    }

    public String getStatusDetails() {
        return statusDetails;
    }

    public void setStatusDetails(String statusDetails) {
        this.statusDetails = statusDetails;
    }

    public String getAvatarPath() {
        return avatarPath;
    }

    public void setAvatarPath(String avatarPath) {
        this.avatarPath = avatarPath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDetailsDTO that = (UserDetailsDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(nickname, that.nickname) &&
                Objects.equals(name, that.name) &&
                Objects.equals(status, that.status) &&
                Objects.equals(statusDetails, that.statusDetails) &&
                Objects.equals(avatarPath, that.avatarPath);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, nickname, name, status, statusDetails, avatarPath);
    }

    @Override
    public String toString() {
        return "UserDetailsDTO{" +
                "id=" + id +
                ", nickname='" + nickname + '\'' +
                ", name='" + name + '\'' +
                ", status='" + status + '\'' +
                ", statusDetails='" + statusDetails + '\'' +
                ", avatarPath='" + avatarPath + '\'' +
                '}';
    }
}
