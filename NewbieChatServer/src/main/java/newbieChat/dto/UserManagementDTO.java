package newbieChat.dto;

import newbieChat.entity.User;

import java.io.Serializable;
import java.time.OffsetDateTime;

public class UserManagementDTO implements Serializable {

    private String id;
    private String login;
    private String name;
    private String avatarUrl;
    private String language;
    private OffsetDateTime birthday;
    private String status;
    private String online;

    public UserManagementDTO() {
    }

    public UserManagementDTO(String id, String login, String name, String avatarUrl, OffsetDateTime birthday, String language, String status, String online) {
        this.id = id;
        this.login = login;
        this.name = name;
        this.avatarUrl = avatarUrl;
        this.birthday = birthday;
        this.language = language;
        this.status = status;
        this.online = online;
    }

    public UserManagementDTO(User user) {
        this.id = user.getId().toString();
        this.login = user.getLogin();
        this.name = user.getName();
        this.avatarUrl = user.getAvatarUrl();
        this.birthday = user.getBirthday();
        this.language = user.getLanguage().toString();
        if (user.getStatus() != null) {
            this.status = String.valueOf(user.getStatus().getStatusType());
            this.online = user.getStatus().isOnline() ? "Yes" : "No";
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public OffsetDateTime getBirthday() {
        return birthday;
    }

    public void setBirthday(OffsetDateTime birthday) {
        this.birthday = birthday;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }
}
