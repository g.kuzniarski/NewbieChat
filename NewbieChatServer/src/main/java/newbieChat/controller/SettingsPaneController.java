package newbieChat.controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.GridPane;

import java.io.File;
import java.io.IOException;

public abstract class SettingsPaneController extends GridPane {

    private ServerMainController rootController;
    private String templateName;

    public ServerMainController getRootController() {
        return rootController;
    }

    public void setRootController(ServerMainController rootController) {
        this.rootController = rootController;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public void initView() {
        if (rootController == null) {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(".." + File.separator + ".." + File.separator + this.templateName));
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);
            try {
                fxmlLoader.load();
            } catch (IOException exception) {
                throw new RuntimeException(exception);
            }
        }
    }

    public abstract void initController();

    public abstract void setServices(ServerMainController serverMainController);
}
