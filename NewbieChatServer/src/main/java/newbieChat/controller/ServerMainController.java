package newbieChat.controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import newbieChat.dictionary.Icons;
import newbieChat.dictionary.ServerTreeItems;
import newbieChat.dictionary.Texts;
import newbieChat.model.Server;
import newbieChat.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class ServerMainController extends WindowController {

    @FXML
    TreeView<String> settingsTree;
    @FXML
    ScrollPane settingsDetailsPane;
    @FXML
    Label leftStatus;
    @FXML
    Label rightStatus;

    private Server serverInstance;

    private static final Logger logger = LogManager.getLogger(ServerMainController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private ContactService contactService;

    @Autowired
    private StatusService statusService;

    @Autowired
    private MessageService messageService;

    @Autowired
    private MatchService matchService;

    @Autowired
    private ConfigGameProfileService configGameProfileService;

    @Override
    public void initialize() {
        //Generate settings tree
        generateSettingsTree();
    }

    private void generateSettingsTree() {
        //Server root item
        TreeItem<String> serverRootItem = new TreeItem<>(Texts.SERVER_TREE_ITEM_LABEL, Icons.SERVER_TREE_ITEM_ICON);
        serverRootItem.setExpanded(true);
        serverRootItem.getChildren().addAll(getListOfSettings());

        this.settingsTree.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<TreeItem<String>>() {
            @Override
            public void changed(ObservableValue<? extends TreeItem<String>> observable, TreeItem<String> oldValue, TreeItem<String> newValue) {
                changeSelectedSettingsView((TreeItem<String>) newValue);
            }
        });
        this.settingsTree.setRoot(serverRootItem);
        this.settingsTree.setEditable(true);
    }

    private void changeSelectedSettingsView(TreeItem<String> selectedItem) {
        Map<String, ServerTreeItems.ServerTreeItem> settingsMap = ServerTreeItems.SERVER_TREE_ITEM_MAP;
        if (!settingsMap.containsKey(selectedItem.getValue())) {
            if (selectedItem.getParent() != null && settingsMap.containsKey(selectedItem.getParent().getValue())) {
                List<ServerTreeItems.ServerTreeItem> childrenList = settingsMap.get(selectedItem.getParent().getValue()).getChildren();
                if (!childrenList.isEmpty()) {
                    ServerTreeItems.ServerTreeItem selectedServerTreeItem = null;
                    for (ServerTreeItems.ServerTreeItem item : childrenList) {
                        if (item.getLabel().equals(selectedItem.getValue())) {
                            selectedServerTreeItem = item;
                            break;
                        }
                    }
                    if (selectedServerTreeItem != null) {
                        changeSettingsView(selectedServerTreeItem.getController());

                        rightStatus.setText(selectedItem.getValue());
                    }
                }
            }
        } else if (settingsMap.get(selectedItem.getValue()).getChildren().isEmpty()) {
            changeSettingsView(settingsMap.get(selectedItem.getValue()).getController());

            rightStatus.setText(selectedItem.getValue());
        }
    }

    private void changeSettingsView(SettingsPaneController controller) {
        controller.initView();
        controller.setRootController(this);
        controller.setServices(this);
        controller.initController();
        this.settingsDetailsPane.setContent(null);
        this.settingsDetailsPane.setContent(controller);
    }

    private List<TreeItem<String>> getListOfSettings() {
        List<TreeItem<String>> listOfSettings = new ArrayList<>();

        ServerTreeItems.SERVER_TREE_ITEM_MAP.forEach((label, item) -> {
            TreeItem<String> treeItem = new TreeItem<>(label, item.getIcon());
            if (!item.getChildren().isEmpty()) {
                item.getChildren().forEach(child -> treeItem.getChildren().add(new TreeItem<>(child.getLabel(), child.getIcon())));
                treeItem.setExpanded(true);
            }
            listOfSettings.add(treeItem);
        });

        return listOfSettings;
    }

    public void createServerInstance(String portString) {
        int port;

        try {
            port = Integer.parseInt(portString);
        } catch (Exception e1) {
            logger.info("Zły format portu");
            return;
        }

        serverInstance = Server.createInstance(port, this);
        Thread serverThread = new Thread(serverInstance);
        logger.info("Start serwera");
        serverThread.start();

        leftStatus.setText("Server: running");
    }

    public void destroyServerInstance() {
        logger.info("Zatrzymanie serwera");
        serverInstance.destroyServer();

        leftStatus.setText("Server: stopped");
    }

    public UserService getUserService() {
        return userService;
    }

    public ClientService getClientService() {
        return clientService;
    }

    public ContactService getContactService() {
        return contactService;
    }

    public StatusService getStatusService() {
        return statusService;
    }

    public MessageService getMessageService() {
        return messageService;
    }

    public MatchService getMatchService() {
        return matchService;
    }

    public ConfigGameProfileService getConfigGameProfileService() {
        return configGameProfileService;
    }
}
