package newbieChat.controller;

import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

/**
 * Class being Window Controller
 */
public abstract class WindowController {

    GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
    Stage primaryStage;

    /**
     * The method used to control Stages and control flow between Controllers
     *
     * @param primaryStage main Stage
     */
    public void setMainController(Stage primaryStage) {
        this.primaryStage = primaryStage;

        //Set close action
        primaryStage.setOnCloseRequest(event -> getCloseAction(event));

        initialize();
    }

    public void showWindow() {
        primaryStage.show();
        primaryStage.toFront();
    }

    public boolean isOpen() {
        return primaryStage != null;
    }

    private void getCloseAction(WindowEvent event) {
        this.primaryStage = null;
    }

    /**
     * Method that initialize Window
     */
    public abstract void initialize();

}
