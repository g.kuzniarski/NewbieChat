package newbieChat.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import newbieChat.dto.UserManagementDTO;
import newbieChat.service.UserService;

import java.util.stream.Collectors;

public class SettingsUsersPaneController extends SettingsPaneController {

    @FXML
    TableView<UserManagementDTO> usersTable;
    @FXML
    Button addButon;
    @FXML
    Button editButton;
    @FXML
    Button deleteButton;

    private UserService userService;

    private ObservableList<UserManagementDTO> usersList;

    public SettingsUsersPaneController(String templateName) {
        this.setTemplateName(templateName);
    }

    @Override
    public void initController() {
        usersList = FXCollections.observableArrayList();
        initTable();
        refreshTable();
    }

    @Override
    public void setServices(ServerMainController serverMainController) {
        userService = serverMainController.getUserService();
    }

    private void initTable() {
        usersTable.setEditable(false);
        usersTable.getColumns().clear();

        TableColumn<UserManagementDTO, String> idColumn = new TableColumn<UserManagementDTO, String>("ID");
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        idColumn.setEditable(false);

        TableColumn<UserManagementDTO, String> onlineColumn = new TableColumn<UserManagementDTO, String>("Online");
        onlineColumn.setCellValueFactory(new PropertyValueFactory<>("online"));
        onlineColumn.setEditable(false);

        TableColumn<UserManagementDTO, String> loginColumn = new TableColumn<UserManagementDTO, String>("Login");
        loginColumn.setCellValueFactory(new PropertyValueFactory<>("login"));
        loginColumn.setEditable(false);

        TableColumn<UserManagementDTO, String> nameColumn = new TableColumn<UserManagementDTO, String>("Name");
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        nameColumn.setEditable(false);

        TableColumn statusColumn = new TableColumn("Status");
        statusColumn.setCellValueFactory(
                new PropertyValueFactory<UserManagementDTO, String>("Status"));
        statusColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        statusColumn.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<UserManagementDTO, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<UserManagementDTO, String> t) {
                        t.getTableView().getItems().get(
                                t.getTablePosition().getRow()).setStatus(t.getNewValue());
                    }
                }
        );

        TableColumn<UserManagementDTO, String> languageColumn = new TableColumn<UserManagementDTO, String>("Language");
        languageColumn.setCellValueFactory(new PropertyValueFactory<>("language"));
        languageColumn.setEditable(false);

        usersTable.setItems(usersList);
        usersTable.getColumns().addAll(idColumn, onlineColumn, loginColumn, nameColumn, languageColumn, statusColumn);
    }

    private void refreshTable() {
        usersList.clear();
        usersList.addAll(userService
                .getAllUsers()
                .stream()
                .map(user -> new UserManagementDTO(user)
                ).collect(Collectors.toSet()));
    }
}
