package newbieChat.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import newbieChat.model.Server;
import newbieChat.service.MessageService;
import newbieChat.service.StatusService;
import newbieChat.service.UserService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.Enumeration;

public class SettingsSummaryPaneController extends SettingsPaneController {

    @FXML
    Label statusLabel;
    @FXML
    Label addressLabel;
    @FXML
    Label portLabel;
    @FXML
    Label registereLabel;
    @FXML
    Label onlineLabel;
    @FXML
    Label totalMsgLabel;
    @FXML
    Label lastMsgLabel;

    private UserService userService;
    private StatusService statusService;
    private MessageService messageService;

    public SettingsSummaryPaneController(String templateName) {
        this.setTemplateName(templateName);
    }

    @Override
    public void initController() {
        if (Server.getInstance() != null && Server.getInstance().isServerRunning()) {
            statusLabel.setText("Running");
            addressLabel.setText(getAllIP());
            portLabel.setText(String.valueOf(Server.getInstance().getPort()));
        } else {
            statusLabel.setText("Stopped");
            addressLabel.setText("---");
            portLabel.setText("---");
        }
        registereLabel.setText(String.valueOf(userService.countAll()));
        onlineLabel.setText(String.valueOf(statusService.countOnline()));
        totalMsgLabel.setText(String.valueOf(messageService.countAll()));
        lastMsgLabel.setText(String.valueOf(messageService.countLast24h()));
    }

    private String getAllIP() {
        String ip = "";

        try {
            URL checkIPAmazon = new URL("http://checkip.amazonaws.com");
            BufferedReader in = new BufferedReader(new InputStreamReader(checkIPAmazon.openStream()));
            ip = in.readLine();
            return ip;
        } catch (IOException ignored) {
        }

        try {
            final DatagramSocket socket = new DatagramSocket();
            socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            ip = socket.getLocalAddress().getHostAddress();
        } catch (UnknownHostException | SocketException e) {
            e.printStackTrace();
        }

        return ip;
    }

    @Override
    public void setServices(ServerMainController serverMainController) {
        userService = serverMainController.getUserService();
        statusService = serverMainController.getStatusService();
        messageService = serverMainController.getMessageService();
    }
}
