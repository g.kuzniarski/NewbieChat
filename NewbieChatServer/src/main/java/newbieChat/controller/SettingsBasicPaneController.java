package newbieChat.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import newbieChat.dictionary.Texts;
import newbieChat.service.UserService;

public class SettingsBasicPaneController extends SettingsPaneController {

    @FXML
    Button startServerButton;

    private UserService userService;

    private boolean isServerRunning = false;

    public SettingsBasicPaneController(String templateName) {
        this.setTemplateName(templateName);
    }

    @Override
    public void initController() {
        this.startServerButton.setOnAction(event -> startOrStopServer());
    }

    @Override
    public void setServices(ServerMainController serverMainController) {
    }

    private void startOrStopServer() {
        this.startServerButton.setDisable(true);
        if (this.isServerRunning) {
            this.getRootController().destroyServerInstance();
            this.startServerButton.setText(Texts.START_SERVER_BUTTON);
            this.isServerRunning = false;
        } else {
            this.getRootController().createServerInstance("1500");
            this.startServerButton.setText(Texts.STOP_SERVER_BUTTON);
            this.isServerRunning = true;
        }
        this.startServerButton.setDisable(false);
    }
}
