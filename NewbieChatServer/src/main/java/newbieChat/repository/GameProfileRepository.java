package newbieChat.repository;

import newbieChat.entity.GameProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameProfileRepository extends JpaRepository<GameProfile, Long> {
}
