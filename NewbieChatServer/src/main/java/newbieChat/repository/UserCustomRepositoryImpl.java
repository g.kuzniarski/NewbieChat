package newbieChat.repository;

import newbieChat.entity.Profile;
import newbieChat.entity.User;
import newbieChat.enums.UserStatusType;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class UserCustomRepositoryImpl implements UserCustomRepository {
    @PersistenceContext
    private EntityManager entityManager;

    public List<User> findAllByFilters(Map<String, Object> filters) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);

        Root<User> user = criteriaQuery.from(User.class);
        Path<Profile> profile = user.get("profile");
        Path<Profile> status = user.get("status");
        List<Predicate> predicates = new ArrayList<>();

        filters.forEach((key, value) -> {
            switch (key) {
                case "login":
                    predicates.add(criteriaBuilder.like(user.get("login"), "%" + value + "%"));
                    break;
                case "name":
                    predicates.add(criteriaBuilder.like(user.get("name"), "%" + value + "%"));
                    break;
                case "country":
                    predicates.add(criteriaBuilder.like(profile.get("country"), "%" + value + "%"));
                    break;
                case "status":
                    String statusType = ((String) value).toUpperCase();
                    if (UserStatusType.contains(statusType)) {
                        if (UserStatusType.OFFLINE.toString().toLowerCase().equals(statusType.toLowerCase())) {
                            predicates.add(criteriaBuilder.equal(status.get("online"), false));
                        } else {
                            predicates.add(criteriaBuilder.equal(status.get("online"), true));
                            int statusTypeValue = UserStatusType.valueOf(statusType).ordinal();
                            predicates.add(criteriaBuilder.equal(status.get("statusType"), statusTypeValue));
                        }
                    }
                    break;
                case "gamingStyle":
                    predicates.add(criteriaBuilder.equal(profile.get("gamingStyle"), value));
                    break;
                case "ageMin":
                    predicates.add(criteriaBuilder.greaterThanOrEqualTo(profile.get("age"), (Integer) value));
                    break;
                case "ageMax":
                    predicates.add(criteriaBuilder.lessThanOrEqualTo(profile.get("age"), (Integer) value));
                    break;
            }
        });

        criteriaQuery
                .select(user)
                .where(criteriaBuilder.and(predicates.toArray(new Predicate[0])))
                .orderBy(criteriaBuilder.asc(user.get("id")));

        return entityManager.createQuery(criteriaQuery).getResultList();
    }
}