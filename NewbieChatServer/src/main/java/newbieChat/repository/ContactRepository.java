package newbieChat.repository;

import newbieChat.entity.Contact;
import newbieChat.entity.ContactId;
import newbieChat.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContactRepository extends JpaRepository<Contact, ContactId> {
    List<Contact> findAllByOwner(User owner);

    List<Contact> findAllByOwner_Id(Long ownerId);

    List<Contact> findAllByContact(User contact);

    List<Contact> findAllByContact_Id(Long contactId);

    @Query(value = "SELECT c.* " +
            "FROM contact_list c " +
            "WHERE c.owner = ?1 " +
            "OR c.contact = ?1", nativeQuery = true)
    List<Contact> findAllByUserId(Long owner);

    void deleteAllByContact_Id(Long contactId);

    void deleteAllByOwner_Id(Long currentUserId);
}
