package newbieChat.repository;

import newbieChat.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {

@Query(value = "SELECT * " +
"FROM message m " +
"WHERE (m.send_to = ?1 AND m.send_from = ?2) " +
"      OR (m.send_to = ?2 AND m.send_from = ?1)", nativeQuery = true)
List<Message> findAllBySendFromAndSendToOrderBySendDateDesc(Long sendFrom, Long sendTo);

    @Query(value = "SELECT * " +
            "FROM message m " +
            "WHERE m.send_to = ?1 " +
            "OR m.send_from = ?1", nativeQuery = true)
    List<Message> findAllBySendFromOrSendTo(Long userId);

    @Query(value = "SELECT * " +
            "FROM message m " +
            "WHERE (m.send_to = ?1 AND m.send_from NOT IN (?2))" +
            "OR (m.send_from = ?1 AND m.send_to NOT IN (?2))", nativeQuery = true)
    List<Message> findAllBySendFromOrSendToAndNotIn(Long userId, Collection<Long> contactIds);

    @Query(value = "SELECT COUNT(*) " +
            "FROM message m " +
            "WHERE m.send_date > (SELECT now() - ('1 day' \\:\\: interval))", nativeQuery = true)
    long countLast24h();
}
