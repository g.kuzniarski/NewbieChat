package newbieChat.repository;

import newbieChat.entity.Match;
import newbieChat.entity.MatchtId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MatchRepository extends JpaRepository<Match, MatchtId> {

    @Query(value = "SELECT m.* " +
            "FROM match m " +
            "WHERE (m.user1 = ?1 AND m.agree1 IS NULL) " +
            "OR (m.user2 = ?1 AND m.agree2 IS NULL) " +
            "LIMIT 1 ", nativeQuery = true)
    Optional<Match> findMatchForUser(Long userId);

    @Query(value = "SELECT m.* " +
            "FROM match m " +
            "WHERE (m.user1 = ?1 AND m.user2 = ?2) " +
            "OR (m.user1 = ?2 AND m.user2 = ?1) " +
            "LIMIT 1 ", nativeQuery = true)
    Optional<Match> findById(Long owner, Long matched);
}
