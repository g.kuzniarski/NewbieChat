package newbieChat.repository;

import newbieChat.entity.ConfigGameProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfigGameProfileRepository extends JpaRepository<ConfigGameProfile, String> {
}
