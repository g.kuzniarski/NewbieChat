package newbieChat.repository;

import newbieChat.entity.User;

import java.util.List;
import java.util.Map;

public interface UserCustomRepository {
    List<User> findAllByFilters(Map<String, Object> filters);
}
