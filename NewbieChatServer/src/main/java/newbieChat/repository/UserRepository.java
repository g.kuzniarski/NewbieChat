package newbieChat.repository;

import newbieChat.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long>, UserCustomRepository {
    Optional<User> findByLogin(String login);

    List<User> findAllByIdIn(Collection<Long> ids);

    @Query(value = "SELECT DISTINCT u.* " +
            "FROM users u " +
            "JOIN profile p  ON u.profile_id = p.id " +
            "JOIN users_game_profile ugp ON u.id = ugp.user_id " +
            "JOIN game_profile gp ON ugp.gameprofilelist_id = gp.id " +
            "WHERE p.gaming_style = ?1 " +
            "AND p.time_of_day IN (?2) " +
            "AND p.time_spent IN (?3) " +
            "AND gp.config_game_profile IN (?4)", nativeQuery = true)
    List<User> findMatchingUser(String gamingStyle, List<String> timeOfDay, List<String> timeSpent, List<String> gameList);

    @Query(value = "SELECT COUNT(*) " +
            "FROM users u " +
            "JOIN status s2 on u.status_id = s2.id " +
            "WHERE s2.online = true", nativeQuery = true)
    long countByOnline();
}
