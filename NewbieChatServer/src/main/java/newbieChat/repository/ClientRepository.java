package newbieChat.repository;

import newbieChat.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
    Optional<Client> findByClientId(String clientId);

    @Query(value = "SELECT c.* " +
            "FROM client c " +
            "WHERE c.user_id IN (?1)", nativeQuery = true)
    List<Client> findAllByUserIn(Collection<Long> userList);
}
