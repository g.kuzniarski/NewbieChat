package newbieChat.enums;

public enum UserStatusType {
    ONLINE("Online"),
    BRB("BRB"),
    OFFLINE("Offline");

    private String status;

    UserStatusType(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return status;
    }

    public static boolean contains(String status) {
        try {
            valueOf(status);
            return true;
        } catch (IllegalArgumentException ignored) {
            return false;
        }
    }
}