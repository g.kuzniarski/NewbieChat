package newbieChat.enums;

import java.io.Serializable;

public enum Language implements Serializable {
    Polski("Polski"),
    English("English");

    private final String language;

    Language(String language) {
        this.language = language;
    }

    public static boolean contains(String language) {
        try {
            valueOf(language);
            return true;
        } catch (IllegalArgumentException ignored) {
            return false;
        }
    }

    @Override
    public String toString() {
        return language;
    }
}
