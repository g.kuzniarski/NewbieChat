package newbieChat.model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import newbieChat.controller.WindowController;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.io.IOException;
import java.net.URL;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import static java.util.ResourceBundle.getBundle;

public abstract class AbstractFxmlView implements ApplicationContextAware {

    private ObjectProperty<Object> presenterProperty;
    private FXMLLoader fxmlLoader;
    private ResourceBundle bundle;

    private URL resource;

    private ApplicationContext applicationContext;
    private Object root;

    public AbstractFxmlView() {
        this.presenterProperty = new SimpleObjectProperty<>();
        this.resource = getClass().getResource("../../templates/" + getConventionalName() + ".fxml");
        try {
            this.bundle = getBundle("lang");
        } catch (MissingResourceException ex) {
            this.bundle = null;
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if (this.applicationContext != null) {
            return;
        }

        this.applicationContext = applicationContext;
    }

    private FXMLLoader loadSynchronously(URL resource, ResourceBundle bundle) throws IllegalStateException {
        FXMLLoader loader = new FXMLLoader(resource, bundle);
        loader.setControllerFactory(type -> this.applicationContext.getBean(type));

        if (this.root != null) {
            loader.setRoot(root);
        }

        try {
            loader.load();
            if (loader.getController() instanceof WindowController)
                ((WindowController) loader.getController()).initialize();
        } catch (IOException ex) {
            throw new IllegalStateException("Cannot load " + getConventionalName(), ex);
        }

        return loader;
    }

    private void ensureFxmlLoaderInitialized() {
        if (this.fxmlLoader != null) {
            return;
        }

        this.fxmlLoader = loadSynchronously(resource, bundle);
        this.presenterProperty.set(this.fxmlLoader.getController());
    }

    public Parent getView() {
        ensureFxmlLoaderInitialized();

        Parent parent = fxmlLoader.getRoot();
        addCssIfAvailable(parent);
        return parent;
    }

    private void addCssIfAvailable(Parent parent) {
        URL uri = getClass().getResource("../../main.css");
        if (uri == null) {
            return;
        }

        String uriToCss = uri.toExternalForm();
        parent.getStylesheets().add(uriToCss);
    }

    private String getConventionalName() {
        String className = getClass().getSimpleName();
        if (!className.endsWith("View")) {
            return className;
        }
        return className.substring(0, className.lastIndexOf("View"));
    }
}
