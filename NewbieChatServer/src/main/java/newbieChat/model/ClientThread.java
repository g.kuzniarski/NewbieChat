package newbieChat.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import newbieChat.controller.ServerMainController;
import newbieChat.dto.*;
import newbieChat.entity.*;
import newbieChat.enums.ServerCommunicationObjectType;
import newbieChat.enums.UserStatusType;
import newbieChat.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

class ClientThread extends Thread {

    private static final Logger logger = LogManager.getLogger(ServerMainController.class);
    private ObjectMapper objectMapper = new ObjectMapper();

    private UserService userService;
    private ClientService clientService;
    private ContactService contactService;
    private StatusService statusService;
    private MessageService messageService;
    private MatchService matchService;
    private ConfigGameProfileService configGameProfileService;

    private Socket socket;
    private ObjectInputStream sInput;
    private ObjectOutputStream sOutput;
    private String clientID;
    private Long currentUserId;

    ClientThread(Socket socket, String clientId) {
        this.clientID = clientId;
        this.socket = socket;
        try {
            sOutput = new ObjectOutputStream(socket.getOutputStream());
            sInput = new ObjectInputStream(socket.getInputStream());
            logger.info(String.format("{%s} is connecting to server", clientId));
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setClientService(ClientService clientService) {
        this.clientService = clientService;
    }

    public void setContactService(ContactService contactService) {
        this.contactService = contactService;
    }

    public void setStatusService(StatusService statusService) {
        this.statusService = statusService;
    }

    public void setMessageService(MessageService messageService) {
        this.messageService = messageService;
    }

    public void setMatchService(MatchService matchService) {
        this.matchService = matchService;
    }

    public void setConfigGameProfileService(ConfigGameProfileService configGameProfileService) {
        this.configGameProfileService = configGameProfileService;
    }

    public void run() {
        if (authorization()) {
            boolean keepGoing = true;
            while (keepGoing) {
                try {
                    ServerCommunicationObject cm = null;

                    try {
                        cm = (ServerCommunicationObject) sInput.readObject();
                    } catch (IOException | ClassNotFoundException e) {
                        logger.error(clientID + "::" + e.getMessage());
                        keepGoing = false;
                    }

                    if (cm != null) {
                        logger.info("CLIENT::" + clientID + "::" + cm.getType() + "::" + cm.getContent());
                        switch (cm.getType()) {
                            case MESSAGE_ADD:
                                addMessage(cm.getContent());
                                break;
                            case MESSAGE_LIST:
                                sendMessageList(cm.getContent());
                                break;
                            case MESSAGE_OTHER:
                                sendMessageFromOthersList();
                                break;
                            case USER_ME:
                                sendCurrentUserInfo();
                                break;
                            case USER_UPDATE:
                                updateUser(cm.getContent());
                                break;
                            case USER_DELETE:
                                deleteUser();
                                keepGoing = false;
                                break;
                            case USER_ME_STATUS:
                                changeUserStatus(cm.getContent());
                                break;
                            case USER_ME_STATUS_DETAILS:
                                changeUserStatusDetails(cm.getContent());
                                break;
                            case USER_LIST:
                                sendContactList();
                                break;
                            case USER_LIST_FILTERS:
                                sendUserListWithFilters(cm.getContent());
                                break;
                            case CONTACT_ADD:
                                addContactById(cm.getContent());
                                break;
                            case CONTACT_ADD_LOGIN:
                                addContactByLogin(cm.getContent());
                                break;
                            case CONTACT_DELETE:
                                deleteContact(cm.getContent());
                                break;
                            case CONTACT_DETAILS:
                                sendUserDetails(cm.getContent());
                                break;
                            case INVITE_LIST:
                                sendInviteList();
                                break;
                            case INVITE_DECIDE:
                                decideInvite(cm.getContent());
                                break;
                            case MATCH_FIND:
                                findMatch();
                                break;
                            case MATCH_DECIDE:
                                decideMatch(cm.getContent());
                                break;
                            case SETTINGS_GET:
                                sendSettingsInfo();
                                break;
                            case LOGOUT:
                                keepGoing = false;
                                break;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.error(e.getMessage());
                    keepGoing = false;
                }
            }
            logoutClient();
        }
        close();
    }

    private void sendSettingsInfo() {
        Map<String, Object> content = new HashMap<>();

        List<ConfigGameProfileDTO> configGameProfiles = configGameProfileService
                .findAll()
                .stream()
                .map(ConfigGameProfileDTO::new)
                .collect(Collectors.toList());

        UserFullProfileDTO userFullProfileDTO = userService.findById(currentUserId).map(UserFullProfileDTO::new).orElse(null);

        if (!configGameProfiles.isEmpty() && userFullProfileDTO != null) {
            try {
                content.put("configGameProfile", objectMapper.writeValueAsString(configGameProfiles));
                content.put("userFullProfile", objectMapper.writeValueAsString(userFullProfileDTO));
            } catch (JsonProcessingException e) {
                logger.error(e.getMessage());
                return;
            }

            write(new ServerCommunicationObject(ServerCommunicationObjectType.SETTINGS_GET, content));
        }
    }

    private void updateUser(Map<String, Object> receivedContent) {
        if (receivedContent.containsKey("userProfile")) {
            try {
                UserFullProfileDTO userFullProfileDTO = objectMapper.readValue((String) receivedContent.get("userProfile"), UserFullProfileDTO.class);
                userService.updateUser(userFullProfileDTO);
                sendCurrentUserInfo();
                broadcastContactChange();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void deleteUser() {
        sendLogout();
        List<String> clientIdListOfMyContacts = getClientIdListOfMyContacts();
        userService.deleteUser(currentUserId);
        broadcastSendContactList(clientIdListOfMyContacts);
        currentUserId = -1L;
    }

    private void sendLogout() {
        write(new ServerCommunicationObject(ServerCommunicationObjectType.LOGOUT, new HashMap<>()));
    }

    private void decideMatch(Map<String, Object> receivedContent) {
        if (receivedContent.containsKey("userId") && receivedContent.containsKey("action")) {
            Long userId = (Long) receivedContent.get("userId");
            String action = (String) receivedContent.get("action");

            switch (action) {
                case "ACCEPT":
                    matchService
                            .addDecisionToMatch(currentUserId, userId, true)
                            .ifPresent(match -> {
                                if (match.isAgree1() != null && match.isAgree1()
                                        && match.isAgree2() != null && match.isAgree2()) {
                                    contactService.addContacts(match.getUser1(), match.getUser2());
                                    sendContactList();
                                    sendContactList(userId);
                                }
                            });
                    break;
                case "DISCARD":
                    matchService.addDecisionToMatch(currentUserId, userId, false);
                    break;
            }
        }
    }

    private void findMatch() {
        matchService
                .findAnyMatch(currentUserId)
                .map(match -> match.getUser1().getId().equals(currentUserId)
                        ? new UserFullProfileDTO(match.getUser2()) : new UserFullProfileDTO(match.getUser1()))
                .ifPresent(this::sendMatch);
    }

    private void sendMatch(UserFullProfileDTO userProfile) {
        Map<String, Object> content = new HashMap<>();

        try {
            content.put("matchedUser", objectMapper.writeValueAsString(userProfile));
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage());
        }

        write(new ServerCommunicationObject(ServerCommunicationObjectType.MATCH_FIND, content));
    }

    private void addContactById(Map<String, Object> content) {
        if (content.containsKey("contactId")) {
            Long contactId = (Long) content.get("contactId");
            if (contactId != null) {
                contactService
                        .addContact(currentUserId, contactId)
                        .ifPresent(contact -> sendContactList());

                sendInvite(contactId);
                sendMessageFromOthersList();
            }
        }
    }

    private void sendInvite(Long contactId) {
        broadcastInvite(findClientIdsByContactId(contactId));
    }

    private void broadcastInvite(List<String> clientIds) {
        Server.getInstance()
                .findAllClientThreadsByIds(clientIds)
                .forEach(ClientThread::sendInviteList);
    }

    private void addContactByLogin(Map<String, Object> content) {
        if (content.containsKey("contactLogin")) {
            String contactLogin = (String) content.get("contactLogin");
            userService
                    .findByLogin(contactLogin)
                    .ifPresent(user -> {
                        contactService
                                .addContact(currentUserId, user.getId())
                                .ifPresent(contact -> sendContactList());

                        sendInvite(user.getId());
                    });
        }
    }

    private void deleteContact(Map<String, Object> content) {
        if (content.containsKey("contactId")) {
            contactService.deleteContact(currentUserId, (Long) content.get("contactId"));
            sendContactList();
            sendContactList((Long) content.get("contactId"));
        }
    }

    private void addMessage(Map<String, Object> content) {
        if (content.containsKey("sendTo") && content.containsKey("message")) {
            messageService
                    .createMessage(currentUserId, (Long) content.get("sendTo"), (String) content.get("message"))
                    .map(MessageDTO::new)
                    .ifPresent(this::broadcastMessage);
        }
    }

    private void sendMessageList(Map<String, Object> receivedContent) {
        if (receivedContent.containsKey("sendTo")) {
            Map<String, Object> content = new HashMap<>();

            List<MessageDTO> messageList = messageService.getMessageListSendBetween(
                    currentUserId,
                    (Long) receivedContent.get("sendTo"))
                    .stream()
                    .map(MessageDTO::new)
                    .collect(Collectors.toList());

            try {
                content.put("messageList", objectMapper.writeValueAsString(messageList));
                content.put("sendTo", receivedContent.get("sendTo"));
            } catch (JsonProcessingException e) {
                logger.error(e.getMessage());
            }

            write(new ServerCommunicationObject(ServerCommunicationObjectType.MESSAGE_LIST, content));
        }
    }

    private void sendMessageFromOthersList() {
        Map<String, Object> content = new HashMap<>();

        Set<Long> contactList = contactService
                .findAllByOwner(currentUserId)
                .stream()
                .map(contact -> contact.getContact().getId())
                .collect(Collectors.toSet());

        List<UserProfileDTO> contactDTOList;
        if (!contactList.isEmpty()) {
            contactDTOList = messageService.getOtherMessageContactList(currentUserId, contactList);
        } else {
            contactDTOList = messageService.getOtherMessageContactList(currentUserId);
        }

        try {
            content.put("other", objectMapper.writeValueAsString(contactDTOList));
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage());
        }

        write(new ServerCommunicationObject(ServerCommunicationObjectType.MESSAGE_OTHER, content));
    }

    private void broadcastMessage(MessageDTO message) {
        Set<Long> users = new HashSet<>();
        users.add(message.getSendFrom());
        users.add(message.getSendTo());

        List<String> clientIds = clientService
                .findAllByUserId(users)
                .stream()
                .map(Client::getClientId)
                .collect(Collectors.toList());

        Server.getInstance()
                .findAllClientThreadsByIds(clientIds)
                .forEach(clientThread -> clientThread.sendMessage(message));
    }

    private void logoutClient() {
        statusService.changeStatusIsOnline(currentUserId, false);
        clientService.setLogoutDate(clientID, OffsetDateTime.now(ZoneId.of("UTC")));
        broadcastContactChange();
    }

    private void changeUserStatus(Map<String, Object> content) {
        if (content.containsKey("status")) {
            statusService.changeUserStatus(currentUserId, (String) content.get("status"));
            broadcastContactChange();
        }
    }

    private void changeUserStatusDetails(Map<String, Object> content) {
        if (content.containsKey("statusDetails")) {
            statusService.changeUserStatusDetails(currentUserId, (String) content.get("statusDetails"));
            broadcastContactChange();
        }
    }

    private void broadcastContactChange() {
        List<String> clientIds = getClientIdListOfMyContacts();

        if (!clientIds.isEmpty()) {
            Server.getInstance()
                    .findAllClientThreadsByIds(clientIds)
                    .forEach(ClientThread::sendContactList);
        }
    }

    private List<String> getClientIdListOfMyContacts() {
        List<Long> allByContact = contactService
                .findAllByContact(currentUserId)
                .stream()
                .map(contact -> contact.getOwner().getId())
                .collect(Collectors.toList());

        if (!allByContact.isEmpty()) {
            return clientService
                    .findAllByUserId(allByContact)
                    .stream()
                    .map(Client::getClientId)
                    .collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

    private void broadcastSendContactList(List<String> clientIds) {
        if (!clientIds.isEmpty()) {
            Server.getInstance()
                    .findAllClientThreadsByIds(clientIds)
                    .forEach(ClientThread::sendContactList);

        }
    }

    private void sendCurrentUserInfo() {
        Map<String, Object> content = new HashMap<>();
        UserDetailsDTO userDetailsDTO = userService
                .findByClientId(clientID)
                .map(this::generateUserDetailsDTOFromCurrentUser)
                .orElse(null);

        try {
            content.put("me", objectMapper.writeValueAsString(userDetailsDTO));
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage());
        }

        write(new ServerCommunicationObject(ServerCommunicationObjectType.USER_ME, content));
        broadcastContactChange();
    }

    private void sendContactList() {
        Map<String, Object> content = new HashMap<>();
        List<UserDetailsDTO> contactList = contactService
                .findAllByOwner(currentUserId)
                .stream()
                .map(contact -> generateUserDetailsDTOFromUser(contact.getContact()))
                .collect(Collectors.toList());

        try {
            content.put("contactList", objectMapper.writeValueAsString(contactList));
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage());
        }

        write(new ServerCommunicationObject(ServerCommunicationObjectType.USER_LIST, content));
    }

    private void sendContactList(Long contactId) {
        broadcastSendContactList(findClientIdsByContactId(contactId));
    }

    private List<String> findClientIdsByContactId(Long contactId) {
        ArrayList<Long> contactIds = new ArrayList<>();
        contactIds.add(contactId);
        return clientService
                .findAllByUserId(contactIds)
                .stream()
                .map(Client::getClientId)
                .collect(Collectors.toList());
    }

    private void sendUserListWithFilters(Map<String, Object> receivedContent) {
        if (receivedContent.containsKey("filters")) {
            Map<String, Object> content = new HashMap<>();
            List<UserProfileDTO> userList = userService.getAllUserFullProfileByFilters(currentUserId, (Map<String, Object>) receivedContent.get("filters"));

            try {
                content.put("userList", objectMapper.writeValueAsString(userList));
            } catch (JsonProcessingException e) {
                logger.error(e.getMessage());
            }

            write(new ServerCommunicationObject(ServerCommunicationObjectType.USER_LIST_FILTERS, content));
        }
    }

    private void sendMessage(MessageDTO message) {
        Map<String, Object> content = new HashMap<>();
        try {
            content.put("message", objectMapper.writeValueAsString(message));
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage());
        }

        write(new ServerCommunicationObject(ServerCommunicationObjectType.MESSAGE_ADD, content));
    }

    private void sendUserDetails(Map<String, Object> receivedContent) {
        if (receivedContent.containsKey("userId")) {
            Map<String, Object> content = new HashMap<>();
            UserFullProfileDTO userFullProfileDTO = userService
                    .findById((Long) receivedContent.get("userId"))
                    .map(UserFullProfileDTO::new)
                    .orElse(null);
            try {
                content.put("userProfile", objectMapper.writeValueAsString(userFullProfileDTO));
            } catch (JsonProcessingException e) {
                logger.error(e.getMessage());
            }

            ServerCommunicationObject msg = new ServerCommunicationObject(ServerCommunicationObjectType.CONTACT_DETAILS, content);
            write(msg);
        }
    }

    private void sendInviteList() {
        Map<String, Object> content = new HashMap<>();

        List<InviteDTO> inviteList = contactService
                .findAllByContactOneSide(currentUserId)
                .stream()
                .map(contact -> new InviteDTO(contact.getOwner()))
                .collect(Collectors.toList());

        try {
            content.put("inviteList", objectMapper.writeValueAsString(inviteList));
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage());
        }

        ServerCommunicationObject msg = new ServerCommunicationObject(ServerCommunicationObjectType.INVITE_LIST, content);
        write(msg);
    }

    private void decideInvite(Map<String, Object> receivedContent) {
        if (receivedContent.containsKey("userId") && receivedContent.containsKey("action")) {
            Long userId = (Long) receivedContent.get("userId");
            String action = (String) receivedContent.get("action");

            switch (action) {
                case "ACCEPT":
                    contactService.addContact(currentUserId, userId);
                    sendContactList();
                    break;
                case "DISCARD":
                    contactService.deleteContact(userId, currentUserId);
                    sendContactList(userId);
                    break;
                case "BLOCK":
                    contactService.deleteContact(userId, currentUserId);
                    userService.addBan(currentUserId, userId);
                    sendContactList(userId);
                    sendMessageFromOthersList();
                    break;
            }

            sendInviteList();
        }
    }

    private UserDetailsDTO generateUserDetailsDTOFromUser(User user) {
        UserDetailsDTO dto = new UserDetailsDTO(user);
        Status status = statusService.findStatusByUserId(user.getId()).orElse(new Status(UserStatusType.OFFLINE, null));
        if (!status.isOnline()) {
            dto.setStatus(UserStatusType.OFFLINE);
        } else {
            dto.setStatus(status.getStatusType());
        }
        dto.setStatusDetails(status.getStatusDetails());
        return dto;
    }

    private UserDetailsDTO generateUserDetailsDTOFromCurrentUser(User user) {
        UserDetailsDTO dto = new UserDetailsDTO(user);
        Status status = statusService.findStatusByUserId(user.getId()).orElse(null);
        if (status != null) {
            dto.setStatus(status.getStatusType());
            dto.setStatusDetails(status.getStatusDetails());
        } else {
            dto.setStatus(UserStatusType.ONLINE);
            statusService.createStatus(user.getId(), UserStatusType.ONLINE);
        }
        return dto;
    }

    private boolean authorization() {
        try {
            ServerCommunicationObject cm = (ServerCommunicationObject) sInput.readObject();
            logger.info(cm.getContent());

            Map<String, Object> loginStatus = getLoginStatus(cm.getContent());
            ServerCommunicationObject statusMsg = new ServerCommunicationObject(ServerCommunicationObjectType.LOGIN, loginStatus);

            if ((boolean) loginStatus.get("status")) {
                clientService.saveNewClient(clientID, currentUserId);
                statusService.changeStatusIsOnline(currentUserId, true);
            }
            sOutput.writeObject(statusMsg);
            return true;
        } catch (IOException | ClassNotFoundException e) {
            logger.error(e.getMessage());
        }
        return false;
    }

    private Map<String, Object> getLoginStatus(Map<String, Object> content) {
        Map<String, Object> statusContent = new HashMap<>();
        currentUserId = userService.checkCredentials(String.valueOf(content.get("login")), String.valueOf(content.get("password")));
        if (currentUserId > 0) {
            statusContent.put("status", true);
        } else {
            statusContent.put("status", false);
        }
        return statusContent;
    }

    public void close() {
        try {
            if (sOutput != null) sOutput.close();
            if (sInput != null) sInput.close();
            if (socket != null) socket.close();
        } catch (Exception ignored) {
        }
        logger.info(String.format("{%s} disconnected", clientID));
    }

    boolean write(ServerCommunicationObject msg) {
        if (!socket.isConnected()) {
            close();
            return false;
        }
        try {
            sOutput.writeObject(msg);
            logger.info("SERVER::" + clientID + "::" + msg.getType() + "::" + msg.getContent());
            return true;
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        return false;
    }

    public String getClientID() {
        return clientID;
    }
}
