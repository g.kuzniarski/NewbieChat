package newbieChat.model;

import java.util.ArrayList;
import java.util.List;

public class MatchingTypes {
    public static final String MANY_HOUR_DAY = "5+ hours a day";
    public static final String FEW_HOUR_DAY = "few hours a day";
    public static final String FEW_HOURS_WEEK = "few hours a week";
    public static final String FEW_HOURS_NOT_EVERY_WEEK = "few hours not every week ";

    public static final String MORNING = "Morning";
    public static final String NOON = "Noon";
    public static final String EVENING = "Evening";
    public static final String NIGHT = "Night";

    public static final ArrayList<String> TIME_SPENT_TYPES = new ArrayList<String>() {{
        add(MANY_HOUR_DAY);
        add(FEW_HOUR_DAY);
        add(FEW_HOURS_NOT_EVERY_WEEK);
        add(FEW_HOURS_WEEK);
    }};

    public static final ArrayList<String> TIME_OF_DAY_TYPES = new ArrayList<String>() {{
        add(MORNING);
        add(NOON);
        add(EVENING);
        add(NIGHT);
    }};

    public static List<String> getNeighbors(ArrayList<String> strings, String element, int depth) {
        int index = strings.indexOf(element);
        int numberOfElements = depth * 2 + 1;
        if (index > -1 && numberOfElements < strings.size()) {
            List<String> result = new ArrayList<>();

            for (int i = -depth; i <= depth; i++) {
                if (index + i >= strings.size()) {
                    index = -i;
                    result.add(strings.get(index + i));
                } else if (index + i < 0) {
                    result.add(strings.get(index + i + strings.size()));
                } else {
                    result.add(strings.get(index + i));
                }
            }

            return result;
        } else {
            return strings;
        }
    }
}
