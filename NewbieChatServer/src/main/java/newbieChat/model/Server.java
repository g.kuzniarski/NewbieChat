package newbieChat.model;

import newbieChat.controller.ServerMainController;
import newbieChat.enums.ServerCommunicationObjectType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class Server implements Runnable {

    private static Server instance;

    private static final Logger logger = LogManager.getLogger(ServerMainController.class);

    private int port;
    private ServerMainController parentController;
    private Map<String, ClientThread> clientThreads;
    private boolean isServerRunning;

    private Server(int port, ServerMainController parentController) {
        this.port = port;
        this.parentController = parentController;
        this.clientThreads = new LinkedHashMap<>();
    }

    public static Server createInstance(int port, ServerMainController serverMainController) {
        if (instance == null) {
            instance = new Server(port, serverMainController);
        }
        return instance;
    }

    public static Server getInstance() {
        return instance;
    }

    public void run() {
        isServerRunning = true;
        ServerSocket serverSocket;

        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            logger.error(e.getMessage());
            return;
        }

        logger.info("Server is running on port " + port);
        while (isServerRunning) {
            Socket socket = null;
            try {
                socket = serverSocket.accept();
            } catch (IOException e) {
                logger.error(e.getMessage());
            }

            if (!isServerRunning)
                break;

            String clientID = UUID.randomUUID().toString();
            ClientThread t = new ClientThread(
                    socket,
                    clientID);
            assignServices(t);
            t.start();

            clientThreads.put(clientID, t);
        }

        clientThreads.forEach((s, clientThread) -> clientThread.write(new ServerCommunicationObject(ServerCommunicationObjectType.LOGOUT, null)));

        try {
            serverSocket.close();
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        clientThreads.forEach((s, clientThread) -> clientThread.close());
    }

    private void assignServices(ClientThread t) {
        t.setClientService(parentController.getClientService());
        t.setContactService(parentController.getContactService());
        t.setMatchService(parentController.getMatchService());
        t.setMessageService(parentController.getMessageService());
        t.setStatusService(parentController.getStatusService());
        t.setUserService(parentController.getUserService());
        t.setConfigGameProfileService(parentController.getConfigGameProfileService());
    }

    public void destroyServer() {
        isServerRunning = false;
        try {
            new Socket("localhost", port);
        } catch (Exception ignored) {
        }
    }

    public Map<String, ClientThread> getClientThreads() {
        return clientThreads;
    }

    public List<ClientThread> findAllClientThreadsByIds(List<String> clientIds) {
        return clientThreads
                .entrySet()
                .stream()
                .filter(stringClientThreadEntry -> clientIds.contains(stringClientThreadEntry.getKey()))
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
    }

    public boolean isServerRunning() {
        return isServerRunning;
    }

    public int getPort() {
        return port;
    }
}
