package newbieChat.service;

import newbieChat.entity.Match;

import java.util.Optional;

public interface MatchService {
    Optional<Match> createMatch(Long user1, Long user2);

    Optional<Match> addDecisionToMatch(Long owner, Long matched, boolean decision);

    Optional<Match> findAnyMatch(Long owner);
}
