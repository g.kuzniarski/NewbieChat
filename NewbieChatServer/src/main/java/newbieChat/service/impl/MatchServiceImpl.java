package newbieChat.service.impl;

import newbieChat.entity.GameProfile;
import newbieChat.entity.Match;
import newbieChat.entity.MatchtId;
import newbieChat.entity.User;
import newbieChat.model.MatchingTypes;
import newbieChat.repository.ContactRepository;
import newbieChat.repository.MatchRepository;
import newbieChat.repository.UserRepository;
import newbieChat.service.MatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Service("MatchService")
public class MatchServiceImpl implements MatchService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    MatchRepository matchRepository;

    @Autowired
    ContactRepository contactRepository;

    @Override
    @Transactional
    public Optional<Match> createMatch(Long user1, Long user2) {
        Match match = null;
        Set<Long> ids = new HashSet<Long>() {{
            add(user1);
            add(user2);
        }};

        List<User> users = userRepository.findAllByIdIn(ids);
        if (users.size() == 2 && (!users.get(0).getUserBanList().contains(users.get(1))
                && !users.get(1).getUserBanList().contains(users.get(0)))) {
            match = new Match();
            if (users.get(0).getId().equals(user1)) {
                match.setUser1(users.get(0));
            } else if (users.get(0).getId().equals(user2)) {
                match.setUser2(users.get(0));
            }
            if (users.get(1).getId().equals(user1)) {
                match.setUser1(users.get(1));
            } else if (users.get(1).getId().equals(user2)) {
                match.setUser2(users.get(1));
            }

            if (match.getUser1() != null && match.getUser2() != null) {
                match.setId(new MatchtId(match.getUser1().getId(), match.getUser2().getId()));
                match.setCreatedDate(OffsetDateTime.now(ZoneId.of("UTC")));
                match = matchRepository.save(match);
            }
        }

        return Optional.ofNullable(match);
    }

    @Override
    @Transactional
    public Optional<Match> addDecisionToMatch(Long owner, Long matched, boolean decision) {
        return matchRepository
                .findById(owner, matched)
                .map(match -> {
                    if (match.getUser1().getId().equals(owner)) {
                        match.setAgree1(decision);
                        return match;
                    } else if (match.getUser2().getId().equals(owner)) {
                        match.setAgree2(decision);
                        return match;
                    } else {
                        return null;
                    }
                });
    }

    @Override
    @Transactional
    public Optional<Match> findAnyMatch(Long owner) {
        Optional<Match> matchForUser = matchRepository.findMatchForUser(owner);
        if (matchForUser.isPresent()) {
            return matchForUser;
        } else {
            return matchProcess(owner);
        }
    }

    @Transactional
    Optional<Match> matchProcess(Long owner) {
        return userRepository
                .findById(owner)
                .map(currentUser -> {
                    List<User> matchingUserList = userRepository.findMatchingUser(
                            currentUser.getProfile().getGamingStyle(),
                            getMatchingTimeOfDay(currentUser.getProfile().getTimeOfDay()),
                            getMatchingTimeSpent(currentUser.getProfile().getTimeSpent()),
                            getMatchingGameProfileList(currentUser.getGameProfileList())
                    );

                    if (!matchingUserList.isEmpty()) {
                        List<Long> contactList = contactRepository
                                .findAllByUserId(owner)
                                .stream()
                                .map(contact -> {
                                    if (contact.getOwner().getId().equals(owner)) {
                                        return contact.getContact().getId();
                                    } else {
                                        return contact.getOwner().getId();
                                    }
                                })
                                .collect(Collectors.toList());

                        matchingUserList = matchingUserList.stream()
                                .filter(user -> !contactList.contains(user.getId()))
                                .collect(Collectors.toList());

                        User user = matchingUserList.get(new Random().nextInt(matchingUserList.size()));
                        return createMatch(owner, user.getId())
                                .orElse(null);
                    } else {
                        return null;
                    }
                });
    }

    private List<String> getMatchingTimeOfDay(String timeOfDay) {
        return MatchingTypes.getNeighbors(MatchingTypes.TIME_OF_DAY_TYPES, timeOfDay, 1);
    }

    private List<String> getMatchingTimeSpent(String timeSpent) {
        return MatchingTypes.getNeighbors(MatchingTypes.TIME_SPENT_TYPES, timeSpent, 1);
    }

    private List<String> getMatchingGameProfileList(List<GameProfile> gameProfileList) {
        return gameProfileList
                .stream()
                .map(gameProfile -> gameProfile.getConfigGameProfile().getGame())
                .collect(Collectors.toList());
    }
}

