package newbieChat.service.impl;

import newbieChat.entity.Client;
import newbieChat.repository.ClientRepository;
import newbieChat.repository.UserRepository;
import newbieChat.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service("ClientService")
public class ClientServiceImpl implements ClientService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    ClientRepository clientRepository;

    @Override
    @Transactional
    public Optional<Client> saveNewClient(String clientID, Long userId) {
        return userRepository
                .findById(userId)
                .map(user -> {
                    Client client = new Client();
                    client.setClientId(clientID);
                    client.setLoginDate(OffsetDateTime.now(ZoneId.of("UTC")));
                    client.setUser(user);
                    return clientRepository.save(client);
                });
    }

    @Override
    @Transactional
    public Optional<Client> setLogoutDate(String clientID, OffsetDateTime dateTime) {
        return clientRepository
                .findByClientId(clientID)
                .map(client -> {
                    client.setLogoutDate(dateTime);
                    return client;
                });
    }

    @Override
    @Transactional(readOnly = true)
    public List<Client> findAllByUserId(Collection<Long> userList) {
        return clientRepository.findAllByUserIn(userList);
    }
}
