package newbieChat.service.impl;

import newbieChat.entity.Contact;
import newbieChat.entity.ContactId;
import newbieChat.entity.User;
import newbieChat.repository.ContactRepository;
import newbieChat.repository.UserRepository;
import newbieChat.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service("ContactService")
public class ContactServiceImpl implements ContactService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    ContactRepository contactRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Contact> findAllByOwner(Long ownerId) {
        return userRepository
                .findById(ownerId)
                .map(owner -> contactRepository
                        .findAllByOwner(owner)
                        .stream()
                        .filter(contact -> !owner.getUserBanList().contains(contact.getContact())
                                && !contact.getContact().getUserBanList().contains(owner))
                        .collect(Collectors.toList())
                )
                .orElse(new ArrayList<>());
    }

    @Override
    @Transactional(readOnly = true)
    public List<Contact> findAllByContact(Long contactId) {
        return userRepository
                .findById(contactId)
                .map(contact -> contactRepository.findAllByContact(contact))
                .orElse(new ArrayList<>());
    }

    @Override
    @Transactional
    public Optional<Contact> addContact(Long ownerId, Long contactId) {
        Contact result = null;
        Set<Long> ids = new HashSet<Long>() {{
            add(ownerId);
            add(contactId);
        }};

        List<User> users = userRepository.findAllByIdIn(ids);
        User owner = null, contact = null;
        if (users.size() == 2 && (!users.get(0).getUserBanList().contains(users.get(1))
                && !users.get(1).getUserBanList().contains(users.get(0)))) {
            if (users.get(0).getId().equals(ownerId)) {
                owner = users.get(0);
            } else if (users.get(0).getId().equals(contactId)) {
                contact = users.get(0);
            }
            if (users.get(1).getId().equals(ownerId)) {
                owner = users.get(1);
            } else if (users.get(1).getId().equals(contactId)) {
                contact = users.get(1);
            }

            if (owner != null && contact != null) {
                result = contactRepository.save(new Contact(
                        new ContactId(owner.getId(), contact.getId()),
                        owner,
                        contact));
            }
        }

        return Optional.ofNullable(result);
    }

    @Override
    public void deleteContact(Long ownerId, Long contactId) {
        try {
            contactRepository.deleteById(new ContactId(ownerId, contactId));
        } catch (EmptyResultDataAccessException ignored) {
        }
        try {
            contactRepository.deleteById(new ContactId(contactId, ownerId));
        } catch (EmptyResultDataAccessException ignored) {
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Contact> findAllByContactOneSide(Long userId) {
        List<Contact> allByContact = contactRepository.findAllByContact_Id(userId);
        List<Long> myContacts = contactRepository
                .findAllByOwner_Id(userId)
                .stream()
                .map(contact -> contact.getContact().getId())
                .collect(Collectors.toList());

        return allByContact
                .stream()
                .filter(contact -> !myContacts.contains(contact.getOwner().getId()))
                .collect(Collectors.toList());
    }

    @Override
    public void addContacts(User user1, User user2) {
        contactRepository.save(new Contact(new ContactId(user1.getId(), user2.getId()), user1, user2));
        contactRepository.save(new Contact(new ContactId(user2.getId(), user1.getId()), user2, user1));
    }
}
