package newbieChat.service.impl;

import newbieChat.dto.UserProfileDTO;
import newbieChat.entity.Message;
import newbieChat.entity.User;
import newbieChat.repository.MessageRepository;
import newbieChat.repository.UserRepository;
import newbieChat.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Service("MessageService")
public class MessageServiceImpl implements MessageService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    MessageRepository messageRepository;

    @Override
    @Transactional
    public Optional<Message> createMessage(Long sendFrom, Long sendTo, String messageContent) {
        Message message = null;
        Set<Long> ids = new HashSet<Long>() {{
            add(sendFrom);
            add(sendTo);
        }};

        List<User> users = userRepository.findAllByIdIn(ids);
        if (users.size() == 2 && (!users.get(0).getUserBanList().contains(users.get(1))
                && !users.get(1).getUserBanList().contains(users.get(0)))) {
            message = new Message();
            message.setContent(messageContent);
            message.setSendDate(OffsetDateTime.now(ZoneId.of("UTC")));

            if (users.get(0).getId().equals(sendFrom)) {
                message.setSendFrom(users.get(0));
            } else if (users.get(0).getId().equals(sendTo)) {
                message.setSendTo(users.get(0));
            }
            if (users.get(1).getId().equals(sendFrom)) {
                message.setSendFrom(users.get(1));
            } else if (users.get(1).getId().equals(sendTo)) {
                message.setSendTo(users.get(1));
            }

            if (message.getSendFrom() != null && message.getSendTo() != null) {
                message = messageRepository.saveAndFlush(message);
            }
        }

        return Optional.ofNullable(message);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Message> getMessageListSendBetween(Long sendFrom, Long sendTo) {
        return messageRepository.findAllBySendFromAndSendToOrderBySendDateDesc(sendFrom, sendTo);
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserProfileDTO> getOtherMessageContactList(Long userId) {
        return messageRepository
                .findAllBySendFromOrSendTo(userId)
                .stream()
                .map(message -> {
                    if (!message.getSendFrom().getUserBanList().contains(message.getSendTo())
                            && !message.getSendTo().getUserBanList().contains(message.getSendFrom())) {
                        if (userId.equals(message.getSendTo().getId())) {
                            return message.getSendFrom();
                        } else {
                            return message.getSendTo();
                        }
                    }
                    return null;
                })
                .distinct()
                .filter(Objects::nonNull)
                .map(UserProfileDTO::new)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserProfileDTO> getOtherMessageContactList(Long userId, Collection<Long> contactList) {
        return messageRepository
                .findAllBySendFromOrSendToAndNotIn(userId, contactList)
                .stream()
                .map(message -> userId.equals(message.getSendTo().getId())
                        ? message.getSendFrom() : message.getSendTo())
                .distinct()
                .map(UserProfileDTO::new)
                .collect(Collectors.toList());
    }

    @Override
    public long countAll() {
        return messageRepository.count();
    }

    @Override
    public long countLast24h() {
        return messageRepository.countLast24h();
    }
}

