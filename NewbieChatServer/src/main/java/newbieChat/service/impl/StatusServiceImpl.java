package newbieChat.service.impl;

import newbieChat.entity.Status;
import newbieChat.entity.User;
import newbieChat.enums.UserStatusType;
import newbieChat.repository.UserRepository;
import newbieChat.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service("StatusService")
public class StatusServiceImpl implements StatusService {

    @Autowired
    UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public Optional<Status> findStatusByUserId(Long id) {
        return userRepository.findById(id).map(User::getStatus);
    }

    @Override
    @Transactional
    public void changeStatusIsOnline(Long userId, boolean isOnline) {
        userRepository.findById(userId).map(user -> {
            user.getStatus().setOnline(isOnline);
            return user;
        });
    }

    @Override
    @Transactional
    public Optional<Status> createStatus(Long userId, UserStatusType statusType) {
        return userRepository
                .findById(userId)
                .map(user -> {
                    Status status = new Status();
                    status.setOnline(true);
                    status.setStatusType(UserStatusType.ONLINE);
                    status.setStatusDetails(null);
                    user.setStatus(status);
                    return status;
                });
    }

    @Override
    @Transactional
    public void changeUserStatus(Long userId, String statusType) {
        userRepository
                .findById(userId)
                .map(user -> {
                    user.getStatus().setStatusType(UserStatusType.valueOf(statusType.toUpperCase()));
                    return user.getStatus();
                });
    }

    @Override
    @Transactional
    public void changeUserStatusDetails(Long userId, String statusDetails) {
        userRepository
                .findById(userId)
                .map(user -> {
                    user.getStatus().setStatusDetails(statusDetails);
                    return user.getStatus();
                });
    }

    @Override
    public long countOnline() {
        return userRepository.countByOnline();
    }
}

