package newbieChat.service.impl;

import newbieChat.dto.GameProfileDTO;
import newbieChat.dto.UserFullProfileDTO;
import newbieChat.dto.UserProfileDTO;
import newbieChat.entity.Client;
import newbieChat.entity.ConfigGameProfile;
import newbieChat.entity.GameProfile;
import newbieChat.entity.User;
import newbieChat.enums.Language;
import newbieChat.enums.UserStatusType;
import newbieChat.repository.*;
import newbieChat.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("UserService")
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    ContactRepository contactRepository;

    @Autowired
    ProfileRepository profileRepository;

    @Autowired
    GameProfileRepository gameProfileRepository;

    @Autowired
    ConfigGameProfileRepository configGameProfileRepository;

    private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(12);

    @Override
    @Transactional(readOnly = true)
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<User> findByClientId(String clientId) {
        return clientRepository.findByClientId(clientId).map(Client::getUser);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<User> findByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public UserProfileDTO getUserProfile(Long userId) {
        Optional<User> user = userRepository.findById(userId);
        return new UserProfileDTO(user.orElse(null));
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserProfileDTO> getAllUserFullProfileByFilters(Long currentUserId, Map<String, Object> filters) {
        return userRepository
                .findById(currentUserId)
                .map(currentUser -> userRepository
                        .findAllByFilters(filters)
                        .stream()
                        .filter(user -> !user.getId().equals(currentUserId) && !user.getUserBanList().contains(currentUser)
                                && !currentUser.getUserBanList().contains(user))
                        .map(UserProfileDTO::new)
                        .collect(Collectors.toList())
                ).orElse(new ArrayList<>());
    }

    @Override
    @Transactional
    public Optional<User> updateUser(UserFullProfileDTO userFullProfileDTO) {
        return userRepository
                .findById(userFullProfileDTO.getId())
                .map(user -> {
                    user.setName(userFullProfileDTO.getName());
                    user.setAvatarUrl(userFullProfileDTO.getAvatarPath());
                    if (userFullProfileDTO.getOldPassword() != null && !userFullProfileDTO.getOldPassword().isEmpty()
                            && bCryptPasswordEncoder.matches(userFullProfileDTO.getOldPassword(), user.getPassword()))
                        user.setPassword(bCryptPasswordEncoder.encode(userFullProfileDTO.getNewPassword()));
                    if (Language.contains(userFullProfileDTO.getLanguage())) {
                        user.setLanguage(Language.valueOf(userFullProfileDTO.getLanguage()));
                    }

                    user.getProfile().setCountry(userFullProfileDTO.getCountry());
                    user.getProfile().setCity(userFullProfileDTO.getCity());
                    user.getProfile().setAge(userFullProfileDTO.getAge());
                    user.getProfile().setGamingStyle(userFullProfileDTO.getGamingStyle());
                    user.getProfile().setTimeOfDay(userFullProfileDTO.getTimeOfDay());
                    user.getProfile().setTimeSpent(userFullProfileDTO.getTimeSpent());

                    List<ConfigGameProfile> configGameProfileList = configGameProfileRepository.findAllById(
                            userFullProfileDTO
                                    .getGameProfileList()
                                    .stream()
                                    .map(GameProfileDTO::getGame)
                                    .collect(Collectors.toList()));

                    List<GameProfile> gameProfileList = userFullProfileDTO.
                            getGameProfileList()
                            .stream()
                            .map(gameProfileDTO -> {
                                GameProfile gameProfile = new GameProfile();
                                gameProfile.setDetails(gameProfileDTO.getDetails());
                                gameProfile.setConfigGameProfile(
                                        configGameProfileList
                                                .stream()
                                                .filter(configGameProfile ->
                                                        configGameProfile
                                                                .getGame()
                                                                .equals(gameProfileDTO.getGame()))
                                                .findFirst()
                                                .orElse(null));
                                return gameProfile;
                            }).collect(Collectors.toList());

                    gameProfileRepository.deleteAll(user.getGameProfileList());
                    user.getGameProfileList().clear();
                    user.setGameProfileList(gameProfileList);

                    return user;
                });
    }

    @Override
    @Transactional
    public void deleteUser(Long currentUserId) {
        userRepository
                .findById(currentUserId)
                .map(user -> {
                    user.setLogin("deleted_account_" + user.getId());
                    user.setPassword(null);
                    user.setAvatarUrl(null);
                    user.setName(null);
                    user.setBirthday(null);

                    user.getProfile().setCountry(null);
                    user.getProfile().setAge(null);
                    user.getProfile().setCity(null);
                    user.getProfile().setGamingStyle(null);
                    user.getProfile().setTimeOfDay(null);
                    user.getProfile().setTimeSpent(null);
                    user.getProfile().setSex(null);

                    user.getGameProfileList().stream().map(gameProfile -> {
                        gameProfile.setDetails(null);
                        return gameProfile;
                    });

                    user.getStatus().setStatusDetails(null);
                    user.getStatus().setStatusType(UserStatusType.OFFLINE);
                    user.getStatus().setOnline(false);
                    return user;
                });

        contactRepository.deleteAllByOwner_Id(currentUserId);
        contactRepository.deleteAllByContact_Id(currentUserId);
    }

    @Override
    @Transactional
    public Optional<User> addBan(Long currentUserId, Long userId) {
        return userRepository
                .findById(userId)
                .map(user -> {
                    userRepository.findById(currentUserId).ifPresent(user1 -> user.getUserBanList().add(user1));
                    return user;
                });
    }

    @Override
    public long countAll() {
        return userRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public Long checkCredentials(String login, String password) {
        return userRepository
                .findByLogin(login)
                .map(user -> {
                    if (bCryptPasswordEncoder.matches(password, user.getPassword())) {
                        return user.getId();
                    } else {
                        return -1L;
                    }
                })
                .orElse(-1L);
    }
}
