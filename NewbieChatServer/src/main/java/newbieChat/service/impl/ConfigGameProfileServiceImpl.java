package newbieChat.service.impl;

import newbieChat.entity.ConfigGameProfile;
import newbieChat.repository.ConfigGameProfileRepository;
import newbieChat.service.ConfigGameProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("ConfigGameProfileService")
public class ConfigGameProfileServiceImpl implements ConfigGameProfileService {

    @Autowired
    ConfigGameProfileRepository configGameProfileRepository;

    @Override
    @Transactional(readOnly = true)
    public List<ConfigGameProfile> findAll() {
        return configGameProfileRepository.findAll();
    }
}
