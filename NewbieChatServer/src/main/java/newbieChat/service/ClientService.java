package newbieChat.service;

import newbieChat.entity.Client;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface ClientService {
    Optional<Client> saveNewClient(String clientID, Long userId);

    Optional<Client> setLogoutDate(String clientID, OffsetDateTime dateTime);

    List<Client> findAllByUserId(Collection<Long> userList);
}
