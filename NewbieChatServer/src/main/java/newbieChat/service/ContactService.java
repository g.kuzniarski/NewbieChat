package newbieChat.service;

import newbieChat.entity.Contact;
import newbieChat.entity.User;

import java.util.List;
import java.util.Optional;

public interface ContactService {
    List<Contact> findAllByOwner(Long ownerId);

    List<Contact> findAllByContact(Long currentUserId);

    Optional<Contact> addContact(Long ownerId, Long contactId);

    void deleteContact(Long ownerId, Long contactId);

    List<Contact> findAllByContactOneSide(Long userId);

    void addContacts(User user1, User user2);
}
