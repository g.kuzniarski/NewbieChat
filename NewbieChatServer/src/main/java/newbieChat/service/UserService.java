package newbieChat.service;

import newbieChat.dto.UserFullProfileDTO;
import newbieChat.dto.UserProfileDTO;
import newbieChat.entity.User;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface UserService {

    Optional<User> findById(Long currentUserId);

    Optional<User> findByClientId(String clientID);

    Optional<User> findByLogin(String login);

    List<User> getAllUsers();

    Long checkCredentials(String login, String password);

    UserProfileDTO getUserProfile(Long userId);

    List<UserProfileDTO> getAllUserFullProfileByFilters(Long currentUserId, Map<String, Object> filters);

    Optional<User> updateUser(UserFullProfileDTO userFullProfileDTO);

    void deleteUser(Long currentUserId);

    Optional<User> addBan(Long currentUserId, Long userId);

    long countAll();
}
