package newbieChat.service;

import newbieChat.entity.Status;
import newbieChat.enums.UserStatusType;

import java.util.Optional;

public interface StatusService {
    Optional<Status> findStatusByUserId(Long id);

    void changeStatusIsOnline(Long userId, boolean isOnline);

    Optional<Status> createStatus(Long userId, UserStatusType statusType);

    void changeUserStatus(Long userId, String status);

    void changeUserStatusDetails(Long userId, String statusDetails);

    long countOnline();
}
