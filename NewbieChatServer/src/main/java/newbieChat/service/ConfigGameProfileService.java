package newbieChat.service;

import newbieChat.entity.ConfigGameProfile;

import java.util.List;

public interface ConfigGameProfileService {
    List<ConfigGameProfile> findAll();
}
