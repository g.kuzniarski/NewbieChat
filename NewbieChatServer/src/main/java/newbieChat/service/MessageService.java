package newbieChat.service;

import newbieChat.dto.UserProfileDTO;
import newbieChat.entity.Message;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface MessageService {
    Optional<Message> createMessage(Long sendFrom, Long sendTo, String messageContent);

    List<Message> getMessageListSendBetween(Long sendFrom, Long sendTo);

    List<UserProfileDTO> getOtherMessageContactList(Long currentUserId);

    List<UserProfileDTO> getOtherMessageContactList(Long currentUserId, Collection<Long> contactList);

    long countAll();
    long countLast24h();
}
