package newbieChat.entity;

import newbieChat.dto.GameProfileDTO;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "game_profile")
public class GameProfile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @ManyToOne(cascade = CascadeType.MERGE, targetEntity = ConfigGameProfile.class)
    @JoinColumn(name = "config_game_profile", nullable = false)
    private ConfigGameProfile configGameProfile;

    @Column(name = "details", length = 4096)
    private String details;

    public GameProfile() {
    }

    public GameProfile(ConfigGameProfile configGameProfile, String game, String details) {
        this.configGameProfile = configGameProfile;
        this.details = details;
    }

    public GameProfile(GameProfileDTO gameProfileDTO) {
        this.configGameProfile = new ConfigGameProfile(gameProfileDTO.getGame());
        this.details = gameProfileDTO.getDetails();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ConfigGameProfile getConfigGameProfile() {
        return configGameProfile;
    }

    public void setConfigGameProfile(ConfigGameProfile configGameProfile) {
        this.configGameProfile = configGameProfile;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameProfile that = (GameProfile) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(configGameProfile, that.configGameProfile) &&
                Objects.equals(details, that.details);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, configGameProfile, details);
    }

    @Override
    public String toString() {
        return "GameProfile{" +
                "id=" + id +
                ", configGameProfile=" + configGameProfile +
                ", details='" + details + '\'' +
                '}';
    }
}
