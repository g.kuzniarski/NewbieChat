package newbieChat.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import newbieChat.util.OffsetDateTimeDeserializer;

import javax.persistence.*;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.Objects;

@Entity
@Table(name = "client")
public class Client implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(cascade = CascadeType.MERGE, targetEntity = User.class)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "client_id", nullable = false)
    private String clientId;

    @Column(name = "login_date", columnDefinition = "timestamp with time zone")
    @JsonSerialize(using = ToStringSerializer.class)
    @JsonDeserialize(using = OffsetDateTimeDeserializer.class)
    @JsonFormat(timezone = "UTC")
    private OffsetDateTime loginDate;

    @Column(name = "logout_date", columnDefinition = "timestamp with time zone")
    @JsonSerialize(using = ToStringSerializer.class)
    @JsonDeserialize(using = OffsetDateTimeDeserializer.class)
    @JsonFormat(timezone = "UTC")
    private OffsetDateTime logoutDate;

    public Client() {
    }

    public Client(User user, String clientId, OffsetDateTime loginDate) {
        this.user = user;
        this.clientId = clientId;
        this.loginDate = loginDate;
    }

    public Client(User user, String clientId, OffsetDateTime loginDate, OffsetDateTime logoutDate) {
        this.user = user;
        this.clientId = clientId;
        this.loginDate = loginDate;
        this.logoutDate = logoutDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public OffsetDateTime getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(OffsetDateTime loginDate) {
        this.loginDate = loginDate;
    }

    public OffsetDateTime getLogoutDate() {
        return logoutDate;
    }

    public void setLogoutDate(OffsetDateTime logoutDate) {
        this.logoutDate = logoutDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Objects.equals(id, client.id) &&
                Objects.equals(user, client.user) &&
                Objects.equals(clientId, client.clientId) &&
                Objects.equals(loginDate, client.loginDate) &&
                Objects.equals(logoutDate, client.logoutDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, user, clientId, loginDate, logoutDate);
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", user=" + user +
                ", clientId='" + clientId + '\'' +
                ", loginDate=" + loginDate +
                ", logoutDate=" + logoutDate +
                '}';
    }
}
