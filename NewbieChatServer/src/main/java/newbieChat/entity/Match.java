package newbieChat.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import newbieChat.util.OffsetDateTimeDeserializer;

import javax.persistence.*;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.Objects;

@Entity
@Table(name = "match")
public class Match implements Serializable {

    @EmbeddedId
    private MatchtId id;

    @ManyToOne(cascade = CascadeType.MERGE, targetEntity = User.class)
    @JoinColumn(name = "user1", insertable = false, updatable = false)
    private User user1;

    @ManyToOne(cascade = CascadeType.MERGE, targetEntity = User.class)
    @JoinColumn(name = "user2", insertable = false, updatable = false)
    private User user2;

    @Column(name = "agree1")
    private Boolean agree1;

    @Column(name = "agree2")
    private Boolean agree2;

    @Column(name = "created_date", columnDefinition = "timestamp with time zone")
    @JsonSerialize(using = ToStringSerializer.class)
    @JsonDeserialize(using = OffsetDateTimeDeserializer.class)
    @JsonFormat(timezone = "UTC")
    private OffsetDateTime createdDate;

    public Match() {
    }

    public Match(MatchtId id, User user1, User user2, Boolean agree1, Boolean agree2, OffsetDateTime createdDate) {
        this.id = id;
        this.user1 = user1;
        this.user2 = user2;
        this.agree1 = agree1;
        this.agree2 = agree2;
        this.createdDate = createdDate;
    }

    public MatchtId getId() {
        return id;
    }

    public void setId(MatchtId id) {
        this.id = id;
    }

    public User getUser1() {
        return user1;
    }

    public void setUser1(User user1) {
        this.user1 = user1;
    }

    public User getUser2() {
        return user2;
    }

    public void setUser2(User user2) {
        this.user2 = user2;
    }

    public Boolean isAgree1() {
        return agree1;
    }

    public void setAgree1(Boolean agree1) {
        this.agree1 = agree1;
    }

    public Boolean isAgree2() {
        return agree2;
    }

    public void setAgree2(Boolean agree2) {
        this.agree2 = agree2;
    }

    public OffsetDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(OffsetDateTime createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Match match = (Match) o;
        return agree1 == match.agree1 &&
                agree2 == match.agree2 &&
                Objects.equals(id, match.id) &&
                Objects.equals(user1, match.user1) &&
                Objects.equals(user2, match.user2) &&
                Objects.equals(createdDate, match.createdDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, user1, user2, agree1, agree2, createdDate);
    }

    @Override
    public String toString() {
        return "Match{" +
                "id=" + id +
                ", user1=" + user1 +
                ", user2=" + user2 +
                ", agree1=" + agree1 +
                ", agree2=" + agree2 +
                ", createdDate=" + createdDate +
                '}';
    }
}
