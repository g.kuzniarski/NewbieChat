package newbieChat.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import newbieChat.enums.Language;
import newbieChat.util.OffsetDateTimeDeserializer;

import javax.persistence.*;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "users")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "login", updatable = false, nullable = false)
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "name")
    private String name;

    @Column(name = "avatar_url")
    private String avatarUrl;

    @Column(name = "birthday", columnDefinition = "timestamp with time zone")
    @JsonSerialize(using = ToStringSerializer.class)
    @JsonDeserialize(using = OffsetDateTimeDeserializer.class)
    @JsonFormat(timezone = "UTC")
    private OffsetDateTime birthday;

    @Column(name = "language")
    @Enumerated(EnumType.STRING)
    private Language language;

    @OneToOne(cascade = CascadeType.ALL, targetEntity = Profile.class)
    @JoinColumn(name = "profile_id", unique = true)
    private Profile profile;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, targetEntity = GameProfile.class)
    private List<GameProfile> gameProfileList;

    @OneToOne(cascade = CascadeType.ALL, targetEntity = Status.class)
    @JoinColumn(name = "status_id", unique = true)
    private Status status;

    @ManyToMany
    @JoinTable(name = "users_ban_list")
    private List<User> userBanList;

    public User() {
    }

    public User(String login, String password, String name, String avatarUrl, OffsetDateTime birthday, Language language, Profile profile, List<GameProfile> gameProfileList, Status status, List<User> userBanList) {
        this.login = login;
        this.password = password;
        this.name = name;
        this.avatarUrl = avatarUrl;
        this.birthday = birthday;
        this.language = language;
        this.profile = profile;
        this.gameProfileList = gameProfileList;
        this.status = status;
        this.userBanList = userBanList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public OffsetDateTime getBirthday() {
        return birthday;
    }

    public void setBirthday(OffsetDateTime birthday) {
        this.birthday = birthday;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public List<GameProfile> getGameProfileList() {
        return gameProfileList;
    }

    public void setGameProfileList(List<GameProfile> gameProfileList) {
        this.gameProfileList = gameProfileList;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<User> getUserBanList() {
        return userBanList;
    }

    public void setUserBanList(List<User> userBanList) {
        this.userBanList = userBanList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(login, user.login) &&
                Objects.equals(password, user.password) &&
                Objects.equals(name, user.name) &&
                Objects.equals(avatarUrl, user.avatarUrl) &&
                Objects.equals(birthday, user.birthday) &&
                language == user.language &&
                Objects.equals(profile, user.profile) &&
                Objects.equals(gameProfileList, user.gameProfileList) &&
                Objects.equals(status, user.status) &&
                Objects.equals(userBanList, user.userBanList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, password, name, avatarUrl, birthday, language, profile, gameProfileList, status, userBanList);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", avatarUrl='" + avatarUrl + '\'' +
                ", birthday=" + birthday +
                ", language=" + language +
                ", profile=" + profile +
                ", gameProfileList=" + gameProfileList +
                ", status=" + status +
                ", userBanList=" + userBanList +
                '}';
    }
}
