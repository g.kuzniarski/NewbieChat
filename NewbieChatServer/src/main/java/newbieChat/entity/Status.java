package newbieChat.entity;

import newbieChat.enums.UserStatusType;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "status")
public class Status {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "status_type", nullable = false)
    private UserStatusType statusType;

    @Column(name = "status_details")
    private String statusDetails;

    @Column(name = "online", nullable = false)
    private boolean online;

    public Status() {
    }

    public Status(UserStatusType statusType, String statusDetails) {
        this.statusType = statusType;
        this.statusDetails = statusDetails;
    }

    public Status(User user, UserStatusType statusType, String statusDetails, boolean online) {
        this.statusType = statusType;
        this.statusDetails = statusDetails;
        this.online = online;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserStatusType getStatusType() {
        return statusType;
    }

    public void setStatusType(UserStatusType statusType) {
        this.statusType = statusType;
    }

    public String getStatusDetails() {
        return statusDetails;
    }

    public void setStatusDetails(String statusDetails) {
        this.statusDetails = statusDetails;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Status status = (Status) o;
        return online == status.online &&
                Objects.equals(id, status.id) &&
                statusType == status.statusType &&
                Objects.equals(statusDetails, status.statusDetails);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, statusType, statusDetails, online);
    }

    @Override
    public String toString() {
        return "Status{" +
                "id=" + id +
                ", statusType=" + statusType +
                ", statusDetails='" + statusDetails + '\'' +
                ", online=" + online +
                '}';
    }
}
