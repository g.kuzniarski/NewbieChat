package newbieChat.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "config_game_profile")
public class ConfigGameProfile {

    @Id
    @Column(name = "game")
    private String game;

    @Column(name = "details", length = 2000)
    private String details;

    public ConfigGameProfile() {
    }

    public ConfigGameProfile(String game, String details) {
        this.game = game;
        this.details = details;
    }

    public ConfigGameProfile(String game) {
        this.game = game;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConfigGameProfile that = (ConfigGameProfile) o;
        return Objects.equals(game, that.game) &&
                Objects.equals(details, that.details);
    }

    @Override
    public int hashCode() {

        return Objects.hash(game, details);
    }

    @Override
    public String toString() {
        return "ConfigGameProfile{" +
                "game='" + game + '\'' +
                ", details='" + details + '\'' +
                '}';
    }
}
