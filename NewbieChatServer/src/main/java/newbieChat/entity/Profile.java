package newbieChat.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "profile")
public class Profile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "country")
    private String country;

    @Column(name = "city")
    private String city;

    @Column(name = "sex")
    private String sex;

    @Column(name = "age")
    private Integer age;

    @Column(name = "gaming_style")
    private String gamingStyle;

    @Column(name = "time_spent")
    private String timeSpent;

    @Column(name = "time_of_day")
    private String timeOfDay;


    public Profile() {
    }

    public Profile(String country, String city, String sex, Integer age, String gamingStyle, String timeSpent, String timeOfDay) {
        this.country = country;
        this.city = city;
        this.sex = sex;
        this.age = age;
        this.gamingStyle = gamingStyle;
        this.timeSpent = timeSpent;
        this.timeOfDay = timeOfDay;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getGamingStyle() {
        return gamingStyle;
    }

    public void setGamingStyle(String gamingStyle) {
        this.gamingStyle = gamingStyle;
    }

    public String getTimeSpent() {
        return timeSpent;
    }

    public void setTimeSpent(String timeSpent) {
        this.timeSpent = timeSpent;
    }

    public String getTimeOfDay() {
        return timeOfDay;
    }

    public void setTimeOfDay(String timeOfDay) {
        this.timeOfDay = timeOfDay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Profile profile = (Profile) o;
        return Objects.equals(id, profile.id) &&
                Objects.equals(country, profile.country) &&
                Objects.equals(city, profile.city) &&
                Objects.equals(sex, profile.sex) &&
                Objects.equals(age, profile.age) &&
                Objects.equals(gamingStyle, profile.gamingStyle) &&
                Objects.equals(timeSpent, profile.timeSpent) &&
                Objects.equals(timeOfDay, profile.timeOfDay);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, country, city, sex, age, gamingStyle, timeSpent, timeOfDay);
    }

    @Override
    public String toString() {
        return "Profile{" +
                "id=" + id +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", sex='" + sex + '\'' +
                ", age=" + age +
                ", gamingStyle='" + gamingStyle + '\'' +
                ", timeSpent='" + timeSpent + '\'' +
                ", timeOfDay='" + timeOfDay + '\'' +
                '}';
    }
}
