package newbieChat.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ContactId implements Serializable {

    @Column(name = "owner")
    private Long owner;

    @Column(name = "contact")
    private Long contact;

    public ContactId() {
    }

    public ContactId(Long owner, Long contact) {
        this.owner = owner;
        this.contact = contact;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContactId contactId = (ContactId) o;
        return Objects.equals(owner, contactId.owner) &&
                Objects.equals(contact, contactId.contact);
    }

    @Override
    public int hashCode() {

        return Objects.hash(owner, contact);
    }
}
