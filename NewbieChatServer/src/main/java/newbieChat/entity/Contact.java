package newbieChat.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "contact_list")
public class Contact implements Serializable {

    @EmbeddedId
    private ContactId id;

    @ManyToOne(cascade = CascadeType.MERGE, targetEntity = User.class)
    @JoinColumn(name = "owner", nullable = false, insertable = false, updatable = false)
    private User owner;

    @ManyToOne(cascade = CascadeType.MERGE, targetEntity = User.class)
    @JoinColumn(name = "contact", nullable = false, insertable = false, updatable = false)
    private User contact;

    public Contact() {
    }

    public Contact(ContactId id, User owner, User contact) {
        this.id = id;
        this.owner = owner;
        this.contact = contact;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public User getContact() {
        return contact;
    }

    public void setContact(User contact) {
        this.contact = contact;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contact that = (Contact) o;
        return Objects.equals(owner, that.owner) &&
                Objects.equals(contact, that.contact);
    }

    @Override
    public int hashCode() {

        return Objects.hash(owner, contact);
    }

    @Override
    public String toString() {
        return "Contact{" +
                "owner=" + owner +
                ", contact=" + contact +
                '}';
    }
}
