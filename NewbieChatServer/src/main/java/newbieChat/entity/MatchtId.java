package newbieChat.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class MatchtId implements Serializable {

    @Column(name = "user1")
    private Long user1;

    @Column(name = "user2")
    private Long user2;

    public MatchtId() {
    }

    public MatchtId(Long user1, Long user2) {
        this.user1 = user1;
        this.user2 = user2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MatchtId matchtId = (MatchtId) o;
        return Objects.equals(user1, matchtId.user1) &&
                Objects.equals(user2, matchtId.user2);
    }

    @Override
    public int hashCode() {

        return Objects.hash(user1, user2);
    }
}
