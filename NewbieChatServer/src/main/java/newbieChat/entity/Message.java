package newbieChat.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import newbieChat.util.OffsetDateTimeDeserializer;

import javax.persistence.*;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.Objects;

@Entity
@Table(name = "message")
public class Message implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE, targetEntity = User.class)
    @JoinColumn(name = "send_from", nullable = false, updatable = false)
    private User sendFrom;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE, targetEntity = User.class)
    @JoinColumn(name = "send_to", nullable = false, updatable = false)
    private User sendTo;

    @Column(name = "content", nullable = false, length = 2048)
    private String content;

    @Column(name = "send_date", columnDefinition = "timestamp with time zone")
    @JsonSerialize(using = ToStringSerializer.class)
    @JsonDeserialize(using = OffsetDateTimeDeserializer.class)
    @JsonFormat(timezone = "UTC")
    private OffsetDateTime sendDate;

    @Column(name = "read", nullable = false)
    private boolean read;

    public Message() {
    }

    public Message(Long id, User sendFrom, User sendTo, String content, OffsetDateTime sendDate, boolean read) {
        this.id = id;
        this.sendFrom = sendFrom;
        this.sendTo = sendTo;
        this.content = content;
        this.sendDate = sendDate;
        this.read = read;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getSendFrom() {
        return sendFrom;
    }

    public void setSendFrom(User sendFrom) {
        this.sendFrom = sendFrom;
    }

    public User getSendTo() {
        return sendTo;
    }

    public void setSendTo(User sendTo) {
        this.sendTo = sendTo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public OffsetDateTime getSendDate() {
        return sendDate;
    }

    public void setSendDate(OffsetDateTime sendDate) {
        this.sendDate = sendDate;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return read == message.read &&
                Objects.equals(id, message.id) &&
                Objects.equals(sendFrom, message.sendFrom) &&
                Objects.equals(sendTo, message.sendTo) &&
                Objects.equals(content, message.content) &&
                Objects.equals(sendDate, message.sendDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, sendFrom, sendTo, content, sendDate, read);
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", sendFrom=" + sendFrom +
                ", sendTo=" + sendTo +
                ", content='" + content + '\'' +
                ", sendDate=" + sendDate +
                ", read=" + read +
                '}';
    }
}
