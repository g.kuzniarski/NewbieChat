package newbieChat;

import javafx.application.Application;
import javafx.application.Preloader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import newbieChat.dictionary.Texts;
import newbieChat.view.ServerMainView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Lazy;

@Lazy
@SpringBootApplication
@SuppressWarnings("restriction")
public class NewbieChatServer extends Application {

    @Autowired
    private ServerMainView serverMainView;

    private static String[] savedArgs;
    private ConfigurableApplicationContext applicationContext;

    @Override
    public void init() {
        applicationContext = SpringApplication.run(getClass(), savedArgs);
        applicationContext.getAutowireCapableBeanFactory().autowireBean(this);
    }

    @Override
    public void start(Stage stage) throws Exception {
        notifyPreloader(new Preloader.StateChangeNotification(Preloader.StateChangeNotification.Type.BEFORE_START));

        stage.setTitle(Texts.SERVER_MAIN_WINDOW_TITLE);
        stage.setScene(new Scene(serverMainView.getView()));
        stage.setResizable(true);
        stage.centerOnScreen();
        stage.show();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        applicationContext.close();
        System.exit(0);
    }

    public static void main(String[] args) {
        NewbieChatServer.savedArgs = args;
        Application.launch(NewbieChatServer.class, args);
    }
}
