package newbieChat.dictionary;

import newbieChat.controller.SettingsBasicPaneController;
import newbieChat.controller.SettingsPaneController;
import newbieChat.controller.SettingsSummaryPaneController;
import newbieChat.controller.SettingsUsersPaneController;
import org.controlsfx.glyphfont.Glyph;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ServerTreeItems {

    public static final Map<String, ServerTreeItem> SERVER_TREE_ITEM_MAP = Stream.of(
            new AbstractMap.SimpleEntry<>(Texts.SUMMARY_TREE_ITEM_LABEL,
                    new ServerTreeItem(Texts.SUMMARY_TREE_ITEM_LABEL, Icons.SUMMARY_TREE_ITEM_ICON, new SettingsSummaryPaneController(Views.SUMMARY_TREE_ITEM_VIEW))),
            new AbstractMap.SimpleEntry<>(Texts.USERS_TREE_ITEM_LABEL,
                    new ServerTreeItem(Texts.USERS_TREE_ITEM_LABEL, Icons.USERS_TREE_ITEM_ICON, new SettingsUsersPaneController(Views.USERS_TREE_ITEM_VIEW))),
            new AbstractMap.SimpleEntry<>(Texts.STATISTICS_TREE_ITEM_LABEL,
                    new ServerTreeItem(Texts.STATISTICS_TREE_ITEM_LABEL, Icons.STATISTICS_TREE_ITEM_ICON, new SettingsUsersPaneController(Views.STATISTICS_TREE_ITEM_VIEW))),
            new AbstractMap.SimpleEntry<>(Texts.BASIC_TREE_ITEM_LABEL,
                    new ServerTreeItem(Texts.BASIC_TREE_ITEM_LABEL, Icons.BASIC_TREE_ITEM_ICON, new SettingsBasicPaneController(Views.BASIC_TREE_ITEM_VIEW))))
            .collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));

    public static class ServerTreeItem {
        private String label;
        private Glyph icon;
        private SettingsPaneController controller;
        private List<ServerTreeItem> children;

        public ServerTreeItem() {
        }

        public ServerTreeItem(String label, Glyph icon, SettingsPaneController view) {
            this(label, icon, view, new ArrayList<>());
        }

        public ServerTreeItem(String label, Glyph icon, SettingsPaneController controller, List<ServerTreeItem> children) {
            this.label = label;
            this.icon = icon;
            this.controller = controller;
            this.children = children;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public Glyph getIcon() {
            return icon;
        }

        public void setIcon(Glyph icon) {
            this.icon = icon;
        }

        public SettingsPaneController getController() {
            return controller;
        }

        public void setController(SettingsPaneController controller) {
            this.controller = controller;
        }

        public List<ServerTreeItem> getChildren() {
            return children;
        }

        public void setChildren(List<ServerTreeItem> children) {
            this.children = children;
        }

        @Override
        public String toString() {
            return "ServerTreeItem{" +
                    "label='" + label + '\'' +
                    ", icon=" + icon +
                    ", controller='" + controller + '\'' +
                    ", children=" + children +
                    '}';
        }
    }
}
