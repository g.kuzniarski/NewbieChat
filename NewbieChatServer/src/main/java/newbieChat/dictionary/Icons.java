package newbieChat.dictionary;

import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

public class Icons {

    //Import Font Awesome
    private static GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");

    public static final Glyph SERVER_TREE_ITEM_ICON = fontAwesome.create(FontAwesome.Glyph.SERVER);
    public static final Glyph SUMMARY_TREE_ITEM_ICON = fontAwesome.create(FontAwesome.Glyph.SLIDERS);
    public static final Glyph USERS_TREE_ITEM_ICON = fontAwesome.create(FontAwesome.Glyph.USER);
    public static final Glyph SETTINGS_TREE_ITEM_ICON = fontAwesome.create(FontAwesome.Glyph.COG);
    public static final Glyph BASIC_TREE_ITEM_ICON = fontAwesome.create(FontAwesome.Glyph.LIST);
    public static final Glyph STATISTICS_TREE_ITEM_ICON = fontAwesome.create(FontAwesome.Glyph.BAR_CHART);
}
