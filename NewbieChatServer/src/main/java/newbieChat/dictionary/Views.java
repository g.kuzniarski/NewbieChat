package newbieChat.dictionary;

import java.io.File;

public class Views {
    public static final String SERVER_MAIN_WINDOW = "templates" + File.separator + "ServerMainWindow.fxml";
    public static final String SUMMARY_TREE_ITEM_VIEW = "templates" + File.separator + "SettingsSummaryPane.fxml";
    public static final String USERS_TREE_ITEM_VIEW = "templates" + File.separator + "SettingsUsersPane.fxml";
    public static final String BASIC_TREE_ITEM_VIEW = "templates" + File.separator + "SettingsBasicPane.fxml";
    public static final String STATISTICS_TREE_ITEM_VIEW = "templates" + File.separator + "StatisticsPane.fxml";
}
