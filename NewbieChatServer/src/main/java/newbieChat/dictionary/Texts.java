package newbieChat.dictionary;

public class Texts {
    //Titles
    public static final String SERVER_MAIN_WINDOW_TITLE = "Newbie Chat Server";

    //Server tree
    public static final String SERVER_TREE_ITEM_LABEL = "Server";
    public static final String SUMMARY_TREE_ITEM_LABEL = "Summary";
    public static final String USERS_TREE_ITEM_LABEL = "User";
    public static final String SETTINGS_TREE_ITEM_LABEL = "Settings";
    public static final String BASIC_TREE_ITEM_LABEL = "Basic";
    public static final String STATISTICS_TREE_ITEM_LABEL = "Statistics";

    //Settings basic pane
    public static final String START_SERVER_BUTTON = "Start";
    public static final String STOP_SERVER_BUTTON = "Stop";

    //Menu bar
    //FILE
    public static final String MENU_BAR_MENU_FILE = "File";
    public static final String MENU_BAR_ITEM_ADD_CONTACT = "Add contact";
    public static final String MENU_BAR_ITEM_SEARCH = "Search";
    public static final String MENU_BAR_ITEM_SETTINGS = "Settings";
    public static final String MENU_BAR_ITEM_EXIT = "Exit";
    //EDIT
    public static final String MENU_BAR_MENU_EDIT = "Edit";
    public static final String MENU_BAR_ITEM_DELETE = "Delete";
    //HELP
    public static final String MENU_BAR_MENU_HELP = "Help";
    public static final String MENU_BAR_ITEM_ABOUT = "About";

}
