package newbieChat.model;

public enum UserStatusType {
    ONLINE("Online"),
    BRB("BRB"),
    OFFLINE("Offline");

    private String status;

    UserStatusType(String status) {
        this.status = status;
    }

    public static boolean contains(String status) {
        try {
            valueOf(status);
            return true;
        } catch (IllegalArgumentException ignored) {
            return false;
        }
    }

    @Override
    public String toString() {
        return status;
    }
}
