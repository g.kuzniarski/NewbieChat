package newbieChat.model;

import newbieChat.enums.ServerCommunicationObjectType;

import java.io.Serializable;
import java.util.Map;

public class ServerCommunicationObject implements Serializable {
    private ServerCommunicationObjectType type;
    private Map<String, Object> content;

    public ServerCommunicationObject() {
    }

    public ServerCommunicationObject(ServerCommunicationObjectType type, Map<String, Object> content) {
        this.type = type;
        this.content = content;
    }

    public ServerCommunicationObjectType getType() {
        return type;
    }

    public void setType(ServerCommunicationObjectType type) {
        this.type = type;
    }

    public Map<String, Object> getContent() {
        return content;
    }

    public void setContent(Map<String, Object> content) {
        this.content = content;
    }
}

