package newbieChat.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.ObjectInputStream;

class ListenFromServer extends Thread {

    private static final Logger logger = LogManager.getLogger(Client.class);

    private ObjectInputStream sInput;
    private Client client;

    ListenFromServer(ObjectInputStream sInput, Client client) {
        this.sInput = sInput;
        this.client = client;
    }

    public void run() {
        while (true) {
            try {
                ServerCommunicationObject msg = (ServerCommunicationObject) sInput.readObject();
                logger.info("SERVER::" + msg.getType() + "::" + msg.getContent());
                switch (msg.getType()) {
                    case MESSAGE_ADD:
                        client.addMessage(msg.getContent());
                        break;
                    case MESSAGE_LIST:
                        client.refreshMessages(msg.getContent());
                        break;
                    case MESSAGE_OTHER:
                        client.refreshMessages(msg.getContent());
                        break;
                    case USER_ME:
                        client.refreshMe(msg.getContent());
                        break;
                    case USER_LIST:
                        client.refreshContacts(msg.getContent());
                        break;
                    case USER_LIST_FILTERS:
                        client.refreshUserTableContent(msg.getContent());
                        break;
                    case CONTACT_DETAILS:
                        client.refreshUserProfileDetails(msg.getContent());
                        break;
                    case INVITE_LIST:
                        client.refreshInvites(msg.getContent());
                        break;
                    case MATCH_FIND:
                        client.addMatch(msg.getContent());
                        break;
                    case SETTINGS_GET:
                        client.refreshSettings(msg.getContent());
                        break;
                    case LOGOUT:
                        client.disconnect();
                        System.exit(0);
                }
            } catch (IOException e) {
                logger.error(e.getMessage());
                client.serverError(new Exception("Server closed connection"));
                break;
            } catch (ClassNotFoundException e) {
                logger.error(e.getMessage());
            }
        }
    }

}