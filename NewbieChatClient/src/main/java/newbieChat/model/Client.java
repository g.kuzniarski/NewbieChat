package newbieChat.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.application.Platform;
import newbieChat.controller.*;
import newbieChat.dto.*;
import newbieChat.enums.ServerCommunicationObjectType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controlsfx.dialog.ExceptionDialog;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Client {

    private static Client instance;

    private static final Logger logger = LogManager.getLogger(Client.class);
    private ObjectMapper objectMapper = new ObjectMapper();

    private ObjectInputStream sInput;
    private ObjectOutputStream sOutput;
    private Socket socket;
    private String server;
    private int port;

    public static Client initializeClient(String server, int port) {
        if (instance == null) {
            instance = new Client(server, port);
        }
        return instance;
    }

    private Client(String server, int port) {
        this.server = server;
        this.port = port;
    }

    public static Client getInstance() {
        return instance;
    }

    public boolean start(String username, String password) {
        try {
            socket = new Socket(server, port);
        } catch (IOException e) {
            logger.error(e.getMessage());
            return false;
        }

        logger.info("Connected to " + socket.getInetAddress() + ":" + socket.getPort());

        try {
            sInput = new ObjectInputStream(socket.getInputStream());
            sOutput = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            logger.error(e.getMessage());
            return false;
        }

        try {
            Map<String, Object> loginContent = new HashMap<>();
            loginContent.put("login", username);
            loginContent.put("password", password);

            ServerCommunicationObject communicationObject = new ServerCommunicationObject(ServerCommunicationObjectType.LOGIN, loginContent);
            sOutput.writeObject(communicationObject);

            communicationObject = (ServerCommunicationObject) sInput.readObject();
            if ((Boolean) communicationObject.getContent().get("status")) {
                logger.info("Login successful");
                ListenFromServer listenFromServer = new ListenFromServer(sInput, this);
                listenFromServer.start();
                return true;
            } else {
                logger.info("Bad credentials");
                return false;
            }
        } catch (IOException | ClassNotFoundException e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    public void send(ServerCommunicationObject msg) {
        try {
            sOutput.writeObject(msg);
            logger.info("CLIENT::" + msg.getType() + "::" + msg.getContent());
        } catch (IOException e) {
            if (e.getMessage().equals("Socket closed")) {
                serverError(e);
            }
            logger.error(e.getMessage());
        }
    }

    public void disconnect() {
        if (sOutput != null)
            send(new ServerCommunicationObject(ServerCommunicationObjectType.LOGOUT, new HashMap<>()));
        try {
            if (sInput != null) sInput.close();
            if (sOutput != null) sOutput.close();
            if (socket != null) socket.close();
        } catch (Exception ignored) {
        }
    }

    public void sendMessage(String msg, Long receiverId) {
        Map<String, Object> content = new HashMap<>();
        content.put("message", msg);
        content.put("sendTo", receiverId);
        send(new ServerCommunicationObject(ServerCommunicationObjectType.MESSAGE_ADD, content));
    }

    public void getMessageList(Long sendTo) {
        send(new ServerCommunicationObject(ServerCommunicationObjectType.MESSAGE_LIST, Collections.singletonMap("sendTo", sendTo)));
    }

    public void getOtherMessages() {
        send(new ServerCommunicationObject(ServerCommunicationObjectType.MESSAGE_OTHER, new HashMap<>()));
    }

    public void addContact(Long contactId) {
        send(new ServerCommunicationObject(ServerCommunicationObjectType.CONTACT_ADD, Collections.singletonMap("contactId", contactId)));
    }

    public void addContactByLogin(String contactLogin) {
        send(new ServerCommunicationObject(ServerCommunicationObjectType.CONTACT_ADD_LOGIN, Collections.singletonMap("contactLogin", contactLogin)));
    }

    public void deleteContact(Long contactId) {
        send(new ServerCommunicationObject(ServerCommunicationObjectType.CONTACT_DELETE, Collections.singletonMap("contactId", contactId)));
    }

    public void getContactList() {
        send(new ServerCommunicationObject(ServerCommunicationObjectType.USER_LIST, new HashMap<>()));
    }

    public void getMatch() {
        send(new ServerCommunicationObject(ServerCommunicationObjectType.MATCH_FIND, new HashMap<>()));
    }

    public void getCurrentUserProfile() {
        send(new ServerCommunicationObject(ServerCommunicationObjectType.USER_ME, new HashMap<>()));
    }

    public void getUserProfileByUserId(Long userId) {
        send(new ServerCommunicationObject(ServerCommunicationObjectType.CONTACT_DETAILS, Collections.singletonMap("userId", userId)));
    }

    public void changeStatus(String status) {
        Map<String, Object> content = new HashMap<>();
        content.put("status", status);
        send(new ServerCommunicationObject(ServerCommunicationObjectType.USER_ME_STATUS, content));
    }

    public void changeStatusDetails(String statusDetails) {
        Map<String, Object> content = new HashMap<>();
        content.put("statusDetails", statusDetails);
        send(new ServerCommunicationObject(ServerCommunicationObjectType.USER_ME_STATUS_DETAILS, content));
    }

    public void getUserListWithFilters(Map<String, Object> filters) {
        Map<String, Object> content = new HashMap<>();
        content.put("filters", filters);
        send(new ServerCommunicationObject(ServerCommunicationObjectType.USER_LIST_FILTERS, content));
    }

    public void getSettingsScreenData() {
        send(new ServerCommunicationObject(ServerCommunicationObjectType.SETTINGS_GET, new HashMap<>()));
    }

    public void getInvites() {
        send(new ServerCommunicationObject(ServerCommunicationObjectType.INVITE_LIST, new HashMap<>()));
    }

    public void changeInvitationState(Long userId, String action) {
        HashMap<String, Object> content = new HashMap<>();
        content.put("userId", userId);
        content.put("action", action);
        send(new ServerCommunicationObject(ServerCommunicationObjectType.INVITE_DECIDE, content));
    }

    public void changeMatchState(Long userId, String action) {
        HashMap<String, Object> content = new HashMap<>();
        content.put("userId", userId);
        content.put("action", action);
        send(new ServerCommunicationObject(ServerCommunicationObjectType.MATCH_DECIDE, content));
    }

    public void sendUserUpdate(UserFullProfileDTO userFullProfileDTO) {
        try {
            Map<String, Object> content = new HashMap<>();
            content.put("userProfile", objectMapper.writeValueAsString(userFullProfileDTO));
            send(new ServerCommunicationObject(ServerCommunicationObjectType.USER_UPDATE, content));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    public void deleteAccount() {
        send(new ServerCommunicationObject(ServerCommunicationObjectType.USER_DELETE, new HashMap<>()));
    }

    public void refreshContacts(Map<String, Object> content) {
        if (content.containsKey("contactList")) {
            try {
                List<ContactDTO> contacts = objectMapper.readValue((String) content.get("contactList"), new TypeReference<List<ContactDTO>>() {
                });

                if (ContactListController.getInstance() != null) {
                    ContactListController.getInstance().refreshContacts(contacts);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void refreshMe(Map<String, Object> content) {
        if (content.containsKey("me")) {
            try {
                ContactDTO me = objectMapper.readValue((String) content.get("me"), ContactDTO.class);

                if (ContactListController.getInstance() != null) {
                    ContactListController.getInstance().refreshUserInfo(me);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void refreshUserProfileDetails(Map<String, Object> content) {
        if (content.containsKey("userProfile")) {
            try {
                UserFullProfileDTO userProfile = objectMapper.readValue((String) content.get("userProfile"), UserFullProfileDTO.class);

                if (ContactListController.getInstance() != null) {
                    ContactListController.getInstance().refreshUserProfileDetails(userProfile);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public void addMatch(Map<String, Object> content) {
        if (content.containsKey("matchedUser")) {
            try {
                UserFullProfileDTO userProfile = objectMapper.readValue((String) content.get("matchedUser"), UserFullProfileDTO.class);

                if (ContactListController.getInstance() != null) {
                    ContactListController.getInstance().refreshMatch(userProfile);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void addMessage(Map<String, Object> content) {
        if (content.containsKey("message")) {
            try {
                MessageDTO message = objectMapper.readValue((String) content.get("message"), MessageDTO.class);

                if (ContactListController.getInstance() != null) {
                    ContactListController.getInstance().addMessageToChatWindow(message);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void refreshMessages(Map<String, Object> content) {
        if (content.containsKey("messageList") && content.containsKey("sendTo")) {
            try {
                List<MessageDTO> messageList = objectMapper.readValue((String) content.get("messageList"), new TypeReference<List<MessageDTO>>() {
                });
                Long sendTo = (Long) content.get("sendTo");

                if (ContactListController.getInstance() != null) {
                    ContactListController.getInstance().refreshMessageListInChatWindow(sendTo, messageList);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (content.containsKey("other")) {
            try {
                List<UserProfileDTO> profileDTOList = objectMapper.readValue((String) content.get("other"), new TypeReference<List<UserProfileDTO>>() {
                });

                if (MessagesController.getInstance() != null) {
                    MessagesController.getInstance().refreshMessagesWindow(profileDTOList);
                }
                if (ContactListController.getInstance() != null) {
                    ContactListController.getInstance().refreshOtherMessagesButton(profileDTOList.size());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void refreshUserTableContent(Map<String, Object> content) {
        if (content.containsKey("userList")) {
            try {
                List<UserProfileDTO> userList = objectMapper.readValue((String) content.get("userList"), new TypeReference<List<UserProfileDTO>>() {
                });

                if (UserCatalogController.getInstance() != null) {
                    UserCatalogController.getInstance().refreshUserTableContent(userList);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void refreshSettings(Map<String, Object> content) {
        if (content.containsKey("userFullProfile") && content.containsKey("configGameProfile")) {
            try {
                UserFullProfileDTO userFullProfileDTO = objectMapper.readValue((String) content.get("userFullProfile"), UserFullProfileDTO.class);
                List<ConfigGameProfileDTO> configGameProfileDTOList = objectMapper.readValue((String) content.get("configGameProfile"), new TypeReference<List<ConfigGameProfileDTO>>() {
                });

                if (SettingsController.getInstance() != null) {
                    SettingsController.getInstance().refreshSettings(configGameProfileDTOList, userFullProfileDTO);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void refreshInvites(Map<String, Object> content) {
        if (content.containsKey("inviteList")) {
            try {
                List<InviteDTO> inviteDTOList = objectMapper.readValue((String) content.get("inviteList"), new TypeReference<List<InviteDTO>>() {
                });

                if (InvitesController.getInstance() != null) {
                    InvitesController.getInstance().refreshInvites(inviteDTOList);
                }

                if (ContactListController.getInstance() != null) {
                    ContactListController.getInstance().refreshInvitesButton(inviteDTOList.size());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void serverError(Exception e) {
        Platform.runLater(() -> {
            ExceptionDialog exceptionDialog = new ExceptionDialog(e);
            exceptionDialog.showAndWait();
            System.exit(-1);
        });
    }
}

