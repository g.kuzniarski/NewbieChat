package newbieChat.dto;

import java.io.Serializable;

public class MatchDTO implements Serializable {

    private Long user1;
    private Long user2;
    private boolean agree1;
    private boolean agree2;

    public MatchDTO() {
    }

    public MatchDTO(Long user1, Long user2, boolean agree1, boolean agree2) {
        this.user1 = user1;
        this.user2 = user2;
        this.agree1 = agree1;
        this.agree2 = agree2;
    }

    public Long getUser1() {
        return user1;
    }

    public void setUser1(Long user1) {
        this.user1 = user1;
    }

    public Long getUser2() {
        return user2;
    }

    public void setUser2(Long user2) {
        this.user2 = user2;
    }

    public boolean isAgree1() {
        return agree1;
    }

    public void setAgree1(boolean agree1) {
        this.agree1 = agree1;
    }

    public boolean isAgree2() {
        return agree2;
    }

    public void setAgree2(boolean agree2) {
        this.agree2 = agree2;
    }
}
