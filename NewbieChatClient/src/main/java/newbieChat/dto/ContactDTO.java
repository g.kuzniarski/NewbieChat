package newbieChat.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import newbieChat.model.UserStatusType;

import java.io.Serializable;
import java.util.Objects;

public class ContactDTO implements Serializable {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("nickname")
    private String nickname;

    @JsonProperty("name")
    private String name;

    @JsonProperty("status")
    private UserStatusType status;

    @JsonProperty("statusDetails")
    private String statusDetails;

    @JsonProperty("avatarPath")
    private String avatarPath;

    public ContactDTO() {
    }

    public ContactDTO(Long id, String nickname, String name, UserStatusType status, String statusDetails, String avatarPath) {
        this.id = id;
        this.nickname = nickname;
        this.name = name;
        this.status = status;
        this.statusDetails = statusDetails;
        this.avatarPath = avatarPath;
    }

    public ContactDTO(UserProfileDTO item) {
        this.id = item.getId();
        this.nickname = item.getNickname();
        this.name = item.getName();
        this.avatarPath = item.getAvatarPath();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserStatusType getStatus() {
        return status;
    }

    public void setStatus(UserStatusType status) {
        this.status = status;
    }

    public String getStatusDetails() {
        return statusDetails;
    }

    public void setStatusDetails(String statusDetails) {
        this.statusDetails = statusDetails;
    }

    public String getAvatarPath() {
        return avatarPath;
    }

    public void setAvatarPath(String avatarPath) {
        this.avatarPath = avatarPath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContactDTO that = (ContactDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(nickname, that.nickname) &&
                Objects.equals(name, that.name) &&
                Objects.equals(status, that.status) &&
                Objects.equals(statusDetails, that.statusDetails) &&
                Objects.equals(avatarPath, that.avatarPath);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, nickname, name, status, statusDetails, avatarPath);
    }

    @Override
    public String toString() {
        return "ContactDTO{" +
                "id=" + id +
                ", nickname='" + nickname + '\'' +
                ", name='" + name + '\'' +
                ", status='" + status + '\'' +
                ", statusDetails='" + statusDetails + '\'' +
                ", avatarPath='" + avatarPath + '\'' +
                '}';
    }
}
