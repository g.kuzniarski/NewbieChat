package newbieChat.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import newbieChat.controller.ContactListController;
import newbieChat.dto.ContactDTO;
import newbieChat.dto.InviteDTO;
import newbieChat.model.Client;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.File;
import java.io.IOException;

import static newbieChat.dictionary.Views.INVITES_ELEMENT;
import static newbieChat.util.JavaFXUtil.generateImageViewCircleFromImageURL;

/**
 * Pane representing an user at the contact list
 */
public class InvitesElement extends GridPane {

    @FXML
    private ImageView avatar;
    @FXML
    private Label nicknameLabel;
    @FXML
    private Button acceptButton;
    @FXML
    private Button discardButton;
    @FXML
    private Button profileButton;
    @FXML
    private Button messageButton;
    @FXML
    private Button blockButton;

    private ContactDTO contact;

    private Client client = Client.getInstance();

    public InvitesElement() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(".." + File.separator + ".." + File.separator + INVITES_ELEMENT));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public void setContact(InviteDTO inviteDTO) {
        ContactDTO contactDTO = new ContactDTO();
        contactDTO.setId(inviteDTO.getUserId());
        contactDTO.setNickname(inviteDTO.getNickname());
        contactDTO.setAvatarPath(inviteDTO.getAvatar());
        contactDTO.setName(inviteDTO.getName());
        this.contact = contactDTO;
    }

    public void init() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");

        this.nicknameLabel.setText(contact.getNickname() + " (" + contact.getName() + ")");
        generateImageViewCircleFromImageURL(this.avatar, contact.getAvatarPath());
        
        acceptButton.setText("");
        Glyph accept = fontAwesome.create(FontAwesome.Glyph.CHECK);
        accept.setFontSize(13);
        acceptButton.setGraphic(accept);
        
        discardButton.setText("");
        Glyph discard = fontAwesome.create(FontAwesome.Glyph.TIMES);
        discard.setFontSize(13);
        discardButton.setGraphic(discard);

        profileButton.setText("");
        Glyph details = fontAwesome.create(FontAwesome.Glyph.LIST_ALT);
        details.setFontSize(13);
        profileButton.setGraphic(details);
        
        messageButton.setText("");
        Glyph message = fontAwesome.create(FontAwesome.Glyph.ENVELOPE);
        message.setFontSize(13);
        messageButton.setGraphic(message);
        
        blockButton.setText("");
        Glyph block = fontAwesome.create(FontAwesome.Glyph.BAN);
        block.setFontSize(13);
        blockButton.setGraphic(block);
        
        acceptButton.setOnAction(this::acceptButtonAction);
        discardButton.setOnAction(this::discardButtonAction);
        profileButton.setOnAction(this::profileButtonAction);
        messageButton.setOnAction(this::messageButtonAction);
        blockButton.setOnAction(this::blockButtonAction);
    }

    private void acceptButtonAction(ActionEvent event) {
        client.changeInvitationState(contact.getId(), "ACCEPT");
    }

    private void discardButtonAction(ActionEvent event) {
        client.changeInvitationState(contact.getId(), "DISCARD");
    }

    private void profileButtonAction(ActionEvent event) {
        ContactListController.getInstance().openUserDetailsWindow(contact.getId());
    }

    private void messageButtonAction(ActionEvent event) {
        ContactListController.getInstance().openChatWindow(contact);
    }

    private void blockButtonAction(ActionEvent event) {
        client.changeInvitationState(contact.getId(), "BLOCK");
    }

}
