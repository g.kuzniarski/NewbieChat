package newbieChat.view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.scene.web.WebView;

import java.io.File;
import java.io.IOException;

import static newbieChat.dictionary.Views.CHAT_MESSAGE_ELEMENT;
import static newbieChat.dictionary.Views.CONTACT_LIST_ELEMENT;
import static newbieChat.util.JavaFXUtil.generateImageViewCircleFromImageURL;

/**
 * Pane representing a message
 */
public class ChatMessageElement extends GridPane {

    @FXML
    private ImageView avatar;
    @FXML
    private Label nickname;
    @FXML
    private Label sendDate;
    @FXML
    private TextFlow contentTextFlow;

    public ChatMessageElement() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(".." + File.separator + ".." + File.separator + CHAT_MESSAGE_ELEMENT));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public void setAvatar(String imagePath) {
        generateImageViewCircleFromImageURL(this.avatar, imagePath);
    }

    public void setNickname(String nickname) {
        this.nickname.setText(nickname);
    }

    public void setSendDate(String sendDate) {
        this.sendDate.setText(sendDate);
    }

    public void setContent(String content) {
        Text text = new Text(content);
        text.setFont(Font.font("System", FontPosture.REGULAR, 12));
        contentTextFlow.getChildren().add(text);
    }
}
