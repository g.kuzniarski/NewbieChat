package newbieChat.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import newbieChat.controller.ContactListController;
import newbieChat.dto.ContactDTO;
import newbieChat.dto.UserProfileDTO;
import newbieChat.model.Client;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.File;
import java.io.IOException;

import static newbieChat.dictionary.Views.MESSAGES_ELEMENT;
import static newbieChat.util.JavaFXUtil.generateImageViewCircleFromImageURL;

/**
 * Pane representing an user at the contact list
 */
public class MessagesElement extends GridPane {

    @FXML
    private ImageView avatar;
    @FXML
    private Label nicknameLabel;
    @FXML
    private Button addContactButton;
    @FXML
    private Button profileButton;
    @FXML
    private Button messageButton;
    @FXML
    private Button blockButton;

    private ContactDTO contact;

    private Client client = Client.getInstance();

    public MessagesElement() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(".." + File.separator + ".." + File.separator + MESSAGES_ELEMENT));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public void setContact(UserProfileDTO userProfileDTO) {
        contact = new ContactDTO();
        contact.setId(userProfileDTO.getId());
        contact.setNickname(userProfileDTO.getNickname());
        contact.setAvatarPath(userProfileDTO.getAvatarPath());
        contact.setName(userProfileDTO.getName());
    }

    public void init() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");

        this.nicknameLabel.setText(contact.getNickname() + " (" + contact.getName() + ")");
        generateImageViewCircleFromImageURL(this.avatar, contact.getAvatarPath());

        addContactButton.setText("");
        Glyph addContact = fontAwesome.create(FontAwesome.Glyph.USER_PLUS);
        addContact.setFontSize(13);
        addContactButton.setGraphic(addContact);

        profileButton.setText("");
        Glyph details = fontAwesome.create(FontAwesome.Glyph.LIST_ALT);
        details.setFontSize(13);
        profileButton.setGraphic(details);
        
        messageButton.setText("");
        Glyph message = fontAwesome.create(FontAwesome.Glyph.ENVELOPE);
        message.setFontSize(13);
        messageButton.setGraphic(message);
        
        blockButton.setText("");
        Glyph block = fontAwesome.create(FontAwesome.Glyph.BAN);
        block.setFontSize(13);
        blockButton.setGraphic(block);
        
        addContactButton.setOnAction(this::addContactButtonAction);
        profileButton.setOnAction(this::profileButtonAction);
        messageButton.setOnAction(this::messageButtonAction);
        blockButton.setOnAction(this::blockButtonAction);
    }

    private void addContactButtonAction(ActionEvent event) {
        client.addContact(contact.getId());
    }

    private void profileButtonAction(ActionEvent event) {
        ContactListController.getInstance().openUserDetailsWindow(contact);
    }

    private void messageButtonAction(ActionEvent event) {
        ContactListController.getInstance().openChatWindow(contact);
    }

    private void blockButtonAction(ActionEvent event) {
        client.changeInvitationState(contact.getId(), "BLOCK");
    }

}
