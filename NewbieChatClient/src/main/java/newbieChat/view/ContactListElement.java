package newbieChat.view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

import java.io.File;
import java.io.IOException;

import static newbieChat.dictionary.Views.CONTACT_LIST_ELEMENT;
import static newbieChat.util.JavaFXUtil.generateImageViewCircleFromImageURL;

/**
 * Pane representing an user at the contact list
 */
public class ContactListElement extends GridPane {

    @FXML
    private ImageView avatar;
    @FXML
    private Label nickname;
    @FXML
    private Label status;
    @FXML
    private Label statusDetails;

    private Long userId;

    public ContactListElement() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(".." + File.separator + ".." + File.separator + CONTACT_LIST_ELEMENT));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setAvatar(String imagePath) {
        generateImageViewCircleFromImageURL(this.avatar, imagePath);
    }

    public void setNicknameText(String nickname) {
        this.nickname.setText(nickname);
    }

    public void setStatusText(String status) {
        this.status.setText(status);
    }

    public String getStatus() {
        return this.status.getText();
    }

    public void setStatusDetailsText(String statusDetails) {
        this.statusDetails.setText(statusDetails);
    }

}
