package newbieChat.util;

import javafx.scene.control.Control;
import org.controlsfx.validation.Severity;
import org.controlsfx.validation.ValidationResult;
import org.controlsfx.validation.Validator;

/**
 * Collection of Validators
 * Validators are used by ControlsFX
 */
public class ValidatorsUtil {

    /**
     * Validator for TextField. Text must be between minLength and maxLength characters long
     *
     * @param fieldName name of validating field
     * @param minLength minimal length of text
     * @param maxLength maximal length of text
     * @return a Validator instance
     */
    public static Validator<String> getTextLengthValidation(String fieldName, int minLength, int maxLength) {
        return new Validator<String>() {
            @Override
            public ValidationResult apply(Control control, String value) {
                if (minLength < -1) {
                    boolean condition = value.length() > maxLength;
                    String text = String.format("%s can have a maximum of %d characters", fieldName, maxLength);
                    return ValidationResult.fromMessageIf(control, text, Severity.ERROR, condition);
                } else if (maxLength < -1) {
                    boolean condition = value.length() < minLength;
                    String text = String.format("%s must be at least %d characters long", fieldName, minLength);
                    return ValidationResult.fromMessageIf(control, text, Severity.ERROR, condition);
                } else {
                    boolean condition = value.length() < minLength || value.length() > maxLength;
                    String text = String.format("%s must be beetween %d and %d characters long", fieldName, minLength, maxLength);
                    return ValidationResult.fromMessageIf(control, text, Severity.ERROR, condition);
                }
            }
        };
    }
}
