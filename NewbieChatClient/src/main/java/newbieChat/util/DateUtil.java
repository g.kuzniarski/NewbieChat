package newbieChat.util;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class DateUtil {

    public static String formatOffsetDateTimeToZonedDateTime(OffsetDateTime offsetDateTime) {
        return offsetDateTime.toZonedDateTime().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).withZone(ZoneId.of("Europe/Warsaw")));
    }
}
