package newbieChat.util;

import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import newbieChat.controller.WindowController;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static newbieChat.dictionary.Images.DEFAULT_AVATAR_IMAGE_FILE;

public class JavaFXUtil {
    private static Map<String, Image> imagesCache = new HashMap<>();

    public static void changeScene(Stage primaryStage, String templatePath, String windowTitle) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(JavaFXUtil.class.getResource(".." + File.separator + ".." + File.separator + templatePath));
        Pane windowLogin = loader.load();
        WindowController controller = loader.getController();

        Scene scene = new Scene(windowLogin);
        scene.getStylesheets().add(JavaFXUtil.class.getResource(".." + File.separator + ".." + File.separator + "main.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.setTitle(windowTitle);
        primaryStage.show();

        controller.setMainController(primaryStage);
    }

    public static void showNewWindow(String templatePath, String windowTitle) throws IOException {
        Stage stage = new Stage();

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(JavaFXUtil.class.getResource(".." + File.separator + ".." + File.separator + templatePath));
        Pane windowLogin = loader.load();
        WindowController controller = loader.getController();

        Scene scene = new Scene(windowLogin);
        scene.getStylesheets().add(JavaFXUtil.class.getResource(".." + File.separator + ".." + File.separator + "main.css").toExternalForm());
        stage.setScene(scene);
        stage.setTitle(windowTitle);
        stage.show();

        controller.setMainController(stage);
    }

    public static WindowController prepareNewWindow(String templatePath, String windowTitle) throws IOException {
        Stage stage = new Stage();

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(JavaFXUtil.class.getResource(".." + File.separator + ".." + File.separator + templatePath));
        Pane pane = loader.load();
        WindowController controller = loader.getController();

        Scene scene = new Scene(pane);
        scene.getStylesheets().add(JavaFXUtil.class.getResource(".." + File.separator + ".." + File.separator + "main.css").toExternalForm());
        stage.setScene(scene);
        stage.setTitle(windowTitle);

        controller.setMainController(stage);

        return controller;
    }

    public static void hideStage(Event e) {
        ((Stage) ((Node) e.getSource()).getScene().getWindow()).close();
    }

    public static void generateImageViewCircleFromImageURL(ImageView imageView, String imageURL) {
        Image avatarImage;
        if (imageURL != null && !imageURL.isEmpty()) {
            if (imagesCache.containsKey(imageURL)) {
                avatarImage = imagesCache.get(imageURL);
            } else {
                try {
                    avatarImage = new Image(imageURL);
                    imagesCache.put(imageURL, avatarImage);
                } catch (IllegalArgumentException e) {
                    avatarImage = new Image("file://" + JavaFXUtil.class.getResource(".." + File.separator + ".." + File.separator + DEFAULT_AVATAR_IMAGE_FILE).getPath());
                }
            }
        } else {
            avatarImage = new Image("file://" + JavaFXUtil.class.getResource(".." + File.separator + ".." + File.separator + DEFAULT_AVATAR_IMAGE_FILE).getPath());
        }

        imageView.setImage(avatarImage);
        Circle avatarCircle = new Circle(
                imageView.getFitHeight() / 2,
                imageView.getFitWidth() / 2,
                imageView.getFitHeight() < imageView.getFitWidth() ? imageView.getFitHeight() / 2 : imageView.getFitWidth() / 2);
        imageView.setClip(avatarCircle);
    }
}
