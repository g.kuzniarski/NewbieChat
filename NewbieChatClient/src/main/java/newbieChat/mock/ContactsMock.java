//package newbieChat.mock;
//
//import newbieChat.dto.ContactDTO;
//import newbieChat.dto.UserProfileDTO;
//
//import java.util.ArrayList;
//
//public class ContactsMock {
//
//    private static ArrayList<ContactDTO> contactList;
//    private static ArrayList<UserProfileDTO> userProfileList;
////    private static ContactDTO userProfile = new ContactDTO(1L, "kuzniarex", "Gniewomirrrr Kuźniarski", UserStatusType.ONLINE, "Expimy z Gośką", "https://media.licdn.com/dms/image/C5603AQG4ZNzWFYNy2g/profile-displayphoto-shrink_200_200/0?e=1536796800&v=beta&t=Ez1r_BEQr1d1p2Bhui9n4C7aS706f1DPySubA9umEGI");
////
////    static {
////        contactList = new ArrayList<>();
////        contactList.add(new ContactDTO(1L, "kuzniarix", "Gniewomir Kuźniarski", "Dostępny", "Expimy z Gośką", ""));
////        contactList.add(new ContactDTO(2L, "gosiaczek", "Gosia K", "Dostępna", "Klikam", ""));
////        contactList.add(new ContactDTO(3L, "szczepnix", "Szczepan Szczepański", "Nie przeszkadzać", "Jem", ""));
////        contactList.add(new ContactDTO(4L, "WpiszNazwe123", "Dariusz Czapliński", "Niedostępny", "U babci :/", ""));
////    }
//
//    static {
//        userProfileList = new ArrayList<>();
//        userProfileList.add(new UserProfileDTO(1L, "kuzniarix", "Gniewomir Kuźniarski", 23, "Male", "Poland", ""));
//        userProfileList.add(new UserProfileDTO(2L, "gosiaczek", "Gosia K",  23, "Female", "Poland", ""));
//        userProfileList.add(new UserProfileDTO(3L, "szczepnix", "Szczepan Szczepański",  26, "Male", "Poland", ""));
//        userProfileList.add(new UserProfileDTO(4L, "WpiszNazwe123", "Dariusz Czapliński",  12, "Male", "Poland", ""));
//    }
//
////    public static ContactDTO getCurrentUserProfile() {
////        return userProfile;
////    }
//
//    public static ArrayList<ContactDTO> getContactListMock() {
//        return contactList;
//    }
//
//    public static UserProfileDTO findUserProfileById(Long id) {
//        return userProfileList
//                .stream()
//                .filter(userProfileDTO -> userProfileDTO.getId().equals(id))
//                .findFirst()
//                .orElseGet(UserProfileDTO::new);
//    }
//}
