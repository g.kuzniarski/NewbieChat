package newbieChat.enums;

import java.io.Serializable;

public enum ServerCommunicationObjectType implements Serializable {
    LOGIN("login"),
    LOGOUT("logout"),
    MESSAGE_ADD("message/add"),
    MESSAGE_LIST("message/list"),
    MESSAGE_OTHER("message/other"),
    USER_ME("user/me"),
    USER_UPDATE("user/update"),
    USER_DELETE("user/delete"),
    USER_ME_STATUS("user/me/status"),
    USER_ME_STATUS_DETAILS("user/me/statusDetails"),
    USER_LIST("user/list"),
    USER_LIST_FILTERS("user/list_with_filters"),
    CONTACT_ADD("contact/add"),
    CONTACT_ADD_LOGIN("contact/add_login"),
    CONTACT_DELETE("contact/delete"),
    CONTACT_DETAILS("contact/details"),
    INVITE_LIST("invite/list"),
    INVITE_DECIDE("invite/decide"),
    MATCH_FIND("match/find"),
    MATCH_DECIDE("match/decide"),
    SETTINGS_GET("settings/get");

    private String type;

    ServerCommunicationObjectType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
}
