package newbieChat.dictionary;

import java.io.File;

public class Views {
    public static final String LOGIN_WINDOW = "templates" + File.separator + "LoginWindow.fxml";
    public static final String CONTACT_LIST_WINDOW = "templates" + File.separator + "ContactListWindow.fxml";
    public static final String CONTACT_LIST_ELEMENT = "templates" + File.separator + "ContactListElement.fxml";
    public static final String USER_CATALOG_WINDOW = "templates" + File.separator + "UserCatalogWindow.fxml";
    public static final String ADD_CONTACT_WINDOW = "templates" + File.separator + "AddContactWindow.fxml";
    public static final String CHAT_WINDOW = "templates" + File.separator + "ChatWindow.fxml";
    public static final String CHAT_MESSAGE_ELEMENT = "templates" + File.separator + "ChatMessageElement.fxml";
    public static final String USER_DETAILS_WINDOW = "templates" + File.separator + "UserDetailsWindow.fxml";
    public static final String SETTINGS_WINDOW = "templates" + File.separator + "SettingsWindow.fxml";
    public static final String INVITES_WINDOW = "templates" + File.separator + "InvitesWindow.fxml";
    public static final String INVITES_ELEMENT = "templates" + File.separator + "InvitesElement.fxml";
    public static final String MESSAGES_WINDOW = "templates" + File.separator + "MessagesWindow.fxml";
    public static final String MESSAGES_ELEMENT = "templates" + File.separator + "MessagesElement.fxml";
    public static final String MATCH_WINDOW = "templates" + File.separator + "MatchWindow.fxml";
}
