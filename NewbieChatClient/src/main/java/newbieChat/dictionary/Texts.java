package newbieChat.dictionary;

import java.util.HashSet;
import java.util.Set;

public class Texts {
    //Titles
    public static final String LOGIN_WINDOW_TITLE = "Newbie Chat";
    public static final String CONTACT_LIST_WINDOW_TITLE = "Newbie Chat";
    public static final String USER_CATALOG_WINDOW_TITLE = "User catalog";
    public static final String ADD_CONTACT_WINDOW_TITLE = "Add new contact";
    public static final String CHAT_WINDOW_TITLE = "Chat";
    public static final String USER_DETAILS_WINDOW_TITLE = "Details";
    public static final String SETTINGS_WINDOW_TITLE = "Settings";
    public static final String INVITES_WINDOW_TITLE = "Invites";
    public static final String MESSAGES_WINDOW_TITLE = "Messages";
    public static final String MATCH_WINDOW_TITLE = "Match";

    //LOGIN WINDOW
    public static final String BAD_CREDENTIALS = "Bad login or/and password";


    //Menu bar
    //FILE
    public static final String MENU_BAR_MENU_FILE = "File";
    public static final String MENU_BAR_ITEM_ADD_CONTACT = "Add contact";
    public static final String MENU_BAR_ITEM_SEARCH = "Search";
    public static final String MENU_BAR_ITEM_SETTINGS = "Settings";
    public static final String MENU_BAR_ITEM_EXIT = "Exit";
    //EDIT
    public static final String MENU_BAR_MENU_EDIT = "Edit";
    public static final String MENU_BAR_ITEM_DETAILS = "Details";
    public static final String MENU_BAR_ITEM_DELETE = "Delete";
    //HELP
    public static final String MENU_BAR_MENU_HELP = "Help";
    public static final String MENU_BAR_ITEM_ABOUT = "About";

    //Context menu
    public static final String CONTACT_LIST_CONTEXT_MENU_ITEM_CHAT = "Chat";
    public static final String CONTACT_LIST_CONTEXT_MENU_ITEM_DETAILS = "Details";
    public static final String CONTACT_LIST_CONTEXT_MENU_ITEM_DELETE = "Delete";


    public static final String MENU_BAR_ITEM_ADDRESS_BOOK = "Address book";
    public static final String MENU_BAR_ITEM_INVITES = "Invites";
    public static final String MENU_BAR_ITEM_MESSAGES = "Messages";


    //Status details
    public static final String ADD_STATUS_DETAILS = "Add description...";


    //Variables used in type lists
    public static final String MALE = "Male";
    public static final String FEMALE = "Female";
    public static final String OTHER = "Other";

    public static final String BEATER = "Beater";
    public static final String SOCIALIZER = "Socializer";
    public static final String EXPLORER = "Explorer";
    public static final String ACHIEVER = "Achiever";

    public static final String MANY_HOUR_DAY = "5+ hours a day";
    public static final String FEW_HOUR_DAY = "few hours a day";
    public static final String FEW_HOURS_WEEK = "few hours a week";
    public static final String FEW_HOURS_NOT_EVERY_WEEK = "few hours not every week ";

    public static final String MORNING = "Morning";
    public static final String NOON = "Noon";
    public static final String EVENING = "Evening";
    public static final String NIGHT = "Night";

    public static final String POLSKI = "Polski";
    public static final String ENGLISH = "English";

    //Type lists
    public static final Set<String> SEX_TYPES = new HashSet<String>() {{
        add(MALE);
        add(FEMALE);
        add(OTHER);
    }};

    public static final Set<String> GAMING_STYLES_TYPES = new HashSet<String>() {{
        add(BEATER);
        add(SOCIALIZER);
        add(EXPLORER);
        add(ACHIEVER);
    }};

    public static final Set<String> TIME_SPENT_TYPES = new HashSet<String>() {{
        add(MANY_HOUR_DAY);
        add(FEW_HOUR_DAY);
        add(FEW_HOURS_NOT_EVERY_WEEK);
        add(FEW_HOURS_WEEK);
    }};

    public static final Set<String> TIME_OF_DAY_TYPES = new HashSet<String>() {{
        add(MORNING);
        add(NOON);
        add(EVENING);
        add(NIGHT);
    }};

    public static final Set<String> LANGUAGES = new HashSet<String>() {{
        add(POLSKI);
        add(ENGLISH);
    }};

}
