package newbieChat;

import javafx.application.Application;
import javafx.stage.Stage;
import newbieChat.util.JavaFXUtil;

import java.io.IOException;

import static newbieChat.dictionary.Texts.LOGIN_WINDOW_TITLE;
import static newbieChat.dictionary.Views.LOGIN_WINDOW;

public class Main extends Application {

    private Stage primaryStage;
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        initLoginWindow();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        System.exit(0);
    }

    private void initLoginWindow() {
        try {
            JavaFXUtil.changeScene(primaryStage, LOGIN_WINDOW, LOGIN_WINDOW_TITLE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        launch(args);
    }
}
