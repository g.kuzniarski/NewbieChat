package newbieChat.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import newbieChat.model.Client;
import newbieChat.util.JavaFXUtil;
import org.controlsfx.glyphfont.FontAwesome;

public class ContactAddNewController extends WindowController {

    @FXML
    Button addButton;
    @FXML
    Button cancelButton;
    @FXML
    TextField loginTextField;

    @Override
    void initialize() {
        //Set buttons icons
        addButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.USER));
        cancelButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.TIMES));

        //Button actions
        addButton.setOnAction(this::addButtonAction);
        cancelButton.setOnAction(JavaFXUtil::hideStage);
    }

    @Override
    void reloadData() {

    }

    private void addButtonAction(ActionEvent event) {
        Client.getInstance().addContactByLogin(loginTextField.getText());
        JavaFXUtil.hideStage(event);
    }
}
