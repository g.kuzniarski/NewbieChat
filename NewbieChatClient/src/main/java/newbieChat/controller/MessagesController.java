package newbieChat.controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import newbieChat.dto.UserProfileDTO;
import newbieChat.util.JavaFXUtil;
import newbieChat.view.MessagesElement;
import org.controlsfx.glyphfont.FontAwesome;

import java.util.List;
import java.util.stream.Collectors;

public class MessagesController extends WindowController {

    @FXML
    ListView<MessagesElement> messagesElementListView;
    @FXML
    Button closeButton;

    public ObservableList<MessagesElement> messageElementObservableList = FXCollections.observableArrayList();

    private static MessagesController instance;

    public static MessagesController getInstance() {
        return instance;
    }

    @Override
    public void initialize() {
        instance = this;

        closeButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.TIMES));
        closeButton.setOnAction(JavaFXUtil::hideStage);

        messagesElementListView.setItems(messageElementObservableList);
    }

    @Override
    void reloadData() {
        client.getOtherMessages();
    }

    public void refreshMessagesWindow(List<UserProfileDTO> contactDTOList){
        Platform.runLater(() -> {
            messageElementObservableList.clear();
            messageElementObservableList.addAll(contactDTOList
                    .stream()
                    .map(this::mapUserProfileDTOToInvitesElement)
                    .collect(Collectors.toList()));
        });
    }

    private MessagesElement mapUserProfileDTOToInvitesElement(UserProfileDTO userProfileDTO) {
        MessagesElement messagesElement = new MessagesElement();
        messagesElement.setContact(userProfileDTO);
        messagesElement.init();
        return messagesElement;
    }
}
