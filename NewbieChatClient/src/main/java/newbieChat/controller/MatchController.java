package newbieChat.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import newbieChat.dto.InviteDTO;
import newbieChat.dto.MatchDTO;
import newbieChat.dto.UserFullProfileDTO;
import newbieChat.util.JavaFXUtil;
import newbieChat.view.InvitesElement;
import org.controlsfx.glyphfont.FontAwesome;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static newbieChat.util.JavaFXUtil.generateImageViewCircleFromImageURL;

public class MatchController extends WindowController {


    @FXML
    private ImageView basicAvatar;
    @FXML
    private Label basicNicknameLabel;
    @FXML
    private Label basicNameLabel;
    @FXML
    private Label basicAgeValueLabel;
    @FXML
    private Label basicSexValueLabel;
    @FXML
    private Label basicCountryValueLabel;
    @FXML
    private Label detailsAgeValueLabel;
    @FXML
    private Label detailsSexValueLabel;
    @FXML
    private Label detailsCountryValueLabel;
    @FXML
    private Label cityValueLabel;
    @FXML
    private Label gamingStyleValueLabel;
    @FXML
    private Label timeSpentValueLabel;
    @FXML
    private Label timeOfDayValueLabel;
    @FXML
    private ComboBox<String> gameComboBox;
    @FXML
    private GridPane gameProfilesGridPane;
    @FXML
    Button acceptButton;
    @FXML
    Button discardButton;

    private ObjectMapper objectMapper = new ObjectMapper();
    private UserFullProfileDTO userProfile;
    private ObservableList<String> userGames = FXCollections.observableArrayList();

    private static MatchController instance;

    public static MatchController getInstance() {
        return instance;
    }

    @Override
    public void initialize() {
        instance = this;

        acceptButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.CHECK));
        acceptButton.setOnAction(this::acceptButtonAction);

        discardButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.TIMES));
        discardButton.setOnAction(this::discardButtonAction);

        gameComboBox.setItems(userGames);
        gameComboBox.setOnAction(this::gameComboBoxAction);

        gameProfilesGridPane.getChildren().clear();
    }

    private void acceptButtonAction(ActionEvent event) {
        client.changeMatchState(userProfile.getId(), "ACCEPT");
        closeWindow(event);
    }

    private void discardButtonAction(ActionEvent event) {
        client.changeMatchState(userProfile.getId(), "DISCARD");
        closeWindow(event);
    }

    private void closeWindow(ActionEvent event) {
        JavaFXUtil.hideStage(event);
    }

    @Override
    void reloadData() {
        client.getMatch();
    }

    private void gameComboBoxAction(ActionEvent event) {
        generateGameView(gameComboBox.getSelectionModel().getSelectedItem());
    }

    private void updateUserProfile() {
        if (userProfile.getNickname() != null && !userProfile.getNickname().isEmpty()) {
            basicNicknameLabel.setText(userProfile.getNickname());
        }
        if (userProfile.getName() != null && !userProfile.getName().isEmpty()) {
            basicNameLabel.setText(userProfile.getName());
        }
        if (userProfile.getAge() != null && userProfile.getAge() > 0) {
            basicAgeValueLabel.setText(String.valueOf(userProfile.getAge()));
        }
        if (userProfile.getSex() != null && !userProfile.getSex().isEmpty()) {
            basicSexValueLabel.setText(userProfile.getSex());
        }
        if (userProfile.getCountry() != null && !userProfile.getCountry().isEmpty()) {
            basicCountryValueLabel.setText(userProfile.getCountry());
        }
        if (userProfile.getAge() != null && !userProfile.getAge().toString().isEmpty()) {
            detailsAgeValueLabel.setText(userProfile.getAge().toString());
        }
        if (userProfile.getSex() != null && !userProfile.getSex().isEmpty()) {
            detailsSexValueLabel.setText(userProfile.getSex());
        }
        if (userProfile.getCountry() != null && !userProfile.getCountry().isEmpty()) {
            detailsCountryValueLabel.setText(userProfile.getCountry());
        }
        if (userProfile.getCity() != null && !userProfile.getCity().isEmpty()) {
            cityValueLabel.setText(userProfile.getCity());
        }
        if (userProfile.getGamingStyle() != null && !userProfile.getGamingStyle().isEmpty()) {
            gamingStyleValueLabel.setText(userProfile.getGamingStyle());
        }
        if (userProfile.getTimeSpent() != null && !userProfile.getTimeSpent().isEmpty()) {
            timeSpentValueLabel.setText(userProfile.getTimeSpent());
        }
        if (userProfile.getTimeOfDay() != null && !userProfile.getTimeOfDay().isEmpty()) {
            timeOfDayValueLabel.setText(userProfile.getTimeOfDay());
        }

        userGames.clear();
        if (userProfile.getGameProfileList() != null) {
            userProfile.getGameProfileList().forEach(gameProfileDTO -> {
                userGames.add(gameProfileDTO.getGame());
            });
            gameComboBox.getSelectionModel().select(0);
        }

        generateImageViewCircleFromImageURL(this.basicAvatar, userProfile.getAvatarPath());
    }

    private void generateGameView(String game) {
        if (game != null && !game.isEmpty()) {
            gameProfilesGridPane.getChildren().clear();
            final Integer[] index = {1};

            userProfile.getGameProfileList()
                    .stream()
                    .filter(gameProfileDTO -> gameProfileDTO.getGame().equals(game))
                    .findFirst()
                    .ifPresent(gameProfileDTO -> {
                        try {
                            Map<String, String> gameDetails = objectMapper.readValue(gameProfileDTO.getDetails(), new TypeReference<HashMap<String, String>>() {
                            });
                            gameDetails.forEach((key, value) -> {
                                Label keyLabel = new Label(key);
                                keyLabel.setFont(Font.font(13));
                                GridPane.setMargin(keyLabel, new Insets(7));

                                Label valueLabel = new Label(value);
                                valueLabel.setFont(Font.font(13));
                                GridPane.setMargin(valueLabel, new Insets(7));

                                gameProfilesGridPane.add(keyLabel, 0, index[0]);
                                gameProfilesGridPane.add(valueLabel, 1, index[0]);
                                index[0]++;
                            });
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
        }
    }

    public void refreshMatch(UserFullProfileDTO userFullProfileDTO) {
        this.userProfile = userFullProfileDTO;
        Platform.runLater(() -> {
            updateUserProfile();
            primaryStage.setTitle("Details of " + userFullProfileDTO.getNickname());
        });
    }

    private InvitesElement mapInviteDTOToInvitesElement(InviteDTO inviteDTO) {
        InvitesElement invitesElement = new InvitesElement();
        invitesElement.setContact(inviteDTO);
        invitesElement.init();
        return invitesElement;
    }
}
