package newbieChat.controller;

import javafx.application.Platform;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import newbieChat.dictionary.Texts;
import newbieChat.dto.ContactDTO;
import newbieChat.dto.UserProfileDTO;
import newbieChat.model.UserStatusType;
import newbieChat.util.JavaFXUtil;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserCatalogController extends WindowController {

    private static final int PAGE_SIZE = 10;

    @FXML
    TableView<UserProfileDTO> usersTable;
    @FXML
    Button searchButton;
    @FXML
    Button closeButton;
    @FXML
    TextField loginTextField;
    @FXML
    TextField nameTextField;
    @FXML
    TextField countryTextField;
    @FXML
    Spinner<Integer> ageMinSpinner;
    @FXML
    Spinner<Integer> ageMaxSpinner;
    @FXML
    ChoiceBox<String> gamingStyleChoiceBox;
    @FXML
    ChoiceBox<UserStatusType> statusChoiceBox;
    @FXML
    Button clearGamingStyleButton;
    @FXML
    Button clearStatusButton;
    @FXML
    Pagination pagination;

    private ObservableList<UserProfileDTO> usersList = FXCollections.observableArrayList();
    private List<UserProfileDTO> allUsersList = new ArrayList<>();

    private static UserCatalogController instance;

    public static UserCatalogController getInstance() {
        return instance;
    }

    @Override
    public void initialize() {
        instance = this;

        //Set buttons icons
        searchButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.SEARCH));
        closeButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.TIMES));
        clearGamingStyleButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.TIMES));
        clearStatusButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.TIMES));

        //Button actions
        searchButton.setOnAction(this::searchButtonAction);
        closeButton.setOnAction(JavaFXUtil::hideStage);
        clearGamingStyleButton.setOnAction(this::clearGamingStyleButtonAction);
        clearStatusButton.setOnAction(this::clearStatusButtonAction);

        //Init spinners
        ageMinSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 100, 0, 1));
        ageMaxSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 100, 100, 1));

        //Init pagination
        pagination.setPageCount(1);
        pagination.setCurrentPageIndex(0);
        pagination.setMaxPageIndicatorCount(5);
        pagination.currentPageIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                changePage(newValue.intValue());
            }
        });

        //Fill choice boxes
        fillStatusChoiceBox();
        fillGamingStyleChoiceBox();

        //Initialize table
        initTable();
    }

    @Override
    void reloadData() {

    }

    private void changePage(int newValue) {
        List<UserProfileDTO> pagedUsers;
        if (allUsersList.size() < (newValue + 1) * PAGE_SIZE) {
            pagedUsers = allUsersList.subList(newValue * PAGE_SIZE, allUsersList.size());
        } else {
            pagedUsers = allUsersList.subList(newValue * PAGE_SIZE, (newValue + 1) * PAGE_SIZE);
        }
        usersList.clear();
        usersList.addAll(pagedUsers);
    }

    private void searchButtonAction(ActionEvent event) {
        Map<String, Object> searchData = collectSearchData();
        client.getUserListWithFilters(searchData);
    }

    private void clearGamingStyleButtonAction(ActionEvent event) {
        gamingStyleChoiceBox.getSelectionModel().clearSelection();
    }

    private void clearStatusButtonAction(ActionEvent event) {
        statusChoiceBox.getSelectionModel().clearSelection();
    }

    private void fillStatusChoiceBox() {
        ObservableList<UserStatusType> statusTypeList = FXCollections.observableArrayList();
        statusTypeList.addAll(UserStatusType.values());
        statusChoiceBox.setItems(statusTypeList);
    }

    private void fillGamingStyleChoiceBox() {
        ObservableList<String> gamingStyleList = FXCollections.observableArrayList();
        gamingStyleList.addAll(Texts.GAMING_STYLES_TYPES);
        gamingStyleChoiceBox.setItems(gamingStyleList);
    }

    private Map<String, Object> collectSearchData() {
        Map<String, Object> filters = new HashMap<>();
        if (loginTextField.getText() != null && !loginTextField.getText().isEmpty()) {
            filters.put("login", loginTextField.getText());
        }
        if (nameTextField.getText() != null && !nameTextField.getText().isEmpty()) {
            filters.put("name", nameTextField.getText());
        }
        if (countryTextField.getText() != null && !countryTextField.getText().isEmpty()) {
            filters.put("country", countryTextField.getText());
        }
        if (statusChoiceBox.getSelectionModel().getSelectedItem() != null) {
            filters.put("status", statusChoiceBox.getSelectionModel().getSelectedItem().toString());
        }
        if (gamingStyleChoiceBox.getSelectionModel().getSelectedItem() != null) {
            filters.put("gamingStyle", gamingStyleChoiceBox.getSelectionModel().getSelectedItem());
        }
        if (ageMinSpinner.getValue() != null) {
            filters.put("ageMin", ageMinSpinner.getValue());
        }
        if (ageMaxSpinner.getValue() != null) {
            filters.put("ageMax", ageMaxSpinner.getValue());
        }
        return filters;
    }

    private void initTable() {
        usersTable.setEditable(false);
        usersTable.getColumns().clear();

        TableColumn<UserProfileDTO, String> statusColumn = new TableColumn<>("Status");
        statusColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getStatus()));
        statusColumn.setEditable(false);

        TableColumn<UserProfileDTO, String> loginColumn = new TableColumn<>("Nickname");
        loginColumn.setCellValueFactory(new PropertyValueFactory<>("Nickname"));
        loginColumn.setEditable(false);

        TableColumn<UserProfileDTO, String> nameColumn = new TableColumn<>("Name");
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("Name"));
        nameColumn.setEditable(false);

        TableColumn<UserProfileDTO, String> countryColumn = new TableColumn<>("Country");
        countryColumn.setCellValueFactory(new PropertyValueFactory<>("Country"));
        countryColumn.setEditable(false);

        TableColumn<UserProfileDTO, Integer> ageColumn = new TableColumn<>("Age");
        ageColumn.setCellValueFactory(cellData -> new SimpleIntegerProperty(cellData.getValue().getAge()).asObject());
        ageColumn.setEditable(false);

        TableColumn<UserProfileDTO, Integer> gamingStyleColumn = new TableColumn<>("Gaming style");
        gamingStyleColumn.setCellValueFactory(new PropertyValueFactory<>("gamingStyle"));
        gamingStyleColumn.setEditable(false);

        TableColumn<UserProfileDTO, UserProfileDTO> actionColumn = new TableColumn<>("Action");
        actionColumn.setCellFactory(new Callback<TableColumn<UserProfileDTO, UserProfileDTO>, TableCell<UserProfileDTO, UserProfileDTO>>() {
            @Override
            public TableCell<UserProfileDTO, UserProfileDTO> call(final TableColumn<UserProfileDTO, UserProfileDTO> param) {
                return new TableCell<UserProfileDTO, UserProfileDTO>() {

                    private final Button messageButton = new Button();
                    private final Button addContactButton = new Button();

                    {
                        Glyph msg1 = fontAwesome.create(FontAwesome.Glyph.ENVELOPE);
                        msg1.setFontSize(14);
                        messageButton.setGraphic(msg1);
                        messageButton.setOnAction(event -> messageButtonAction(event, getTableView().getItems().get(getIndex())));

                        Glyph add1 = fontAwesome.create(FontAwesome.Glyph.USER_PLUS);
                        add1.setFontSize(14);
                        addContactButton.setGraphic(add1);
                        addContactButton.setOnAction(event -> addContactButtonAction(event, getTableView().getItems().get(getIndex())));
                    }

                    @Override
                    public void updateItem(UserProfileDTO item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(new HBox(messageButton, addContactButton));
                        }
                    }
                };
            }
        });

        usersTable.setItems(usersList);
        usersTable.getColumns().addAll(statusColumn, loginColumn, nameColumn, countryColumn, ageColumn, gamingStyleColumn, actionColumn);
    }

    private void messageButtonAction(ActionEvent event, UserProfileDTO item) {
        if (item != null && !item.getId().equals(ContactListController.getInstance().getCurrentUserProfile().getId())) {
            ContactListController.getInstance().openChatWindow(new ContactDTO(item));
        }
    }

    private void addContactButtonAction(ActionEvent event, UserProfileDTO item) {
        if (item != null) {
            if (!item.getId().equals(ContactListController.getInstance().getCurrentUserProfile().getId())
                    && ContactListController.getInstance().getContactList().stream().noneMatch(contactDTO -> contactDTO.getId().equals(item.getId()))) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Add Contact");
                alert.setHeaderText(null);
                alert.setContentText(String.format("Do you really want to add %s to your contact list?", item.getNickname()));

                alert.showAndWait().ifPresent(buttonType -> {
                    if (buttonType == ButtonType.OK) {
                        client.addContact(item.getId());
                    }
                });
            } else {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Add Contact");
                alert.setHeaderText(null);
                alert.setContentText(String.format("%s is already on your contact list", item.getNickname()));

                alert.showAndWait();
            }
        }
    }

    public void refreshUserTableContent(List<UserProfileDTO> users) {
        Platform.runLater(() -> {
            allUsersList.clear();
            allUsersList.addAll(users);

            if (users.size() <= PAGE_SIZE) {
                pagination.setPageCount(1);
            } else {
                int modulo = users.size() % PAGE_SIZE == 0 ? 0 : 1;
                pagination.setPageCount((users.size() / PAGE_SIZE) + modulo);
            }
            pagination.setCurrentPageIndex(0);
            pagination.setMaxPageIndicatorCount(5);
            changePage(0);
        });
    }

}
