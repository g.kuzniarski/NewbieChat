package newbieChat.controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import newbieChat.dictionary.Texts;
import newbieChat.model.Client;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.validation.ValidationSupport;

import java.io.IOException;

import static newbieChat.dictionary.Texts.CONTACT_LIST_WINDOW_TITLE;
import static newbieChat.dictionary.Views.CONTACT_LIST_WINDOW;
import static newbieChat.util.JavaFXUtil.hideStage;
import static newbieChat.util.JavaFXUtil.showNewWindow;

/**
 * LoginController
 */
public class LoginController extends WindowController {

    @FXML
    Button loginButton;
    @FXML
    Button cancelButton;
    @FXML
    Button registerButton;
    @FXML
    TextField loginTextField;
    @FXML
    PasswordField passwordField;
    @FXML
    ImageView logoImage;
    @FXML
    Label badLabel;

    private static final Logger logger = LogManager.getLogger(LoginController.class);

    private ValidationSupport validationSupport = new ValidationSupport();

    @Override
    void initialize() {
        //Set buttons icons
        loginButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.KEY));
        cancelButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.CHAIN_BROKEN));

        //Validation
        validationSupport.setErrorDecorationEnabled(false);
//        validationSupport.registerValidator(loginTextField, true, getTextLengthValidation("Login", 5, 16));
//        validationSupport.registerValidator(passwordField, true, getTextLengthValidation("Password", 4, 32));

        //Button actions
        loginButton.setOnAction(this::loginButtonAction);

        cancelButton.setOnAction(event -> Platform.exit());
    }

    @Override
    void reloadData() {
    }

    private void loginButtonAction(ActionEvent event) {
        if (!validationSupport.isInvalid()) {
            login(event);
        }
        validationSupport.setErrorDecorationEnabled(true);
    }

    private void login(ActionEvent event) {
        Client client = connect(event);
        if (client != null) {
            loginSuccessful(event);
        } else {
            loginFailure(event);
        }

    }

    private Client connect(ActionEvent e) {
        String serverAddress = "localhost";
        int serverPort = 1500;

        logger.info("Connecting to " + serverAddress);
        Client client = Client.initializeClient(serverAddress, serverPort);

        if (client.start(loginTextField.getText(), passwordField.getText())) {
            logger.info("Connected");
            return client;
        } else {
            logger.error("Connecting failed");
            client.disconnect();
            return null;
        }
    }

    private void loginSuccessful(ActionEvent event) {
        try {
            hideStage(event);
            showNewWindow(CONTACT_LIST_WINDOW, CONTACT_LIST_WINDOW_TITLE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loginFailure(ActionEvent event) {
        badLabel.setText(Texts.BAD_CREDENTIALS);
        logger.error("Login failure");
    }
}
