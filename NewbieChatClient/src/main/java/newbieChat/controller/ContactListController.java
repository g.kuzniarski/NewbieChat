package newbieChat.controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.WindowEvent;
import newbieChat.dictionary.Texts;
import newbieChat.dto.ContactDTO;
import newbieChat.dto.MessageDTO;
import newbieChat.dto.UserFullProfileDTO;
import newbieChat.model.UserStatusType;
import newbieChat.view.ContactListElement;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static newbieChat.dictionary.Texts.*;
import static newbieChat.dictionary.Views.*;
import static newbieChat.util.JavaFXUtil.*;

public class ContactListController extends WindowController {

    @FXML
    TextField filterTextField;
    @FXML
    Button searchButton;
    @FXML
    Button addButton;
    @FXML
    Button detailsButton;
    @FXML
    Button deleteButton;
    @FXML
    Button addressBookButton;
    @FXML
    Button invitesButton;
    @FXML
    Button messagesButton;
    @FXML
    GridPane currentUserGridPane;
    @FXML
    ImageView avatarImageView;
    @FXML
    Label nicknameLabel;
    @FXML
    Label statusDetailsLabel;
    @FXML
    MenuButton statusMenu;
    @FXML
    MenuBar menuBar;
    @FXML
    ListView<ContactListElement> contactListView;
    private GridPane statusDetailsEditGridPane;
    private TextArea statusDetailsEditTextArea;

    private ObservableList<ContactListElement> contactElementsObservableList = FXCollections.observableArrayList();
    private List<ContactListElement> allContactElementList = new ArrayList<>();
    private List<ContactDTO> contactList = new ArrayList<>();
    private ContactDTO currentUserProfile;
    private HashMap<String, WindowController> activeWindowsMap = new HashMap<>();
    private ContextMenu contextMenuContactList;

    private static ContactListController instance;

    public static ContactListController getInstance() {
        return instance;
    }

    @Override
    void initialize() {
        instance = this;

        //Set close action - Contact List is main view
        this.primaryStage.setOnCloseRequest(this::closeAction);

        //Set button icons
        generateButtonIcons();

        //Button actions
        generateButtonAndFilterActions();

        //Status details
        generateStatusDetails();

        //MenuButton
        generateMenuButton();

        //Menu actions
        generateMenuBar();

        //Generate ContextMenu of ContactListElement
        generateContextMenuContactList();

        //Set Items
        contactListView.setItems(contactElementsObservableList);

        //Get user and contacts
        reloadData();
    }

    @Override
    void reloadData() {
        client.getContactList();
        client.getCurrentUserProfile();
    }

    private void closeAction(WindowEvent windowEvent) {
        client.disconnect();
        System.exit(0);
    }

    private void generateStatusDetails() {
        statusDetailsEditGridPane = new GridPane();

        statusDetailsEditTextArea = new TextArea();
        statusDetailsEditTextArea.setMaxHeight(200L);
        statusDetailsEditTextArea.setWrapText(true);

        GridPane buttons = new GridPane();
        Button save = new Button("Save");
        Button cancel = new Button("Cancel");
        buttons.addColumn(0, save);
        buttons.addColumn(1, cancel);

        statusDetailsEditGridPane.addRow(0, statusDetailsEditTextArea);
        statusDetailsEditGridPane.addRow(1, buttons);

        GridPane.setMargin(save, new Insets(7, 7, 7, 7));
        GridPane.setMargin(cancel, new Insets(7, 7, 7, 7));
        GridPane.setMargin(statusDetailsEditTextArea, new Insets(7, 7, 7, 7));

        save.setOnAction(event -> statusDetailsSaveAction(event, statusDetailsEditTextArea.getText()));
        cancel.setOnAction(this::statusDetailsCancelAction);
        statusDetailsLabel.setOnMouseClicked(this::statusDetailsLabelAction);
    }

    private void generateButtonAndFilterActions() {
        searchButton.setOnAction(this::searchButtonAction);
        addButton.setOnAction(this::addButtonAction);
        detailsButton.setOnAction(this::detailsButtonAction);
        addressBookButton.setOnAction(this::addressBookButtonAction);
        deleteButton.setOnAction(this::deleteButtonAction);
        invitesButton.setOnAction(this::invitesButtonAction);
        messagesButton.setOnAction(this::messagesButtonAction);

        filterTextField.setOnKeyTyped(this::filterAction);
    }

    private void filterAction(KeyEvent keyEvent) {
        String filterText;
        if (keyEvent.getCharacter().length() == 1 && Character.isLetterOrDigit(keyEvent.getCharacter().charAt(0))) {
            filterText = filterTextField.getText().toLowerCase() + keyEvent.getCharacter().toLowerCase();
        } else {
            filterText = filterTextField.getText().toLowerCase();
        }

        contactElementsObservableList.clear();

        if (filterText.isEmpty()) {
            contactElementsObservableList.addAll(allContactElementList);
        } else {
            List<Long> filteredContactList = contactList
                    .stream()
                    .filter(contactDTO -> contactDTO.getNickname().toLowerCase().contains(filterText)
                            || contactDTO.getName().toLowerCase().contains(filterText))
                    .map(ContactDTO::getId)
                    .collect(Collectors.toList());

            contactElementsObservableList.addAll(
                    allContactElementList
                            .stream()
                            .filter(contactListElement -> filteredContactList.contains(contactListElement.getUserId()))
                            .collect(Collectors.toList())
            );
        }
    }

    private void generateButtonIcons() {
        searchButton.setText("");
        Glyph search = fontAwesome.create(FontAwesome.Glyph.SEARCH);
        search.setFontSize(18);
        searchButton.setGraphic(search);
        searchButton.setTooltip(new Tooltip(Texts.MENU_BAR_ITEM_SEARCH));

        addButton.setText("");
        Glyph add = fontAwesome.create(FontAwesome.Glyph.USER_PLUS);
        add.setFontSize(18);
        addButton.setGraphic(add);
        addButton.setTooltip(new Tooltip(Texts.MENU_BAR_ITEM_ADD_CONTACT));

        detailsButton.setText("");
        Glyph details = fontAwesome.create(FontAwesome.Glyph.LIST_ALT);
        details.setFontSize(18);
        detailsButton.setGraphic(details);
        detailsButton.setTooltip(new Tooltip(Texts.MENU_BAR_ITEM_DETAILS));

        deleteButton.setText("");
        Glyph delete = fontAwesome.create(FontAwesome.Glyph.USER_TIMES);
        delete.setFontSize(18);
        deleteButton.setGraphic(delete);
        deleteButton.setTooltip(new Tooltip(Texts.MENU_BAR_ITEM_DELETE));

        addressBookButton.setText("");
        Glyph addressBook = fontAwesome.create(FontAwesome.Glyph.BOOK);
        addressBook.setFontSize(18);
        addressBookButton.setGraphic(addressBook);
        addressBookButton.setTooltip(new Tooltip(Texts.MENU_BAR_ITEM_ADDRESS_BOOK));

        invitesButton.setText("");
        Glyph invites = fontAwesome.create(FontAwesome.Glyph.BELL);
        invites.setFontSize(18);
        invitesButton.setGraphic(invites);
        invitesButton.setTooltip(new Tooltip(Texts.MENU_BAR_ITEM_INVITES));

        messagesButton.setText("");
        Glyph messages = fontAwesome.create(FontAwesome.Glyph.ENVELOPE);
        messages.setFontSize(18);
        messagesButton.setGraphic(messages);
        messagesButton.setTooltip(new Tooltip(Texts.MENU_BAR_ITEM_MESSAGES));
    }

    private void generateMenuButton() {
        statusMenu.getItems().clear();
        Stream.of(UserStatusType.values()).forEach(status -> {
            MenuItem item = new MenuItem(status.toString());
            item.setOnAction(event -> changeMenuStatus(event, status.toString()));
            statusMenu.getItems().add(item);
        });
    }

    private void generateMenuBar() {
        menuBar.getMenus().clear();

        //FILE
        Menu file = new Menu(MENU_BAR_MENU_FILE);

        MenuItem addContact = new MenuItem(MENU_BAR_ITEM_ADD_CONTACT);
        addContact.setOnAction(this::addButtonAction);
        file.getItems().add(addContact);

        MenuItem search = new MenuItem(MENU_BAR_ITEM_SEARCH);
        search.setOnAction(this::searchButtonAction);
        file.getItems().add(search);

        file.getItems().add(new SeparatorMenuItem());

        MenuItem settings = new MenuItem(MENU_BAR_ITEM_SETTINGS);
        settings.setOnAction(this::settingsButtonAction);
        file.getItems().add(settings);

        file.getItems().add(new SeparatorMenuItem());

        MenuItem exit = new MenuItem(MENU_BAR_ITEM_EXIT);
        exit.setOnAction(event -> Platform.exit());
        file.getItems().add(exit);

        menuBar.getMenus().add(file);

        //EDIT
        Menu edit = new Menu(MENU_BAR_MENU_EDIT);

        MenuItem details = new MenuItem(MENU_BAR_ITEM_DETAILS);
        details.setOnAction(this::detailsButtonAction);
        edit.getItems().add(details);

        MenuItem delete = new MenuItem(MENU_BAR_ITEM_DELETE);
        delete.setOnAction(this::deleteButtonAction);
        edit.getItems().add(delete);

        menuBar.getMenus().add(edit);

        //HELP
        Menu help = new Menu(MENU_BAR_MENU_HELP);

        MenuItem about = new MenuItem(MENU_BAR_ITEM_ABOUT);
//        about.setOnAction(this::addButtonAction);
        help.getItems().add(about);

        menuBar.getMenus().add(help);
    }

    private ContactListElement mapContactDTOToContactListElement(ContactDTO contact) {
        ContactListElement element = new ContactListElement();
        element.setUserId(contact.getId());
        element.setAvatar(contact.getAvatarPath());
        element.setNicknameText(contact.getNickname() + " (" + contact.getName() + ")");
        element.setStatusText(contact.getStatus().toString());
        element.setStatusDetailsText(strongTrim(contact.getStatusDetails()));
        element.setOnMouseClicked(this::getContactElementMouseClickAction);
        return element;
    }

    private String strongTrim(String statusDetails) {
        if (statusDetails == null) {
            return "";
        } else {
            statusDetails = statusDetails
                    .replaceAll("\n", " ")
                    .replaceAll("\t", " ")
                    .trim();

            if (statusDetails.length() > 50) {
                statusDetails = statusDetails.substring(0, 50) + "...";
            }
            return statusDetails;
        }
    }

    private void getContactElementMouseClickAction(MouseEvent event) {
        if (event.getClickCount() == 2 && event.getButton() == MouseButton.PRIMARY) {
            chatWindowAction();
        } else if (event.getButton() == MouseButton.SECONDARY) {
            contextMenuContactList.show(contactListView.getSelectionModel().getSelectedItem(), event.getScreenX(), event.getScreenY());
        }
    }

    private void openNewChatWindow(ContactDTO contact) {
        if (contact != null) {
            try {
                ChatController chatController = (ChatController) prepareNewWindow(CHAT_WINDOW, CHAT_WINDOW_TITLE);
                chatController.setContact(contact);
                chatController.setOwner(currentUserProfile);
                chatController.showWindow();
                activeWindowsMap.put("CHAT_" + contact.getId(), chatController);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void openNewUserCatalogWindow() {
        try {
            UserCatalogController userCatalogController = (UserCatalogController) prepareNewWindow(USER_CATALOG_WINDOW, USER_CATALOG_WINDOW_TITLE);
            userCatalogController.showWindow();
            activeWindowsMap.put("USER_CATALOG", userCatalogController);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void openNewUserDetailsWindow(ContactDTO contactDTO) {
        if (contactDTO != null) {
            try {
                UserDetailsController userDetailsController = (UserDetailsController) prepareNewWindow(USER_DETAILS_WINDOW, USER_DETAILS_WINDOW_TITLE);
                userDetailsController.setUserId(contactDTO.getId());
                activeWindowsMap.put("DETAILS_" + contactDTO.getId(), userDetailsController);
                userDetailsController.showWindow();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void openNewSettingsWindow() {
        try {
            SettingsController settingsController = (SettingsController) prepareNewWindow(SETTINGS_WINDOW, SETTINGS_WINDOW_TITLE);
            settingsController.showWindow();
            activeWindowsMap.put("SETTINGS", settingsController);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void openNewInvitesWindow() {
        try {
            InvitesController invitesController = (InvitesController) prepareNewWindow(INVITES_WINDOW, INVITES_WINDOW_TITLE);
            invitesController.showWindow();
            activeWindowsMap.put("INVITES", invitesController);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void openNewMessagesWindow() {
        try {
            MessagesController messagesController = (MessagesController) prepareNewWindow(MESSAGES_WINDOW, MESSAGES_WINDOW_TITLE);
            messagesController.showWindow();
            activeWindowsMap.put("MESSAGES", messagesController);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void generateContextMenuContactList() {
        contextMenuContactList = new ContextMenu();
        ArrayList<MenuItem> menuItemList = new ArrayList<>();
        MenuItem menuItem;

        menuItem = new MenuItem(CONTACT_LIST_CONTEXT_MENU_ITEM_CHAT);
        menuItem.setOnAction(this::contextMenuChatAction);
        menuItemList.add(menuItem);

        menuItem = new MenuItem(CONTACT_LIST_CONTEXT_MENU_ITEM_DETAILS);
        menuItem.setOnAction(this::contextMenuDetailsAction);
        menuItemList.add(menuItem);

        menuItem = new MenuItem(CONTACT_LIST_CONTEXT_MENU_ITEM_DELETE);
        menuItem.setOnAction(this::contextMenuDeleteAction);
        menuItemList.add(menuItem);

        contextMenuContactList.getItems().addAll(menuItemList);
    }

    private void contextMenuDetailsAction(ActionEvent event) {
        detailsButtonAction(event);
    }

    private void contextMenuDeleteAction(ActionEvent event) {
        deleteContactFromList();
    }

    private void contextMenuChatAction(ActionEvent event) {
        chatWindowAction();
    }

    private void deleteContactFromList() {
        int index = this.contactListView.getSelectionModel().getSelectedIndex();
        if (index > -1) {
            client.deleteContact(contactElementsObservableList.get(index).getUserId());
        }
    }

    private void chatWindowAction() {
        Long selectedContactId = contactElementsObservableList.get(contactListView.getSelectionModel().getSelectedIndex()).getUserId();
        openChatWindow(selectedContactId);
    }

    public void openChatWindow(Long contactId) {
        if (activeWindowsMap.containsKey("CHAT_" + contactId) && activeWindowsMap.get("CHAT_" + contactId).isOpen()) {
            activeWindowsMap.get("CHAT_" + contactId).showWindow();
        } else {
            ContactDTO contactDTO = contactList.stream().filter(contact -> contact.getId().equals(contactId)).findFirst().orElse(null);
            openNewChatWindow(contactDTO);
        }
    }

    public void openChatWindow(ContactDTO contactDTO) {
        if (activeWindowsMap.containsKey("CHAT_" + contactDTO.getId()) && activeWindowsMap.get("CHAT_" + contactDTO.getId()).isOpen()) {
            activeWindowsMap.get("CHAT_" + contactDTO.getId()).showWindow();
        } else {
            openNewChatWindow(contactDTO);
        }
    }

    private void deleteButtonAction(ActionEvent event) {
        deleteContactFromList();
    }

    private void invitesButtonAction(ActionEvent event) {
        if (activeWindowsMap.containsKey("INVITES") && activeWindowsMap.get("INVITES").isOpen()) {
            activeWindowsMap.get("INVITES").showWindow();
        } else {
            openNewInvitesWindow();
        }
    }

    private void messagesButtonAction(ActionEvent event) {
        if (activeWindowsMap.containsKey("MESSAGES") && activeWindowsMap.get("MESSAGES").isOpen()) {
            activeWindowsMap.get("MESSAGES").showWindow();
        } else {
            openNewMessagesWindow();
        }
    }

    private void addressBookButtonAction(ActionEvent event) {
        if (activeWindowsMap.containsKey("USER_CATALOG") && activeWindowsMap.get("USER_CATALOG").isOpen()) {
            activeWindowsMap.get("USER_CATALOG").showWindow();
        } else {
            openNewUserCatalogWindow();
        }
    }

    private void addButtonAction(ActionEvent event) {
        try {
            showNewWindow(ADD_CONTACT_WINDOW, ADD_CONTACT_WINDOW_TITLE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void detailsButtonAction(ActionEvent event) {
        if (contactListView.getSelectionModel().getSelectedIndex() > -1) {
            Long selectedContactId = contactElementsObservableList.get(contactListView.getSelectionModel().getSelectedIndex()).getUserId();
            if (selectedContactId > -1) {
                openUserDetailsWindow(selectedContactId);
            }
        }
    }

    public void openUserDetailsWindow(Long selectedContactId) {
        if (activeWindowsMap.containsKey("DETAILS_" + selectedContactId) && activeWindowsMap.get("DETAILS_" + selectedContactId).isOpen()) {
            activeWindowsMap.get("DETAILS_" + selectedContactId).showWindow();
        } else {
            ContactDTO contactDTO = contactList.stream().filter(contact -> contact.getId().equals(selectedContactId)).findFirst().orElse(null);
            openNewUserDetailsWindow(contactDTO);
        }
    }

    public void openUserDetailsWindow(ContactDTO contactDTO) {
        if (activeWindowsMap.containsKey("DETAILS_" + contactDTO.getId()) && activeWindowsMap.get("DETAILS_" + contactDTO.getId()).isOpen()) {
            activeWindowsMap.get("DETAILS_" + contactDTO.getId()).showWindow();
        } else {
            openNewUserDetailsWindow(contactDTO);
        }
    }

    private void openSettingsWindow() {
        if (activeWindowsMap.containsKey("SETTINGS") && activeWindowsMap.get("SETTINGS").isOpen()) {
            activeWindowsMap.get("SETTINGS").showWindow();
        } else {
            openNewSettingsWindow();
        }
    }

    private void searchButtonAction(ActionEvent event) {
        openMatchWindow();
    }

    private void openMatchWindow() {
        if (activeWindowsMap.containsKey("MATCH") && activeWindowsMap.get("MATCH").isOpen()) {
            activeWindowsMap.get("MATCH").showWindow();
        } else {
            openNewMatchWindow();
        }
    }

    private void openNewMatchWindow() {
        try {
            MatchController matchController = (MatchController) prepareNewWindow(MATCH_WINDOW, MATCH_WINDOW_TITLE);
            matchController.showWindow();
            activeWindowsMap.put("MATCH", matchController);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void settingsButtonAction(ActionEvent event) {
        openSettingsWindow();
    }

    private void statusDetailsSaveAction(ActionEvent event, String statusDetails) {
        currentUserGridPane.getChildren().forEach(node -> node.setDisable(true));

        if (!statusDetails.equals(currentUserProfile.getStatusDetails())) {
            currentUserProfile.setStatusDetails(statusDetails);
            setStatusDetailsLabelText();
            client.changeStatusDetails(statusDetails);
        }

        currentUserGridPane.getChildren().forEach(node -> node.setDisable(false));

        currentUserGridPane.getChildren().removeIf(node -> GridPane.getRowIndex(node) != null && GridPane.getRowIndex(node) == 2);
        currentUserGridPane.addRow(2, statusDetailsLabel);
    }

    private void statusDetailsCancelAction(ActionEvent event) {
        currentUserGridPane.getChildren().removeIf(node -> GridPane.getRowIndex(node) != null && GridPane.getRowIndex(node) == 2);
        currentUserGridPane.addRow(2, statusDetailsLabel);
    }

    private void statusDetailsLabelAction(MouseEvent event) {
        statusDetailsEditTextArea.setText(statusDetailsLabel.getText());
        currentUserGridPane.getChildren().removeIf(node -> GridPane.getRowIndex(node) != null && GridPane.getRowIndex(node) == 2);
        currentUserGridPane.addRow(2, statusDetailsEditGridPane);
    }

    private void changeMenuStatus(ActionEvent event, String status) {
        if (!status.equals(currentUserProfile.getStatus().toString())) {
            currentUserProfile.setStatus(UserStatusType.valueOf(status.toUpperCase()));
            statusMenu.setText(status);
            client.changeStatus(status);
        }
    }

    public void refreshUserInfo(ContactDTO me) {
        currentUserProfile = me;
        generateImageViewCircleFromImageURL(avatarImageView, currentUserProfile.getAvatarPath());
        Platform.runLater(() -> {
            nicknameLabel.setText(currentUserProfile.getNickname() + " (" + currentUserProfile.getName() + ")");
            statusMenu.setText(currentUserProfile.getStatus().toString());
            setStatusDetailsLabelText();
        });
    }

    public void refreshContacts(List<ContactDTO> contactDTOList) {
        Platform.runLater(() -> {
            contactList.clear();
            contactList.addAll(contactDTOList);

            allContactElementList = contactDTOList
                    .stream()
                    .map(this::mapContactDTOToContactListElement)
                    .sorted((o1, o2) -> {
                        int compareOnline = compareStatus(o1.getStatus(), o2.getStatus());
                        if (compareOnline != 0) {
                            return compareOnline;
                        } else {
                            return o1.getUserId().compareTo(o2.getUserId());
                        }
                    })
                    .collect(Collectors.toList());

            contactElementsObservableList.clear();
            contactElementsObservableList.addAll(allContactElementList);
        });
    }

    private int compareStatus(String status1, String status2) {
        if (UserStatusType.OFFLINE.toString().equals(status1)) {
            if (UserStatusType.OFFLINE.toString().equals(status2)) {
                return 0;
            } else {
                return 1;
            }
        } else if (UserStatusType.OFFLINE.toString().equals(status2)) {
            return -1;
        } else {
            return 0;
        }
    }

    public void refreshUserProfileDetails(UserFullProfileDTO userProfile) {
        if (userProfile != null && activeWindowsMap.containsKey("DETAILS_" + userProfile.getId())) {
            WindowController windowController = activeWindowsMap.get("DETAILS_" + userProfile.getId());
            if (windowController instanceof UserDetailsController) {
                ((UserDetailsController) windowController).refreshUserProfile(userProfile);
            }
        }
    }

    public void refreshMatch(UserFullProfileDTO userProfile) {
        if (userProfile != null && activeWindowsMap.containsKey("MATCH")) {
            WindowController windowController = activeWindowsMap.get("MATCH");
            if (windowController instanceof MatchController) {
                ((MatchController) windowController).refreshMatch(userProfile);
            }
        }
    }

    public void addMessageToChatWindow(MessageDTO message) {
        if (message != null) {
            WindowController windowController = null;

            if (activeWindowsMap.containsKey("CHAT_" + message.getSendTo())) {
                windowController = activeWindowsMap.get("CHAT_" + message.getSendTo());
            } else if (activeWindowsMap.containsKey("CHAT_" + message.getSendFrom())) {
                windowController = activeWindowsMap.get("CHAT_" + message.getSendFrom());
            }

            if (windowController instanceof ChatController) {
                ((ChatController) windowController).addMessage(message);
            }
        }
    }

    public void refreshMessageListInChatWindow(Long sendTo, List<MessageDTO> messageList) {
        if (messageList != null && activeWindowsMap.containsKey("CHAT_" + sendTo)) {
            WindowController windowController = activeWindowsMap.get("CHAT_" + sendTo);
            if (windowController instanceof ChatController) {
                ((ChatController) windowController).refreshMessages(messageList);
            }
        }


    }

    public void refreshInvitesButton(int inviteDTOList) {
        if (inviteDTOList > 0) {
//            invitesButton
        }
    }

    public void refreshOtherMessagesButton(int messageList) {
        if (messageList > 0) {
//            messagesButton
        }
    }

    private void setStatusDetailsLabelText() {
        if (currentUserProfile.getStatusDetails() != null && !currentUserProfile.getStatusDetails().isEmpty()) {
            statusDetailsLabel.setText(currentUserProfile.getStatusDetails());
        } else {
            statusDetailsLabel.setText(Texts.ADD_STATUS_DETAILS);
        }
    }

    public void findUserProfileById(Long userId) {
        client.getUserProfileByUserId(userId);
    }

    public List<ContactDTO> getContactList() {
        return contactList;
    }

    public ContactDTO getCurrentUserProfile() {
        return currentUserProfile;
    }
}
