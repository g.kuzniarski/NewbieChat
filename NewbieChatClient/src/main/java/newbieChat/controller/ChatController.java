package newbieChat.controller;

import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import newbieChat.dto.ContactDTO;
import newbieChat.dto.MessageDTO;
import newbieChat.model.Client;
import newbieChat.util.JavaFXUtil;
import newbieChat.view.ChatMessageElement;
import org.controlsfx.glyphfont.FontAwesome;

import java.util.List;
import java.util.stream.Collectors;

import static newbieChat.util.DateUtil.formatOffsetDateTimeToZonedDateTime;

public class ChatController extends WindowController {

    @FXML
    Button sendButton;
    @FXML
    Button closeButton;
    @FXML
    TextArea messageTextArea;
    @FXML
    ScrollPane scrollPane;
    @FXML
    VBox messagesVBox;

    private ContactDTO owner;
    private ContactDTO contact;

    @Override
    void initialize() {
        //Set buttons icons
        sendButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.PAPER_PLANE));

        closeButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.TIMES));

        //Button actions
        sendButton.setOnAction(this::sendButtonAction);
        closeButton.setOnAction(JavaFXUtil::hideStage);
        messageTextArea.setOnKeyPressed(this::messageTextArenaKeyPressed);
        messageTextArea.setOnKeyReleased(this::messageTextArenaKeyReleased);

        messagesVBox.heightProperty().addListener(this::messagesListChanged);
    }

    @Override
    void reloadData() {
        Client.getInstance().getMessageList(contact.getId());
    }

    private void messagesListChanged(Observable observable) {
        scrollPane.setVvalue(1.0);
    }

    private void messageTextArenaKeyReleased(KeyEvent keyEvent) {
        if (keyEvent.getCode().equals(KeyCode.ENTER)) {
            messageTextArea.clear();
        }
    }

    private void messageTextArenaKeyPressed(KeyEvent keyEvent) {
        if (keyEvent.getCode().equals(KeyCode.ENTER)) {
            sendMessage();
        }
    }

    private void sendButtonAction(ActionEvent event) {
        sendMessage();
    }

    private void sendMessage() {
        String message = messageTextArea.getText().trim();
        messageTextArea.clear();
        if (!message.isEmpty()) {
            Client.getInstance().sendMessage(message, contact.getId());
        }
    }

    public void setContact(ContactDTO contact) {
        this.contact = contact;
        primaryStage.setTitle("Chat with " + contact.getNickname());
    }

    public void setOwner(ContactDTO owner) {
        this.owner = owner;
    }

    public void addMessage(MessageDTO messageDTO) {
        Platform.runLater(() -> {
            ChatMessageElement element = mapMessageDTOToChatMessageElement(messageDTO);
            messagesVBox.getChildren().add(element);
        });
    }

    public void refreshMessages(List<MessageDTO> messageDTOList) {
        Platform.runLater(() ->
                {
                    List<ChatMessageElement> elementList = messageDTOList
                            .stream()
                            .map(this::mapMessageDTOToChatMessageElement)
                            .collect(Collectors.toList());
                    messagesVBox.getChildren().clear();
                    messagesVBox.getChildren().addAll(elementList);
                }
        );
    }

    private ChatMessageElement mapMessageDTOToChatMessageElement(MessageDTO messageDTO) {
        ChatMessageElement chatMessageElement = new ChatMessageElement();

        if (messageDTO.getSendFrom().equals(contact.getId())) {
            chatMessageElement.setNickname(contact.getNickname());
            chatMessageElement.setAvatar(contact.getAvatarPath());
        } else {
            chatMessageElement.setNickname(owner.getNickname());
            chatMessageElement.setAvatar(owner.getAvatarPath());
        }

        chatMessageElement.setSendDate(formatOffsetDateTimeToZonedDateTime(messageDTO.getSendDate()));
        chatMessageElement.setContent(messageDTO.getContent());
        return chatMessageElement;
    }
}
