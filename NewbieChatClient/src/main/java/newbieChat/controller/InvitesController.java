package newbieChat.controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import newbieChat.dto.InviteDTO;
import newbieChat.util.JavaFXUtil;
import newbieChat.view.InvitesElement;
import org.controlsfx.glyphfont.FontAwesome;

import java.util.List;
import java.util.stream.Collectors;

public class InvitesController extends WindowController {

    @FXML
    ListView<InvitesElement> invitesListView;
    @FXML
    Button closeButton;

    public ObservableList<InvitesElement> invitesElementObservableList = FXCollections.observableArrayList();

    private static InvitesController instance;

    public static InvitesController getInstance() {
        return instance;
    }

    @Override
    public void initialize() {
        instance = this;

        closeButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.TIMES));
        closeButton.setOnAction(JavaFXUtil::hideStage);

        invitesListView.setItems(invitesElementObservableList);
    }

    @Override
    void reloadData() {
        client.getInvites();
    }

    public void refreshInvites(List<InviteDTO> inviteDTOList) {
        Platform.runLater(() -> {
            invitesElementObservableList.clear();
            invitesElementObservableList.addAll(inviteDTOList
                    .stream()
                    .map(this::mapInviteDTOToInvitesElement)
                    .collect(Collectors.toList()));
        });
    }

    private InvitesElement mapInviteDTOToInvitesElement(InviteDTO inviteDTO) {
        InvitesElement invitesElement = new InvitesElement();
        invitesElement.setContact(inviteDTO);
        invitesElement.init();
        return invitesElement;
    }
}
