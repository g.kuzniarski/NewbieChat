package newbieChat.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import newbieChat.dictionary.Images;
import newbieChat.dictionary.Texts;
import newbieChat.dto.ConfigGameProfileDTO;
import newbieChat.dto.GameProfileDTO;
import newbieChat.dto.UserFullProfileDTO;
import newbieChat.util.JavaFXUtil;
import org.controlsfx.glyphfont.FontAwesome;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static newbieChat.util.JavaFXUtil.generateImageViewCircleFromImageURL;

public class SettingsController extends WindowController {

    @FXML
    TabPane settingsTabPane;
    @FXML
    TextField nameTextField;
    @FXML
    ComboBox<String> languageComboBox;
    @FXML
    PasswordField oldPasswordField;
    @FXML
    PasswordField newPasswordField;
    @FXML
    PasswordField repeatPasswordField;
    @FXML
    Button deleteAccountButton;
    @FXML
    ImageView avatarImageView;
    @FXML
    TextField avatarTextField;
    @FXML
    Button changeAvatarButton;
    @FXML
    TextField countryTextField;
    @FXML
    TextField cityTextField;
    @FXML
    ComboBox<String> sexComboBox;
    @FXML
    Spinner<Integer> ageSpinner;
    @FXML
    ComboBox<String> gamingStyleComboBox;
    @FXML
    ComboBox<String> timeSpentComboBox;
    @FXML
    ComboBox<String> timeOfDayComboBox;
    @FXML
    ComboBox<String> gameComboBox;
    @FXML
    Button addProfileButton;
    @FXML
    Button saveButton;
    @FXML
    Button cancelButton;
    @FXML
    Button applyButton;

    private ObjectMapper objectMapper = new ObjectMapper();
    private UserFullProfileDTO currentUserFullProfile;
    private List<ConfigGameProfileDTO> configGameProfileDTOList;
    private ObservableList<String> gameList = FXCollections.observableArrayList();
    private Map<String, String> gameProfileElementsMap = new HashMap<>();
    private Map<String, Node> gameProfileNodeMap = new HashMap<>();

    private static SettingsController instance;

    public static SettingsController getInstance() {
        return instance;
    }

    @Override
    public void initialize() {
        instance = this;

        //Set buttons icons
        addProfileButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.USER_PLUS));
        deleteAccountButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.USER_TIMES));

        //Validation
//        ValidationSupport validationSupport = new ValidationSupport();
//        validationSupport.setErrorDecorationEnabled(false);
//        validationSupport.registerValidator(newPasswordField, false, getTextLengthValidation("Login", 5, 16));
//        validationSupport.registerValidator(repeatPasswordField, false, getTextLengthValidation("Password", 4, 32));

        //Button actions
        addProfileButton.setOnAction(this::addProfileButtonAction);
        changeAvatarButton.setOnAction(this::changeAvatarButtonAction);
        saveButton.setOnAction(this::saveButtonAction);
        cancelButton.setOnAction(JavaFXUtil::hideStage);
        applyButton.setOnAction(this::applyButtonAction);
        deleteAccountButton.setOnAction(this::deleteAccountButtonAction);

        //Init spinners
        ageSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 100, 20, 1));

        //Fill combo boxes
        fillComboBoxes();
    }

    @Override
    void reloadData() {
        client.getSettingsScreenData();
    }

    private void addProfileButtonAction(ActionEvent event) {
        String selectedItem = gameComboBox.getSelectionModel().getSelectedItem();
        if (settingsTabPane.getTabs().stream().noneMatch(tab -> tab.getText().equals(selectedItem))) {
            configGameProfileDTOList
                    .stream()
                    .filter(configGameProfileDTO -> configGameProfileDTO.getGame().equals(selectedItem))
                    .findFirst()
                    .ifPresent(configGameProfileDTO -> {
                        Tab gameTab = generateGameTab(configGameProfileDTO);
                        if (gameTab != null) {
                            this.settingsTabPane.getTabs().add(gameTab);
                        }
                    });
        }
    }

    private void applyButtonAction(ActionEvent event) {
        prepareUserProfile();
        client.sendUserUpdate(currentUserFullProfile);
    }

    private void prepareUserProfile() {
        currentUserFullProfile.setName(nameTextField.getText());
        currentUserFullProfile.setAvatarPath(avatarTextField.getText());
        currentUserFullProfile.setLanguage(languageComboBox.getSelectionModel().getSelectedItem());
        currentUserFullProfile.setCountry(countryTextField.getText());
        currentUserFullProfile.setCity(cityTextField.getText());
        currentUserFullProfile.setAge(ageSpinner.getValue());
        currentUserFullProfile.setGamingStyle(gamingStyleComboBox.getSelectionModel().getSelectedItem());
        currentUserFullProfile.setTimeOfDay(timeOfDayComboBox.getSelectionModel().getSelectedItem());
        currentUserFullProfile.setTimeSpent(timeSpentComboBox.getSelectionModel().getSelectedItem());

        List<GameProfileDTO> gameProfileDTOList = new ArrayList<>();

        Set<String> keySet = gameProfileElementsMap.keySet();
        keySet.forEach((id) -> {
            String value = null;

            Node field = gameProfileNodeMap.get(id);
            if (field instanceof TextField) {
                value = ((TextField) field).getText();
            } else if (field instanceof ComboBox) {
                value = ((ComboBox<String>) field).getSelectionModel().getSelectedItem();
            }
            if (value != null && !value.isEmpty() && !value.equals("null")) {
                gameProfileElementsMap.put(id, value);
            }
        });

        gameProfileElementsMap.entrySet().removeIf(entry -> entry.getValue() == null || entry.getValue().isEmpty());

        gameProfileElementsMap
                .entrySet()
                .stream()
                .collect(Collectors.toMap(
                        entry -> entry.getKey().substring(0, entry.getKey().indexOf("::")),
                        entry -> "\"" + entry.getKey().substring(entry.getKey().indexOf("::") + 2) + "\": \"" + entry.getValue() + "\"",
                        (value1, value2) -> value1 + ", " + value2))
                .forEach((game, details) -> gameProfileDTOList.add(new GameProfileDTO(game, "{" + details + "}")));

        currentUserFullProfile.setGameProfileList(gameProfileDTOList);

        if (oldPasswordField.getText() != null && !oldPasswordField.getText().isEmpty()
                && newPasswordField.getText() != null && !newPasswordField.getText().isEmpty()
                && repeatPasswordField.getText() != null && !repeatPasswordField.getText().isEmpty()
                && newPasswordField.getText().equals(repeatPasswordField.getText())) {
            currentUserFullProfile.setOldPassword(oldPasswordField.getText());
            currentUserFullProfile.setNewPassword(newPasswordField.getText());
        }
    }

    private void saveButtonAction(ActionEvent event) {
        applyButtonAction(event);
        JavaFXUtil.hideStage(event);
    }

    private void changeAvatarButtonAction(ActionEvent event) {
        currentUserFullProfile.setAvatarPath(avatarTextField.getText());
        refreshAvatar();
    }

    private void refreshAvatar() {
        if (currentUserFullProfile != null
                && currentUserFullProfile.getAvatarPath() != null && !currentUserFullProfile.getAvatarPath().isEmpty()) {
            generateImageViewCircleFromImageURL(avatarImageView, currentUserFullProfile.getAvatarPath());
        } else {
            generateImageViewCircleFromImageURL(avatarImageView, Images.DEFAULT_AVATAR_IMAGE_FILE);
        }
    }

    private void deleteAccountButtonAction(ActionEvent event) {
        client.deleteAccount();
    }

    private void fillComboBoxes() {
        fillSexComboBox();
        fillGamingStyleComboBox();
        fillTimeSpentComboBox();
        fillTimeOfDayComboBox();
        fillLanguageComboBox();
        fillGameComboBox();
    }

    private void fillSexComboBox() {
        ObservableList<String> sexTypeList = FXCollections.observableArrayList();
        sexTypeList.addAll(Texts.SEX_TYPES);
        sexComboBox.setItems(sexTypeList);
    }

    private void fillGamingStyleComboBox() {
        ObservableList<String> gamingStyleList = FXCollections.observableArrayList();
        gamingStyleList.addAll(Texts.GAMING_STYLES_TYPES);
        gamingStyleComboBox.setItems(gamingStyleList);
    }

    private void fillTimeSpentComboBox() {
        ObservableList<String> timeSpentTypeList = FXCollections.observableArrayList();
        timeSpentTypeList.addAll(Texts.TIME_SPENT_TYPES);
        timeSpentComboBox.setItems(timeSpentTypeList);
    }

    private void fillTimeOfDayComboBox() {
        ObservableList<String> timeOfDayTypeList = FXCollections.observableArrayList();
        timeOfDayTypeList.addAll(Texts.TIME_OF_DAY_TYPES);
        timeOfDayComboBox.setItems(timeOfDayTypeList);
    }

    private void fillLanguageComboBox() {
        ObservableList<String> languagesList = FXCollections.observableArrayList();
        languagesList.addAll(Texts.LANGUAGES);
        languageComboBox.setItems(languagesList);
    }

    private void fillGameComboBox() {
        gameComboBox.setItems(gameList);
    }

    public void refreshSettings(List<ConfigGameProfileDTO> configGameProfileDTOList, UserFullProfileDTO userFullProfileDTO) {
        Platform.runLater(() -> {
            this.currentUserFullProfile = userFullProfileDTO;
            this.configGameProfileDTOList = configGameProfileDTOList;
            gameProfileElementsMap.clear();
            refreshGameProfiles();
            refreshUserSettings();
            refreshAvatar();
        });
    }

    private void refreshGameProfiles() {
        if (currentUserFullProfile.getGameProfileList() != null && this.settingsTabPane.getTabs().size() > 3) {
            this.settingsTabPane.getTabs().remove(3, this.settingsTabPane.getTabs().size());
        }

        gameList.clear();
        gameList.addAll(configGameProfileDTOList
                .stream()
                .map(ConfigGameProfileDTO::getGame)
                .collect(Collectors.toList()));

        configGameProfileDTOList.forEach(configGameProfileDTO -> {
            if (currentUserFullProfile.getGameProfileList().stream().anyMatch(gameProfileDTO -> gameProfileDTO.getGame().equals(configGameProfileDTO.getGame()))) {
                Tab gameTab = generateGameTab(configGameProfileDTO);
                if (gameTab != null) {
                    gameTab.setOnClosed(this::closeTabAction);
                    this.settingsTabPane.getTabs().add(gameTab);
                }
            }
        });
    }

    private void closeTabAction(Event event) {
        String game = ((Tab) event.getSource()).getText();
        gameProfileElementsMap = gameProfileElementsMap
                .entrySet()
                .stream()
                .filter(entry -> entry.getKey().startsWith(game))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private void refreshUserSettings() {
        if (currentUserFullProfile.getCountry() != null) {
            this.countryTextField.setText(currentUserFullProfile.getCountry());
        }

        if (currentUserFullProfile.getCity() != null) {
            this.cityTextField.setText(currentUserFullProfile.getCity());
        }

        if (currentUserFullProfile.getSex() != null) {
            this.sexComboBox.getSelectionModel().select(currentUserFullProfile.getSex());
        }

        if (currentUserFullProfile.getAge() != null) {
            this.ageSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 100, currentUserFullProfile.getAge(), 1));
        }

        if (currentUserFullProfile.getGamingStyle() != null) {
            this.gamingStyleComboBox.getSelectionModel().select(currentUserFullProfile.getGamingStyle());
        }

        if (currentUserFullProfile.getTimeSpent() != null) {
            this.timeSpentComboBox.getSelectionModel().select(currentUserFullProfile.getTimeSpent());
        }

        if (currentUserFullProfile.getTimeOfDay() != null) {
            this.timeOfDayComboBox.getSelectionModel().select(currentUserFullProfile.getTimeOfDay());
        }

        if (currentUserFullProfile.getName() != null) {
            this.nameTextField.setText(currentUserFullProfile.getName());
        }

        if (currentUserFullProfile.getAvatarPath() != null) {
            this.avatarTextField.setText(currentUserFullProfile.getAvatarPath());
        }

        if (currentUserFullProfile.getLanguage() != null) {
            this.languageComboBox.getSelectionModel().select(currentUserFullProfile.getLanguage());
        }
    }

    private Tab generateGameTab(ConfigGameProfileDTO configGameProfileDTO) {
        Label tabTitleLabel = new Label(configGameProfileDTO.getGame());
        tabTitleLabel.setFont(Font.font(16));
        GridPane.setMargin(tabTitleLabel, new Insets(10));

        GridPane gameGridPane = new GridPane();
        GridPane.setMargin(gameGridPane, new Insets(10));
        final int[] row = {0};

        try {
            Map<String, Map<String, Object>> gameDetails = objectMapper.readValue(configGameProfileDTO.getDetails(), new TypeReference<HashMap<String, Map<String, Object>>>() {
            });

            gameDetails.forEach((paramsKey, paramsValue) -> {
                String elementId = configGameProfileDTO.getGame() + "::" + paramsKey;

                Map<String, String> userGameParams = currentUserFullProfile
                        .getGameProfileList()
                        .stream()
                        .filter(gameProfileDTO -> gameProfileDTO.getGame().equals(configGameProfileDTO.getGame()))
                        .findFirst()
                        .map(userGameProfileDTO -> {
                            try {
                                return objectMapper.<HashMap<String, String>>readValue(userGameProfileDTO.getDetails(), new TypeReference<HashMap<String, String>>() {
                                });
                            } catch (IOException e) {
                                e.printStackTrace();
                                return null;
                            }
                        }).orElse(new HashMap<>());

                Label gameLabel = new Label(paramsKey);
                gameLabel.setFont(Font.font(13));
                gameLabel.setPrefWidth(150);
                GridPane.setMargin(gameLabel, new Insets(7));

                switch ((String) paramsValue.get("type")) {
                    case "text":
                        TextField textField = new TextField();
                        gameProfileElementsMap.put(elementId, "");

                        if (userGameParams.containsKey(paramsKey)) {
                            textField.setText(userGameParams.get(paramsKey));
                        }

                        GridPane.setMargin(textField, new Insets(7));
                        gameProfileNodeMap.put(elementId, textField);
                        gameGridPane.add(gameLabel, 0, row[0]);
                        gameGridPane.add(textField, 1, row[0]);
                        row[0]++;
                        break;
                    case "select":
                        ComboBox<String> comboBox = new ComboBox<>();
                        gameProfileElementsMap.put(elementId, "");

                        ObservableList<String> observableList = FXCollections.observableArrayList();
                        comboBox.setItems(observableList);

                        if (paramsValue.containsKey("values")) {
                            observableList.addAll((ArrayList<String>) paramsValue.get("values"));
                        }

                        if (userGameParams.containsKey(paramsKey)) {
                            comboBox.getSelectionModel().select(userGameParams.get(paramsKey));
                        }

                        comboBox.setMaxWidth(Double.MAX_VALUE);
                        GridPane.setMargin(comboBox, new Insets(7));
                        gameProfileNodeMap.put(elementId, comboBox);
                        gameGridPane.add(gameLabel, 0, row[0]);
                        gameGridPane.add(comboBox, 1, row[0]);
                        row[0]++;
                        break;
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        if (row[0] == 0) {
            return null;
        }

        GridPane mainGridPane = new GridPane();
        mainGridPane.add(tabTitleLabel, 0, 0);
        mainGridPane.add(gameGridPane, 0, 1);

        return new Tab(configGameProfileDTO.getGame(), mainGridPane);
    }

}
